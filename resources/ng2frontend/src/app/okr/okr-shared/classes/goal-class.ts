export class Goalclass {


  goal_id: number;
  goal_name: string;
  goal_description: string;
  time_frame_id: number;
  time_frame_description: string;

  time_frame_start: Date;
  time_frame_end: Date;
  goal_status: string;
  goal_unit: string;
  goal_progress_status: string;
  goal_target: string;






}
