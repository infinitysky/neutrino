export class Objectiveclass {

    objective_id: number;
    objective_name: string;
    objective_description: string;
    objective_unit: string;
    objective_status: string;
    objective_progress_status: string;


    objective_target: string;

}
