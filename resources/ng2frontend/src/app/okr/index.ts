/**
*	This barrel file provides the export for the lazy loaded DashboardComponent.
*/
export * from './okr.component';
export * from './okr-routing.module';
