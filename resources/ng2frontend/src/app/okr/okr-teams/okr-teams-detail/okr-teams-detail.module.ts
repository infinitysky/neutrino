import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OkrTeamsDetailRoutingModule } from './okr-teams-detail-routing.module';
import { OkrTeamsDetailComponent } from './okr-teams-detail.component';

import { DoorgetsTruncateModule } from 'doorgets-ng-truncate';


import {OkrTeamsMembersComponent} from '../okr-teams-members/okr-teams-members.component';
import {OkrTeamsActivityComponent} from '../okr-teams-activity/okr-teams-activity.component';

@NgModule({
  imports: [
    DoorgetsTruncateModule,
    CommonModule,
    OkrTeamsDetailRoutingModule
  ],
  declarations: [OkrTeamsDetailComponent,OkrTeamsMembersComponent,OkrTeamsActivityComponent ]
})
export class OkrTeamsDetailModule { }
