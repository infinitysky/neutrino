<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 16/05/2017
 * Time: 10:03
 */
class System_setting_model extends CI_Model
{
    public $table = 'system_setting';
    public $id = 'setting_id';
    public $order = 'ASC';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function get_company_info(){
        $mysqlQuery = "
        SELECT system_setting.*
        FROM system_setting
        WHERE setting_name IN ('company_name',  
         'company_mission','company_vision','company_address','company_phone','company_email')
        ";
        $result = $this->db->query($mysqlQuery);


        return $result->result();

    }
    function get_company_name(){
        $mysqlQuery = "
        SELECT setting_name, setting_value
        FROM system_setting
        WHERE system_setting.setting_name = 'company_name'
        ";
        $result = $this->db->query($mysqlQuery);
        return $result->row();

    }

    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->select("*");
        $this->db->from($this->table);

        return $this->db->get()->result();
    }

    // get data by id
    function get_by_id($id)
    {

        $this->db->order_by($this->id, $this->order);
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->id, $id);
        return $this->db->get()->row();
    }

    // insert data
    function insert($data)
    {
        $this->db->trans_start();
        $this->db->insert($this->table, $data);
        $insert_id=$this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }


    // update data
    function update($id, $data)
    {
        $this->db->trans_start();
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
        return  $affectedRowsNumber;
    }

    // delete data
    function delete($id)
    {
        $this->db->trans_start();
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
        return  $affectedRowsNumber;
    }

    function update_company_info($dataArray){

        $this->db->trans_start();
        $this->db->update_batch($this->table, $dataArray,'setting_name');
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
       // echo $this->db->last_query();
        return  $affectedRowsNumber;
    }



}