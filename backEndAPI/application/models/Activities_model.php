<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Activities_model extends CI_Model
{

    public $table = 'activities';
    public $id = 'activity_id';
    public $order = 'DESC';
    public $orderByDate = 'activity_timestamp';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    // datatables
    function json() {
        $this->datatables->select('activity_id,activity_detail');
        $this->datatables->from('activities');
        //add this line for join
        //$this->datatables->join('table2', 'activities.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('activities/read/$1'),'Read')." | ".anchor(site_url('activities/update/$1'),'Update')." | ".anchor(site_url('activities/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'activity_id');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->trans_start();
        $this->db->order_by($this->orderByDate, $this->order);
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join("users_details",$this->table.".user_id = users_details.user_id",'left');

        $result=$this->db->get();
        $this->db->trans_complete();


        return  $result->result();

    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }


    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('activity_id', $q);
        $this->db->or_like('activity_detail', $q);
        $this->db->or_like('activity_comment', $q);
        $this->db->or_like('activity_type', $q);
        $this->db->or_like('user_id', $q);
        $this->db->or_like('activity_timestamp', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('activity_id', $q);
        $this->db->or_like('activity_detail', $q);
        $this->db->or_like('activity_comment', $q);
        $this->db->or_like('activity_type', $q);
        $this->db->or_like('user_id', $q);
        $this->db->or_like('activity_timestamp', $q);



        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->trans_start();
        $this->db->insert($this->table, $data);
        $insert_id=$this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }


    // update data
    function update($id, $data)
    {
        $this->db->trans_start();
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
        return  $affectedRowsNumber;
    }

    // delete data
    function delete($id)
    {
        $this->db->trans_start();
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
        return  $affectedRowsNumber;
    }



    //get activities by user id and receiver_user_id
    function get_by_user_id($userId){
        $this->db->trans_start();


        $mysqlQuery = "SELECT
                        activities.*,
                        users_details.first_name,
                        users_details.last_name,
                        receiver_details.first_name AS receiver_first_name,
                        receiver_details.last_name AS receiver_last_name
                        FROM activities
                        
                        LEFT JOIN users ON activities.user_id = users.user_id
                        LEFT JOIN users_details ON users_details.user_id = users.user_id
                        
                        LEFT JOIN users as receiver ON activities.receiver_user_id = receiver.user_id 
                        LEFT JOIN users_details as receiver_details ON receiver_details.user_id = receiver.user_id
                        
                        WHERE activities.user_id = $userId
                        OR activities.receiver_user_id = $userId
                        
                        GROUP BY activities.activity_id
                        ORDER BY activities.activity_id DESC
                        ";

        $result = $this->db->query($mysqlQuery);
        $this->db->trans_complete();
        return  $result->result();
    }

    function get_by_team_id($team_Id){
        $this->db->trans_start();
        $this->db->order_by($this->orderByDate, $this->order);
        $this->db->select("*");
        $this->db->from($this->table);

        $this->db->join("teams_users",$this->table.".user_id = teams_users.user_id",'left');

        $this->db->join("users_details",$this->table.".user_id = users_details.user_id",'left');
        $this->db->where('teams_users.team_id',$team_Id);
        $result=$this->db->get();
        $this->db->trans_complete();
        return  $result->result();
    }

    function get_last_activities($timeFrame, $numbers){
        $this->db->trans_start();




        $result=$this->db->get();
        $this->db->trans_complete();
        return  $result->result();
    }



    function get_by_goal_id($goal_id){
        $mysqlQuery = "
      SELECT
            activities.*, users_details.first_name,
            users_details.last_name,
            receiver_details.first_name AS receiver_first_name,
            receiver_details.last_name AS receiver_last_name
        FROM
            activities
        LEFT JOIN users ON activities.user_id = users.user_id
        LEFT JOIN users_details ON users_details.user_id = users.user_id
        LEFT JOIN users AS receiver ON activities.receiver_user_id = receiver.user_id
        LEFT JOIN users_details AS receiver_details ON receiver_details.user_id = receiver.user_id,
         goals_objectives
        WHERE
            (
                activity_group_id IN (
                    SELECT
                        key_results.result_id
                    FROM
                        key_results
                    WHERE
                        key_results.objective_id IN (
                            SELECT
                                goals_objectives.objective_id
                            FROM
                                goals_objectives
                            WHERE
                                goal_id = $goal_id
                        )
                )
                AND activities.activity_group = 'k'
            )
        OR (
            activity_group_id IN (
                SELECT
                    goals_objectives.objective_id
                FROM
                    goals_objectives
                WHERE
                    goal_id = $goal_id
            )
            AND activities.activity_group = 'o'
        )
        OR (
            activity_group_id = $goal_id
            AND activities.activity_group = 'g'
        )
        GROUP BY
            activities.activity_id
        ORDER BY
            activities.activity_id DESC

        
        
        ";


        $result = $this->db->query($mysqlQuery);
        $this->db->trans_complete();
        return  $result->result();
    }

    function get_by_objective_id($objective_id){
        $this->db->trans_start();

        $mysqlQuery = " 
            SELECT
                    activities.*,
                    users_details.first_name,
                    users_details.last_name,
                    receiver_details.first_name AS receiver_first_name,
                    receiver_details.last_name AS receiver_last_name
            FROM    activities
            LEFT JOIN users ON activities.user_id = users.user_id
            
            LEFT JOIN users_details ON users_details.user_id = users.user_id
            
            LEFT JOIN users AS receiver ON activities.receiver_user_id = receiver.user_id
            
            LEFT JOIN users_details AS receiver_details ON receiver_details.user_id = receiver.user_id
            
            LEFT JOIN teams_users ON teams_users.user_id = users.user_id 
                        AND teams_users.user_id = receiver.user_id
            
            LEFT JOIN teams ON teams.team_leader_user_id = users.user_id 
                        AND teams.team_leader_user_id = receiver.user_id 
                        AND teams_users.team_id = teams.team_id ,     
                        goals_objectives
            
            WHERE activities.activity_group='o' 
                  and activities.activity_group_id = $objective_id
            
            GROUP BY activities.activity_id
            ORDER BY activities.activity_id DESC;
        ";

        //activities.activity_group='o'  the O stands for objectives


        //$result=$this->db-> get();
        $result = $this->db->query($mysqlQuery);


        $this->db->trans_complete();
        return  $result->result();
    }


   function get_by_KeyResult_id($KeyResult_id){
        $this->db->trans_start();

        $mysqlQuery = "
            SELECT
                    activities.*,
                    users_details.first_name,
                    users_details.last_name,
                    receiver_details.first_name AS receiver_first_name,
                    receiver_details.last_name AS receiver_last_name
            FROM    activities
            LEFT JOIN users ON activities.user_id = users.user_id
            
            LEFT JOIN users_details ON users_details.user_id = users.user_id
            
            LEFT JOIN users AS receiver ON activities.receiver_user_id = receiver.user_id
            
            LEFT JOIN users_details AS receiver_details ON receiver_details.user_id = receiver.user_id
            
            LEFT JOIN teams_users ON teams_users.user_id = users.user_id 
                        AND teams_users.user_id = receiver.user_id
            
            LEFT JOIN teams ON teams.team_leader_user_id = users.user_id 
                        AND teams.team_leader_user_id = receiver.user_id 
                        AND teams_users.team_id = teams.team_id ,     
                        goals_objectives
            
            WHERE activities.activity_group='k' 
                  and activities.activity_group_id = $KeyResult_id
            
            GROUP BY activities.activity_id
            ORDER BY activities.activity_id DESC;
        ";



        //$result=$this->db-> get();
        $result = $this->db->query($mysqlQuery);
        $this->db->trans_complete();
        return  $result->result();
    }


    function get_by_timeFrame_id($time_frame_id){
        $this->db->trans_start();



        $mysqlQuery = " 
           SELECT
                activities.*, users_details.first_name,
                users_details.last_name,
                receiver_details.first_name AS receiver_first_name,
                receiver_details.last_name AS receiver_last_name
            FROM
                activities
            LEFT JOIN users ON activities.user_id = users.user_id
            LEFT JOIN users_details ON users_details.user_id = users.user_id
            LEFT JOIN users AS receiver ON activities.receiver_user_id = receiver.user_id
            LEFT JOIN users_details AS receiver_details ON receiver_details.user_id = receiver.user_id,
             goals_objectives
            WHERE
                (
                    activity_group_id IN (
                        SELECT
                            key_results.result_id
                        FROM
                            key_results
                        WHERE
                            key_results.objective_id IN (
                                SELECT
                                    goals_objectives.objective_id
                                FROM
                                    goals_objectives
                                WHERE
                                    goal_id IN (
                                        SELECT
                                            goals.goal_id
                                        FROM
                                            goals
                                        LEFT JOIN time_frames ON goals.time_frame_id = time_frames.time_frame_id
                                        WHERE
                                            time_frames.time_frame_id = $time_frame_id
                                    )
                            )
                    )
                    AND activities.activity_group = 'k'
                )
            OR (
                activity_group_id IN (
                    SELECT
                        goals_objectives.objective_id
                    FROM
                        goals_objectives
                    WHERE
                        goal_id IN (
                            SELECT
                                goals.goal_id
                            FROM
                                goals
                            LEFT JOIN time_frames ON goals.time_frame_id = time_frames.time_frame_id
                            WHERE
                                time_frames.time_frame_id = $time_frame_id
                        )
                )
                AND activities.activity_group = 'o'
            )
            OR (
                activity_group_id IN (
                    SELECT
                        goals.goal_id
                    FROM
                        goals
                    LEFT JOIN time_frames ON goals.time_frame_id = time_frames.time_frame_id
                    WHERE
                        time_frames.time_frame_id = $time_frame_id
                )
                AND activities.activity_group = 'g'
            )
            GROUP BY
                activities.activity_id
            ORDER BY
                activities.activity_id DESC
        ";

        //activities.activity_group='o'  the O stands for objectives


        //$result=$this->db-> get();
        $result = $this->db->query($mysqlQuery);


        $this->db->trans_complete();
        return  $result->result();
    }




}
