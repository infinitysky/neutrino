<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reviews_model extends CI_Model
{
    public $table = 'reviews';
    public $id = 'review_id';
    public $order = 'DESC';


    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    // get all
    function get_all()
    {
        $this->db->trans_start();
        $this->db->order_by($this->id, $this->order);
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join("users_details",$this->table.".review_user_id = users_details.user_id",'left');

        $result=$this->db->get();
        $this->db->trans_complete();


        return  $result->result();

    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        $this->db->join("users_details",$this->table.".review_user_id = users_details.user_id",'left');

        return $this->db->get($this->table)->row();
    }


    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('review_id', $q);
        $this->db->or_like('review_rating', $q);
        $this->db->or_like('review_details', $q);
        $this->db->or_like('review_user_id', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }


    // insert data
    function insert($data)
    {
        $this->db->trans_start();
        $this->db->insert($this->table, $data);
        $insert_id=$this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }


    // update data
    function update($id, $data)
    {
        $this->db->trans_start();
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
        return  $affectedRowsNumber;
    }

    // delete data
    function delete($id)
    {
        $this->db->trans_start();
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
        return  $affectedRowsNumber;
    }

    function get_by_objective_id($id)
    {
        $mysqlQuery ="
            SELECT
            reviews.*,
            users_details.last_name,
            users_details.first_name
            FROM reviews , users
            LEFT JOIN users_details ON users_details.user_id = users.user_id
            WHERE reviews.review_object_type = 'o' AND reviews.review_object_id = $id
            GROUP BY reviews.review_id
            ORDER BY reviews.review_id DESC
        
        ";
        $this->db->trans_start();
        $result=$this->db->query($mysqlQuery);
        $this->db->trans_complete();
        return $result->row();
    }

    function get_by_goal_id($id)
    {
        $mysqlQuery ="
            SELECT
            reviews.*,
            users_details.last_name,
            users_details.first_name
            FROM reviews , users
            LEFT JOIN users_details ON users_details.user_id = users.user_id
            WHERE reviews.review_object_type = 'g' AND reviews.review_object_id = $id
            GROUP BY reviews.review_id
            ORDER BY reviews.review_id DESC
        
        ";

        $this->db->trans_start();
        $result=$this->db->query($mysqlQuery);
        $this->db->trans_complete();
        return $result->row();

    }


}