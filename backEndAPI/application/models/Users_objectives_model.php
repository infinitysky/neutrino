<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_objectives_model extends CI_Model
{

    public $table = 'users_objectives';
    public $id = 'record_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    // datatables
    function json() {
        $this->datatables->select('objective_id,user_id,record_id');
        $this->datatables->from('users_objectives');
        //add this line for join
        //$this->datatables->join('table2', 'users_objectives.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('users_objectives/read/$1'),'Read')." | ".anchor(site_url('users_objectives/update/$1'),'Update')." | ".anchor(site_url('users_objectives/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('record_id', $q);
        $this->db->or_like('objective_id', $q);
        $this->db->or_like('user_id', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('record_id', $q);
        $this->db->or_like('objective_id', $q);
        $this->db->or_like('user_id', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->trans_start();
        $this->db->insert($this->table, $data);
        $insert_id=$this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }

    function batch_insert($data)
    {
        $this->db->trans_start();
        $query=$this->db->insert_batch($this->table, $data);
       // $query=$this->db->insert_id();
        $this->db->trans_complete();
        return $query;

    }



    // update data
    function update($id, $data)
    {
        $this->db->trans_start();
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
        return  $affectedRowsNumber;
    }

    // delete data
    function delete($id)
    {
        $this->db->trans_start();
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
        return  $affectedRowsNumber;
    }


    // delete data
    function delete_by_user_id_objective_id($userId,$teamId)
    {

        $this->db->trans_start();

        $this->db->where('user_id', $userId);
        $this->db->where('objective_id', $teamId);

        $this->db->delete($this->table);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
        return $affectedRowsNumber;

    }


    //The idea is comes from DELETE FROM `users_objectives` WHERE `users_objectives`.`objective_id`=3 AND `users_objectives`.`user_id` IN (87,88,89)
    function batch_delete_by_user_id($userId,$teamIdDataArray)
    {
        $this->db->trans_start();

        $this->db->where('user_id', $userId);
        $this->db->where_in('objective_id', $teamIdDataArray);

        $this->db->delete($this->table);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
        return $affectedRowsNumber;

    }



    //delete data
    function batch_delete_by_objective_id($teamId,$userIdDataArray)
    {
        //DELETE FROM `users_objectives` WHERE `objective_id` = '19' AND `user_id` IN(0, '100', '99', '76'))

        $this->db->trans_start();
        $this->db->where('objective_id', $teamId);
        $this->db->where_in('user_id', $userIdDataArray);
        $this->db->delete($this->table);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();

     //   echo $this->db->last_query();
        return $affectedRowsNumber;

    }

    function delete_all_by_user_id($userId)
    {
        $this->db->trans_start();


        $this->db->where_in('objective_id', $userId);

        $this->db->delete($this->table);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
        return $affectedRowsNumber;

    }

    function delete_all_by_objective_id($objective_id)
    {
        $this->db->trans_start();


        $this->db->where_in('objective_id', $objective_id);

        $this->db->delete($this->table);
        $affectedRowsNumber=$this->db->affected_rows();
        $this->db->trans_complete();
        return $affectedRowsNumber;

    }



    function get_by_user_id($user_id){
        $this->db->trans_start();
        $this->db->order_by($this->id, $this->order);
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('users_objectives.user_id',$user_id);
        $this->db->join('objectives', 'objectives.objective_id=users_objectives.objective_id','left');
        $this->db->join('users_details', 'users_details.user_id=users_objectives.user_id','left');
        $this->db->join('users', 'users.user_id=users_details.user_id','left');


        $queryResult=$this->db->get();
        $this->db->trans_complete();


        //echo $this->db->last_query();

        return $queryResult->result();



    }
    function get_by_objective_id($objective_id){

        $this->db->trans_start();
        $this->db->order_by($this->id, $this->order);
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('users_objectives.objective_id',$objective_id);
        $this->db->join('objectives', 'objectives.objective_id=users_objectives.objective_id','left');
        $this->db->join('users_details', 'users_details.user_id=users_objectives.user_id','left');
        $this->db->join('users', 'users.user_id=users_details.user_id','left');

        $queryResult=$this->db->get();
        $this->db->trans_complete();



        return $queryResult->result();

    }

    function get_objectives_and_users_details(){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('objectives', 'objectives.objective_id=users_objectives.objective_id','left');
        $this->db->join('users_details', 'users_details.user_id=users_objectives.user_id','left');
        $queryResult=$this->db->get();
        $this->db->trans_complete();
        return $queryResult->result();

    }

}

