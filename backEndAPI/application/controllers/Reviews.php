<?php


class Reviews extends CI_Controller
{

    function __construct()
    {

        parent::__construct();

        header('Content-type: application/json');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

        $this->load->model('Reviews_model');


        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        };
    }



    public function index()
    {
        $this->getall();
    }


    //Main entrance
    public function items($id)
    {
        $Data = json_decode(trim(file_get_contents('php://input')), true);
        //GET, POST, OPTIONS, PUT, DELETE
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }elseif ($method == "GET"){

            $this->read($id);
        }elseif ($method == "PUT"){

            $this->update($id,$Data);
        }elseif ($method == "DELETE"){

            $this->delete($id);
        }

    }


    public function json($resArray) {
        header('Content-Type: application/json');
        $outputMessageArray=array(
            "status"=>"success",
            "data"=>$resArray
        );
        echo json_encode($outputMessageArray);
    }



    public function getall()
    {
        $tempData=$this->Reviews_model->get_all();

        echo $this->json($tempData);
    }




    public function create_error_messageArray($message){
        $tempMessageArray=array(
            "status"=>"error",
            "errorMessage"=>$message
        );
        return $tempMessageArray;
    }

    public function dataValidate($Data){
        if(empty($Data)){
            echo json_encode( $this->create_error_messageArray("Message Empty"));
            return 0;
        }
        else {

            //  goal_description can be empty
            if (empty($Data['review_rating'])) {
                echo json_encode($this->create_error_messageArray("review_rating Empty"));
                return 0;
            }elseif (empty($Data['review_user_id'])){
                echo json_encode($this->create_error_messageArray("review_user_id Empty"));
                return 0;
            }elseif (empty($Data['review_object_type'])){
                echo json_encode($this->create_error_messageArray("review_object_type Empty"));
                return 0;
            }elseif (empty($Data['review_object_id'])){
                echo json_encode($this->create_error_messageArray("review_object_id Empty"));
                return 0;
            }
            else {
                if (empty($Data['review_details'])) {
                    $Data['review_details'] = '';
                }
                $processArray = array(
                    'review_rating' => $Data['review_rating'],
                    'review_user_id' => $Data['review_user_id'],
                    'review_details' =>$Data['review_details'],
                    'review_object_type' =>$Data['review_object_type'],
                    'review_object_id' =>$Data['review_object_id'],

                );
                return $processArray;
            }
        }
    }



    public function read($id)
    {
        $row = $this->Reviews_model->get_by_id($id);

        if ($row) {
            $data = array(



                'review_id' => $row->review_id,
                'review_rating' => $row->review_rating,
                'review_details' => $row->review_details,
                'review_user_id' => $row->review_user_id,
                'review_object_type' => $row->review_object_type,
                'review_object_id' => $row->review_object_id,

                'review_user_first_name' =>$row->first_name,
                'review_user_last_name' =>$row->last_name,

            );
            $this->json($data);
        }
        else {
            $tempReturnArray=$this->create_error_messageArray('Record Not Found');
            echo json_encode($tempReturnArray);
        }

    }


    public function create()
    {
        $Data = json_decode(trim(file_get_contents('php://input')), true);

        $checkArray=$this->dataValidate($Data);
        if($checkArray!=0){
            $last_insert_id=$this->Reviews_model->insert($checkArray);
            $this->read($last_insert_id);
        }
    }


    public function update($id,$updateData)
    {
        $row = $this->Reviews_model->get_by_id($id);


        if ($row) {
            $processArray=$this->dataValidate($updateData);
            if($processArray!=0) {

                $data = array(
                    'review_rating' => $processArray['review_rating'],
                    'review_details' => $processArray['review_details'],
                    'review_user_id' => $processArray['review_user_id'],
                    'review_object_type' =>$processArray['review_object_type'],
                    'review_object_id' =>$processArray['review_object_id'],
                );
                $affectedRowsNumber = $this->Reviews_model->update($id, $data);

                $tempReturnArray = array(
                    "status" => 'success',
                    "affectRows" => $affectedRowsNumber
                );
                $this->json($tempReturnArray);
            }
        }
        else {

            $tempReturnArray=$this->create_error_messageArray('Record Not Found');
            echo json_encode($tempReturnArray);
        }





    }



    public function delete($id)
    {
        $row = $this->Reviews_model->get_by_id($id);

        if ($row) {
            $affectRow=$this->Reviews_model->delete($id);
            $tempReturnArray=array(
                "status"=>'success',
                "affectRows"=>$affectRow
            );
            $this->json($tempReturnArray);
        }
        else {
            //$this->session->set_flashdata('message', 'Record Not Found');
            $tempReturnArray=$this->create_error_messageArray('Record Not Found');
            echo json_encode($tempReturnArray);

        }

    }


    public function get_by_objective_id($id){
        $row = $this->Reviews_model->get_by_objective_id($id);

        if ($row) {
            $data = array(
                'review_id' => $row->review_id,
                'review_rating' => $row->review_rating,
                'review_details' => $row->review_details,
                'review_user_id' => $row->review_user_id,
                'review_object_type' => $row->review_object_type,
                'review_object_id' => $row->review_object_id,

                'review_user_first_name' =>$row->first_name,
                'review_user_last_name' =>$row->last_name,

            );
            $this->json($data);
        }
        else {
            $tempReturnArray=$this->create_error_messageArray('Record Not Found');
            echo json_encode($tempReturnArray);
        }


    }
    public function get_by_goal_id($id){
        $row = $this->Reviews_model->get_by_goal_id($id);

        if ($row) {
            $data = array(

                'review_id' => $row->review_id,
                'review_rating' => $row->review_rating,
                'review_details' => $row->review_details,
                'review_user_id' => $row->review_user_id,
                'review_object_type' => $row->review_object_type,
                'review_object_id' => $row->review_object_id,

                'review_user_first_name' =>$row->first_name,
                'review_user_last_name' =>$row->last_name,

            );
            $this->json($data);
        }
        else {
            $tempReturnArray=$this->create_error_messageArray('Record Not Found');
            echo json_encode($tempReturnArray);
        }


    }


}