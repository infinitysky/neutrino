<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 16/05/2017
 * Time: 10:13
 */
class System_setting extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('System_setting_model');
        header('Content-type: application/json');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }

    }


    function getall()
    {
        $tempData=$this->System_setting_model->get_all();

        echo $this->json($tempData);
    }




    public function index()
    {
        $this->getall();
    }

    function read($id)
    {


        $row = $this->System_setting_model->get_by_id($id);
        if ($row) {
            $data = array(
                'setting_id' => $row->setting_id,
                'setting_name' => $row->setting_name,
                'setting_value' => $row->setting_value,
            );
            $this->json($data);
        }
        else {
            $tempReturnArray=$this->create_error_messageArray('Record Not Found');
            echo json_encode($tempReturnArray);
        }

    }



    public function create()
    {
        $Data = json_decode(trim(file_get_contents('php://input')), true);

        $checkArray=$this->dataValidate($Data);
        if($checkArray!=0){
            $last_insert_id=$this->System_setting_model->insert($checkArray);
            $this->read($last_insert_id);
        }

    }



    function update($id,$updateData)
    {


        $row = $this->System_setting_model->get_by_id($id);

        if ($row) {
            $processArray=$this->dataValidate($updateData);
            if($processArray!=0) {
                $data = array(
                    'setting_name' => $processArray['setting_name'],
                    'setting_value' => $processArray['setting_value'],
                );
                $affectedRowsNumber = $this->System_setting_model->update($id, $data);

                $tempReturnArray = array(
                    "status" => 'success',
                    "affectRows" => $affectedRowsNumber
                );
                $this->json($tempReturnArray);
            }

        }
        else {

            $tempReturnArray=$this->create_error_messageArray('Record Not Found');
            echo json_encode($tempReturnArray);
        }

    }



    function delete($id)
    {

        $row = $this->System_setting_model->get_by_id($id);

        if ($row) {
            $affectRow=$this->System_setting_model->delete($id);
            $tempReturnArray=array(
                "status"=>'success',
                "affectRows"=>$affectRow
            );
            $this->json($tempReturnArray);
        }
        else {
            //$this->session->set_flashdata('message', 'Record Not Found');
            $tempReturnArray=$this->create_error_messageArray('Record Not Found');
            echo json_encode($tempReturnArray);

        }


    }





    //Main entrance
    public function items($id)
    {
        $Data = json_decode(trim(file_get_contents('php://input')), true);
        //GET, POST, OPTIONS, PUT, DELETE
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }elseif ($method == "GET"){

            $this->read($id);
        }elseif ($method == "PUT"){

            $this->update($id,$Data);
        }elseif ($method == "DELETE"){

            $this->delete($id);
        } else{

            die();

        }

    }


    function json($resArray) {
        header('Content-Type: application/json');
        $outputMessageArray=array(
            "status"=>"success",
            "data"=>$resArray
        );
        echo json_encode($outputMessageArray);
    }





    function create_error_messageArray($message){
        $tempMessageArray=array(
            "status"=>"error",
            "errorMessage"=>$message
        );
        return $tempMessageArray;
    }


    function dataValidate($Data){
        if(empty($Data)){
            echo json_encode( $this->create_error_messageArray("Message Empty"));
            return 0;
        }
        else {


            //  goal_description can be empty
            if (empty($Data['setting_name'])) {
                echo json_encode($this->create_error_messageArray("setting_name Empty"));
                return 0;
            }
            else {
                if (empty($Data['setting_value'])) {
                    $Data['setting_value'] = '';
                }


                $processArray = array(

                    'setting_name' => $Data['setting_name'],
                    'setting_value' => $Data['setting_value'],

                );
                return $processArray;
            }
        }
    }

    public function get_company_info(){

        $data = $this->System_setting_model->get_company_info();

        if ($data){
            $this->json($data);
        }

    }

    public function update_company_info(){
        $Data = json_decode(trim(file_get_contents('php://input')), true);

        if ($Data){

            $affectedRowsNumber = $this->System_setting_model->update_company_info($Data);

            $tempReturnArray = array(
                "status" => 'success',
                "affectRows" => $affectedRowsNumber
            );
            $this->json($tempReturnArray);

        }


    }


}