/*
Navicat MySQL Data Transfer

Source Server         : shapethefuture.info
Source Server Version : 50555
Source Host           : 127.0.0.1:3306
Source Database       : okrs

Target Server Type    : MYSQL
Target Server Version : 50555
File Encoding         : 65001

Date: 2017-05-24 15:53:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activities
-- ----------------------------
DROP TABLE IF EXISTS `activities`;
CREATE TABLE `activities` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_detail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `activity_comment` text,
  `activity_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `activity_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `activity_group` varchar(255) NOT NULL,
  `activity_group_id` int(11) unsigned zerofill DEFAULT NULL,
  `receiver_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`activity_id`),
  KEY `fk_activities_users` (`user_id`),
  CONSTRAINT `fk_activities_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=367 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activities
-- ----------------------------
INSERT INTO `activities` VALUES ('105', 'rrrr', null, 'note', '1', '2017-05-17 11:30:54', 'n', null, null);
INSERT INTO `activities` VALUES ('106', 'ppppp', null, 'note', '1', '2017-05-17 11:30:54', 'n', null, null);
INSERT INTO `activities` VALUES ('107', 'd', null, 'note', '1', '2017-05-17 11:30:54', 'n', null, null);
INSERT INTO `activities` VALUES ('108', '888', null, 'note', '1', '2017-05-17 11:30:54', 'n', null, null);
INSERT INTO `activities` VALUES ('109', '000', null, 'note', '1', '2017-05-17 11:30:54', 'n', null, null);
INSERT INTO `activities` VALUES ('110', ' Updated a new Key Result Progress Status :', null, 'Updated', '1', '2017-05-17 11:30:54', 'o', '00000000050', null);
INSERT INTO `activities` VALUES ('111', ' Created a new Objective : add tem objective', null, 'Create', '1', '2017-05-17 11:30:54', 'o', '00000000050', null);
INSERT INTO `activities` VALUES ('112', 'Updated Objective : add tem objective update log : ', null, 'Update', '1', '2017-05-17 11:30:54', 'o', '00000000050', null);
INSERT INTO `activities` VALUES ('113', ' Created a new Key Result :ddd', null, 'Create', '1', '2017-05-17 11:30:54', 'k', '00000000031', null);
INSERT INTO `activities` VALUES ('114', ' Updated a new Key Result Progress Status :', null, 'Updated', '1', '2017-05-17 11:30:54', 'k', '00000000031', null);
INSERT INTO `activities` VALUES ('115', ' Updated a new Key Result Progress Status :', null, 'Updated', '1', '2017-05-17 11:30:54', 'k', '00000000031', null);
INSERT INTO `activities` VALUES ('116', ' Updated a new Key Result Progress Status :', null, 'Updated', '1', '2017-05-17 11:30:54', 'k', '00000000031', null);
INSERT INTO `activities` VALUES ('117', ' Created a new goal : Not one', null, 'Create', '1', '2017-05-17 11:30:54', 'g', null, null);
INSERT INTO `activities` VALUES ('118', ' Created a new Objective : 111', null, 'Create', '1', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('119', ' Created a new Key Result :22222', null, 'Create', '1', '2017-05-17 11:30:54', 'k', null, null);
INSERT INTO `activities` VALUES ('120', ' Created a new Objective : 2333', null, 'Create', '1', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('121', ' Created a new Objective : 222', null, 'Create', '1', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('122', ' Created a new Objective : 33', null, 'Create', '1', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('123', ' Created a new Key Result :776', null, 'Create', '1', '2017-05-17 11:30:54', 'k', null, null);
INSERT INTO `activities` VALUES ('124', ' Updated a new Key Result Progress Status :', null, 'Update', '1', '2017-05-17 11:30:54', 'k', null, null);
INSERT INTO `activities` VALUES ('125', ' Updated a new Key Result Progress Status :ds', null, 'Update', '1', '2017-05-17 11:30:54', 'k', null, null);
INSERT INTO `activities` VALUES ('126', 'Updated Objective : 33 update log : ', null, 'Update', '105', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('127', ' Created a new Objective : 2312312', null, 'Create', '105', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('128', ' Created a new Key Result :1123', null, 'Create', '105', '2017-05-17 11:30:54', 'k', null, null);
INSERT INTO `activities` VALUES ('129', ' Updated a new Key Result Progress Status :', null, 'Update', '105', '2017-05-17 11:30:54', 'k', null, null);
INSERT INTO `activities` VALUES ('130', ' Created a new Objective : 中文测试', null, 'Create', '105', '2017-05-17 11:30:54', 'k', null, null);
INSERT INTO `activities` VALUES ('131', 'Updated Objective : 中文测试 update log : ', null, 'Update', '105', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('132', ' Created a new Objective : 中文测试', null, 'Create', '105', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('133', ' Created a new Objective : 反反复复', null, 'Create', '105', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('134', ' Created a new Objective : ADASda', null, 'Create', '105', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('135', ' Created a new Objective : fdsafsdaf', null, 'Create', '105', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('136', ' Created a new Objective : Objective for team A', null, 'Create', '105', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('137', ' Updated a new Key Result Progress Status :', null, 'Update', '105', '2017-05-17 11:30:54', 'k', null, null);
INSERT INTO `activities` VALUES ('138', ' Created a new Objective : All Team Objectives 1', null, 'Create', '1', '2017-05-17 11:30:54', 'o', null, null);
INSERT INTO `activities` VALUES ('139', ' Created a new Key Result : Key Result', null, 'Create', '105', '2017-05-17 11:30:54', 'k', null, null);
INSERT INTO `activities` VALUES ('140', ' Created a new Key Result :All Team Objectives 1', null, 'Create', '105', '2017-05-17 11:30:54', 'k', null, null);
INSERT INTO `activities` VALUES ('141', ' Updated a new Key Result Progress Status :', null, 'Update', '105', '2017-05-17 11:30:54', 'k', null, null);
INSERT INTO `activities` VALUES ('142', 'add note ', null, 'Create', '105', '2017-05-17 11:30:54', 'n', null, '1');
INSERT INTO `activities` VALUES ('143', 'Created a new Objective w', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000054', '0');
INSERT INTO `activities` VALUES ('144', 'Created a new Key Result 2', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000032', '0');
INSERT INTO `activities` VALUES ('145', 'Created a new Key Result 123', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000033', '0');
INSERT INTO `activities` VALUES ('146', 'Deleted Objective : rr ( Objective ID: 53)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('147', 'Deleted Objective : w ( Objective ID: 54)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('148', 'Deleted Objective : 54555 ( Objective ID: 52)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('149', 'Deleted Objective : etert ( Objective ID: 51)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('150', 'Created a new Key Result fdsa', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000034', '0');
INSERT INTO `activities` VALUES ('151', 'Created a new Key Result 4434', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000035', '0');
INSERT INTO `activities` VALUES ('152', 'Deleted Objective : fdsa ( Objective ID: 50)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('153', 'Created a new Objective fdsgdsfgdfgdfsg', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000055', '0');
INSERT INTO `activities` VALUES ('154', 'Created a new Objective 123', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000056', '0');
INSERT INTO `activities` VALUES ('155', 'Deleted Objective : 123 ( Objective ID: 56)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('156', 'Created a new Objective 12312312312', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000057', '0');
INSERT INTO `activities` VALUES ('157', 'Created a new Objective 43242342', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000058', '0');
INSERT INTO `activities` VALUES ('158', 'Created a new Objective 5435345', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000062', '0');
INSERT INTO `activities` VALUES ('159', 'Created a new Objective 543534', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000063', '0');
INSERT INTO `activities` VALUES ('160', 'Created a new Objective fdsa', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000064', '0');
INSERT INTO `activities` VALUES ('161', 'Created a new Objective rew', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000065', '0');
INSERT INTO `activities` VALUES ('162', 'Change Objective tag to: Risk; ', null, 'Update', '105', '2017-05-17 11:30:54', 'o', '00000000064', '0');
INSERT INTO `activities` VALUES ('163', 'Created a new Key Result 55', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000036', '0');
INSERT INTO `activities` VALUES ('164', 'Change Key Result name to: 44;   Change Key Result description to: 44;  Change Key Result progress status From 55 To: 44;  ', null, 'Update', '105', '2017-05-17 11:30:54', 'k', '00000000036', '0');
INSERT INTO `activities` VALUES ('165', 'Change Objective tag to: Warning; ', null, 'Update', '105', '2017-05-17 11:30:54', 'o', '00000000065', '0');
INSERT INTO `activities` VALUES ('166', 'Deleted Objective : dsfasdfsad ( Objective ID: 60)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('167', 'Deleted Objective : rew ( Objective ID: 65)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('168', 'Deleted Objective : fdsa ( Objective ID: 64)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('169', 'Deleted Objective : 543534 ( Objective ID: 63)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('170', 'Deleted Objective : 5435345 ( Objective ID: 62)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('171', 'Deleted Objective : dsfasdfsad ( Objective ID: 61)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('172', 'Deleted Objective : dsfasdfsad ( Objective ID: 59)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('173', 'Deleted Objective : 43242342 ( Objective ID: 58)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('174', 'Deleted Objective : 12312312312 ( Objective ID: 57)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('175', 'Deleted Objective : fdsgdsfgdfgdfsg ( Objective ID: 55)', null, 'Delete', '105', '2017-05-17 11:30:54', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('176', 'Created a new Objective 123412341234', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000066', '0');
INSERT INTO `activities` VALUES ('177', 'Created a new Key Result 1234', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000037', '0');
INSERT INTO `activities` VALUES ('178', 'Created a new Key Result 555', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000038', '0');
INSERT INTO `activities` VALUES ('179', 'Created a new Key Result 6666', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000039', '0');
INSERT INTO `activities` VALUES ('180', 'Created a new Key Result 4764445', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000040', '0');
INSERT INTO `activities` VALUES ('181', 'Created a new Objective Reviewed', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000067', '0');
INSERT INTO `activities` VALUES ('182', 'Change Objective tag to: Complete; ', null, 'Update', '105', '2017-05-17 11:30:54', 'o', '00000000067', '0');
INSERT INTO `activities` VALUES ('183', 'Created a new Objective 123', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000068', '0');
INSERT INTO `activities` VALUES ('184', 'Created a new Objective 123', null, 'Create', '105', '2017-05-17 11:30:54', 'o', '00000000069', '0');
INSERT INTO `activities` VALUES ('185', 'Created a new Key Result reviewd', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000041', '0');
INSERT INTO `activities` VALUES ('186', 'Created a new Key Result reviewed2', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000042', '0');
INSERT INTO `activities` VALUES ('187', 'Created a new Key Result reviewed 3', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000043', '0');
INSERT INTO `activities` VALUES ('188', 'Change Key Result progress status From 1 To: 0;  ', null, 'Update', '105', '2017-05-17 11:30:54', 'k', '00000000041', '0');
INSERT INTO `activities` VALUES ('189', 'Created a new Key Result reviewed 4', null, 'Create', '105', '2017-05-17 11:30:54', 'k', '00000000044', '0');
INSERT INTO `activities` VALUES ('190', 'Change Key Result progress status From  69 to 81', 'I like it very much!', 'Update', '3', '2017-05-17 11:30:54', 'k', '00000000043', '0');
INSERT INTO `activities` VALUES ('191', 'Deleted Objective : 123 ( Objective ID: 69)', '', 'Delete', '105', '2017-05-17 11:29:32', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('192', 'Update Progress From 70% to 79%', 'Good Try', 'Update', '105', '2017-05-17 11:29:51', 'k', '00000000043', '0');
INSERT INTO `activities` VALUES ('193', 'Update Progress From 79% to 66%', 'Yryy', 'Update', '105', '2017-05-17 11:31:12', 'k', '00000000043', '0');
INSERT INTO `activities` VALUES ('194', 'Update Key Result Progress From 66% to 79%', '21`21`12', 'Update', '105', '2017-05-17 11:33:31', 'k', '00000000043', '0');
INSERT INTO `activities` VALUES ('195', 'Update Key Result Progress From 0% to 58%', '5555', 'Update', '105', '2017-05-17 11:33:36', 'k', '00000000041', '0');
INSERT INTO `activities` VALUES ('196', 'Created a new Objective 432', '', 'Create', '105', '2017-05-17 11:35:33', 'o', '00000000070', '0');
INSERT INTO `activities` VALUES ('197', 'Created a new Key Result rrr', '', 'Create', '105', '2017-05-17 11:35:40', 'k', '00000000045', '0');
INSERT INTO `activities` VALUES ('198', 'Update Key Result Progress From 0% to 24%', 'New test for update to 25%', 'Update', '105', '2017-05-17 11:41:01', 'k', '00000000045', '0');
INSERT INTO `activities` VALUES ('199', 'Created a new Key Result 33', '', 'Create', '105', '2017-05-17 11:41:17', 'k', '00000000046', '0');
INSERT INTO `activities` VALUES ('200', 'Update Key Result Progress From 0% to 37%', '35', 'Update', '105', '2017-05-17 11:41:28', 'k', '00000000044', '0');
INSERT INTO `activities` VALUES ('201', 'Update Key Result Progress From 0% to 39%', '654645', 'Update', '105', '2017-05-17 12:29:19', 'k', '00000000046', '0');
INSERT INTO `activities` VALUES ('202', 'Update Key Result Progress From 44% to 29%', '34534534', 'Update', '105', '2017-05-17 12:29:55', 'k', '00000000035', '0');
INSERT INTO `activities` VALUES ('203', 'Update Key Result Progress From 0% to 50%', '345345345', 'Update', '105', '2017-05-17 12:30:00', 'k', '00000000046', '0');
INSERT INTO `activities` VALUES ('204', 'Update Key Result Progress From 0% to 26%', '52343242', 'Update', '105', '2017-05-17 12:30:41', 'k', '00000000030', '0');
INSERT INTO `activities` VALUES ('205', 'Created a new Objective 234', '', 'Create', '105', '2017-05-17 12:34:35', 'o', '00000000071', '0');
INSERT INTO `activities` VALUES ('206', 'Created a new Objective 32123', '', 'Create', '105', '2017-05-17 12:34:57', 'o', '00000000072', '0');
INSERT INTO `activities` VALUES ('207', 'Created a new Key Result 321312', '', 'Create', '105', '2017-05-17 12:35:02', 'k', '00000000047', '0');
INSERT INTO `activities` VALUES ('208', 'Created a new Key Result 123', '', 'Create', '105', '2017-05-17 12:35:06', 'k', '00000000048', '0');
INSERT INTO `activities` VALUES ('209', 'Created a new Key Result 12312312', '', 'Create', '105', '2017-05-17 12:35:10', 'k', '00000000049', '0');
INSERT INTO `activities` VALUES ('210', 'Created a new Key Result 3334', '', 'Create', '105', '2017-05-17 12:35:14', 'k', '00000000050', '0');
INSERT INTO `activities` VALUES ('211', 'Update Key Result Progress From 24% to 67%', '', 'Update', '105', '2017-05-17 12:35:30', 'k', '00000000045', '0');
INSERT INTO `activities` VALUES ('212', 'Update Key Result Progress From 0% to 59%', '', 'Update', '105', '2017-05-17 12:36:26', 'k', '00000000039', '0');
INSERT INTO `activities` VALUES ('213', 'Created a new Key Result 45555', '', 'Create', '105', '2017-05-17 12:37:01', 'k', '00000000051', '0');
INSERT INTO `activities` VALUES ('214', 'Created a new Key Result 444', '', 'Create', '105', '2017-05-17 12:37:08', 'k', '00000000052', '0');
INSERT INTO `activities` VALUES ('215', 'Created a new Key Result dasdasd', '', 'Create', '105', '2017-05-17 12:37:51', 'k', '00000000053', '0');
INSERT INTO `activities` VALUES ('216', 'Update Key Result Progress From 0% to 49%', '', 'Update', '105', '2017-05-17 12:41:51', 'k', '00000000053', '0');
INSERT INTO `activities` VALUES ('217', 'Update Key Result Progress From 0% to 69%', '', 'Update', '105', '2017-05-17 12:41:56', 'k', '00000000051', '0');
INSERT INTO `activities` VALUES ('218', 'Update Key Result Progress From 36% to 84%', '', 'Update', '105', '2017-05-17 12:41:59', 'k', '00000000052', '0');
INSERT INTO `activities` VALUES ('219', 'Update Key Result Progress From 0% to 83%', '', 'Update', '105', '2017-05-17 12:42:02', 'k', '00000000053', '0');
INSERT INTO `activities` VALUES ('220', 'Update Key Result Progress From 0% to 85%', '', 'Update', '105', '2017-05-17 12:42:08', 'k', '00000000051', '0');
INSERT INTO `activities` VALUES ('221', 'Update Key Result Progress From 67% to 83%', '', 'Update', '105', '2017-05-17 12:42:12', 'k', '00000000049', '0');
INSERT INTO `activities` VALUES ('222', 'Update Key Result Progress From 72% to 87%', '', 'Update', '105', '2017-05-17 12:42:16', 'k', '00000000048', '0');
INSERT INTO `activities` VALUES ('223', 'Update Key Result Progress From 70% to 88%', '', 'Update', '105', '2017-05-17 12:42:20', 'k', '00000000047', '0');
INSERT INTO `activities` VALUES ('224', 'Update Key Result Progress From 72% to 88%', '', 'Update', '105', '2017-05-17 12:42:28', 'k', '00000000048', '0');
INSERT INTO `activities` VALUES ('225', 'Update Key Result Progress From 67% to 88%', '', 'Update', '105', '2017-05-17 12:42:32', 'k', '00000000049', '0');
INSERT INTO `activities` VALUES ('226', 'Update Key Result Progress From 36% to 88%', '', 'Update', '105', '2017-05-17 12:42:40', 'k', '00000000052', '0');
INSERT INTO `activities` VALUES ('227', 'Update Key Result Progress From 0% to 88%', '', 'Update', '105', '2017-05-17 12:42:46', 'k', '00000000051', '0');
INSERT INTO `activities` VALUES ('228', 'Update Key Result Progress From 0% to 88%', '', 'Update', '105', '2017-05-17 12:42:50', 'k', '00000000053', '0');
INSERT INTO `activities` VALUES ('229', 'Update Key Result Progress From 50% to 0%', '', 'Update', '105', '2017-05-17 12:43:05', 'k', '00000000046', '0');
INSERT INTO `activities` VALUES ('230', 'Update Key Result Progress From 67% to 50%', '', 'Update', '105', '2017-05-17 12:43:11', 'k', '00000000045', '0');
INSERT INTO `activities` VALUES ('231', 'Update Key Result Progress From 0% to 35%', '', 'Update', '105', '2017-05-17 12:43:27', 'k', '00000000038', '0');
INSERT INTO `activities` VALUES ('232', 'Update Key Result Progress From 4% to 50%', '', 'Update', '105', '2017-05-17 12:43:34', 'k', '00000000040', '0');
INSERT INTO `activities` VALUES ('233', 'Deleted Objective : Reviewed ( Objective ID: 67)', '', 'Delete', '105', '2017-05-17 12:43:44', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('234', 'Deleted Objective : 123412341234 ( Objective ID: 66)', '', 'Delete', '105', '2017-05-17 12:43:46', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('235', 'Created a new Objective 12', '', 'Create', '105', '2017-05-17 12:44:11', 'o', '00000000073', '0');
INSERT INTO `activities` VALUES ('236', 'Created a new Objective 13', '', 'Create', '105', '2017-05-17 13:18:23', 'o', '00000000074', '0');
INSERT INTO `activities` VALUES ('237', 'Created a new Objective 14', '', 'Create', '105', '2017-05-17 13:19:02', 'o', '00000000075', '0');
INSERT INTO `activities` VALUES ('238', 'Deleted Objective : 14 ( Objective ID: 75)', '', 'Delete', '105', '2017-05-17 13:19:16', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('239', 'Deleted Objective : 13 ( Objective ID: 74)', '', 'Delete', '105', '2017-05-17 13:19:19', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('240', 'Created a new Objective 133', '', 'Create', '105', '2017-05-17 13:22:04', 'o', '00000000076', '0');
INSERT INTO `activities` VALUES ('241', 'Deleted Objective : 133 ( Objective ID: 76)', '', 'Delete', '105', '2017-05-17 13:22:20', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('242', 'Deleted Objective : 12 ( Objective ID: 73)', '', 'Delete', '105', '2017-05-17 13:22:22', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('243', 'Deleted Objective : All Team Objectives 1 ( Objective ID: 49)', '', 'Delete', '105', '2017-05-17 13:22:25', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('244', 'Deleted Objective : objectives ` ( Objective ID: 41)', '', 'Delete', '105', '2017-05-17 13:22:28', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('245', 'Deleted Objective : 2333 ( Objective ID: 38)', '', 'Delete', '105', '2017-05-17 13:22:31', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('246', 'Deleted Objective : objectives 3 ( Objective ID: 47)', '', 'Delete', '105', '2017-05-17 13:22:34', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('247', 'Deleted Objective : Objective for team A ( Objective ID: 48)', '', 'Delete', '105', '2017-05-17 13:22:37', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('248', 'Deleted Objective : team 24 ( Objective ID: 28)', '', 'Delete', '105', '2017-05-17 13:22:41', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('249', 'Change Objective tag to: None; ', '', 'Update', '105', '2017-05-17 13:23:06', 'o', '00000000072', '0');
INSERT INTO `activities` VALUES ('250', 'Reviewed Objective 32123', '', 'Review', '105', '2017-05-17 15:54:56', 'o', '00000000072', '0');
INSERT INTO `activities` VALUES ('251', 'Updated reviewed Info for Objective 32123', '', 'Updated', '105', '2017-05-17 16:01:36', 'o', '00000000072', '0');
INSERT INTO `activities` VALUES ('252', 'Updated reviewed Info for Objective 32123', '', 'Updated', '105', '2017-05-17 16:08:56', 'o', '00000000072', '0');
INSERT INTO `activities` VALUES ('253', 'Reviewed Objective 234', '', 'Review', '105', '2017-05-17 16:09:06', 'o', '00000000071', '0');
INSERT INTO `activities` VALUES ('254', 'Reviewed Objective 432', '', 'Review', '105', '2017-05-17 16:09:21', 'o', '00000000070', '0');
INSERT INTO `activities` VALUES ('255', 'Updated reviewed Info for Objective 32123', '', 'Updated', '105', '2017-05-17 16:10:00', 'o', '00000000072', '0');
INSERT INTO `activities` VALUES ('256', 'Created a new Objective 23424234', '', 'Create', '105', '2017-05-17 17:21:14', 'o', '00000000077', '0');
INSERT INTO `activities` VALUES ('257', 'Created a new Key Result ewrt', '', 'Create', '105', '2017-05-17 17:25:03', 'k', '00000000054', '0');
INSERT INTO `activities` VALUES ('258', 'Created a new Objective new', '', 'Create', '105', '2017-05-17 17:34:00', 'o', '00000000078', '0');
INSERT INTO `activities` VALUES ('259', 'Created a new Objective eerer', '', 'Create', '105', '2017-05-17 17:34:27', 'o', '00000000079', '0');
INSERT INTO `activities` VALUES ('260', 'Created a new Objective fdsafd', '', 'Create', '105', '2017-05-17 17:48:02', 'o', '00000000080', '0');
INSERT INTO `activities` VALUES ('261', 'Deleted Objective : fdsafd ( Objective ID: 80)', '', 'Delete', '105', '2017-05-17 17:48:13', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('262', 'Deleted Objective : eerer ( Objective ID: 79)', '', 'Delete', '105', '2017-05-17 17:48:15', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('263', 'Deleted Objective : 23424234 ( Objective ID: 77)', '', 'Delete', '105', '2017-05-17 17:48:19', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('264', 'Deleted Objective : new ( Objective ID: 78)', '', 'Delete', '105', '2017-05-17 17:48:22', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('265', 'Created a new Objective team blue', '', 'Create', '105', '2017-05-17 17:48:38', 'o', '00000000081', '0');
INSERT INTO `activities` VALUES ('266', 'Created a new Key Result dsafsda', '', 'Create', '105', '2017-05-17 17:48:45', 'k', '00000000055', '0');
INSERT INTO `activities` VALUES ('267', 'Update Key Result Progress From 60% to 72%', '', 'Update', '105', '2017-05-17 17:49:54', 'k', '00000000055', '0');
INSERT INTO `activities` VALUES ('268', 'Created a new Key Result 456465', '', 'Create', '105', '2017-05-17 17:50:09', 'k', '00000000056', '0');
INSERT INTO `activities` VALUES ('269', 'Created a new Objective 4564564', '', 'Create', '105', '2017-05-17 17:50:24', 'o', '00000000082', '0');
INSERT INTO `activities` VALUES ('270', 'Created a new Objective 123132123132', '', 'Create', '105', '2017-05-17 17:50:39', 'o', '00000000083', '0');
INSERT INTO `activities` VALUES ('271', 'Created a new Key Result 456465', '', 'Create', '105', '2017-05-17 17:51:00', 'k', '00000000057', '0');
INSERT INTO `activities` VALUES ('272', 'Update Key Result Progress From 0% to 45%', '', 'Update', '105', '2017-05-17 17:54:15', 'k', '00000000057', '0');
INSERT INTO `activities` VALUES ('273', 'Update Key Result Progress From 45% to 61%', '432423423', 'Update', '105', '2017-05-19 09:16:44', 'k', '00000000057', '0');
INSERT INTO `activities` VALUES ('274', 'Update Key Result Progress From 61% to 74%', 'I fdlsajfkdlsaj f4aj fdsoiajf dijafdk jafkl; j4ekal; jedfioa pj43fk ;jafi sdjfk dsajfk e;aiof; jdaskl;f eaw dsfda', 'Update', '105', '2017-05-19 09:22:38', 'k', '00000000057', '0');
INSERT INTO `activities` VALUES ('275', 'Created a new Objective trewtrewert', '', 'Create', '105', '2017-05-19 09:40:03', 'o', '00000000084', '0');
INSERT INTO `activities` VALUES ('276', 'Created a new Objective 423423', '', 'Create', '105', '2017-05-19 09:40:21', 'o', '00000000085', '0');
INSERT INTO `activities` VALUES ('277', 'Created a new Key Result trewtrewert', '', 'Create', '105', '2017-05-19 09:40:27', 'k', '00000000058', '0');
INSERT INTO `activities` VALUES ('278', 'Created a new Key Result trewtrewert', '', 'Create', '105', '2017-05-19 09:40:30', 'k', '00000000059', '0');
INSERT INTO `activities` VALUES ('279', 'Created a new Key Result trewtrewert', '', 'Create', '105', '2017-05-19 09:40:41', 'k', '00000000060', '0');
INSERT INTO `activities` VALUES ('280', 'Created a new Key Result trewtrewert', '', 'Create', '105', '2017-05-19 09:40:45', 'k', '00000000061', '0');
INSERT INTO `activities` VALUES ('281', 'Created a new Key Result 32423423', '', 'Create', '105', '2017-05-19 09:43:50', 'k', '00000000062', '0');
INSERT INTO `activities` VALUES ('282', 'Created a new Key Result ertert', '', 'Create', '105', '2017-05-19 09:46:20', 'k', '00000000063', '0');
INSERT INTO `activities` VALUES ('283', 'Created a new Key Result trewtrw', '', 'Create', '105', '2017-05-19 09:50:16', 'k', '00000000064', '0');
INSERT INTO `activities` VALUES ('284', 'Update Key Result Progress From 42% to 42%', '345345345', 'Update', '105', '2017-05-19 09:50:22', 'k', '00000000064', '0');
INSERT INTO `activities` VALUES ('285', 'Update Key Result Progress From 42% to 69%', '666', 'Update', '105', '2017-05-19 09:51:58', 'k', '00000000064', '0');
INSERT INTO `activities` VALUES ('286', 'Created a new Key Result 54454', '', 'Create', '105', '2017-05-19 09:52:11', 'k', '00000000065', '0');
INSERT INTO `activities` VALUES ('287', 'Update Key Result Progress From 30% to 30%', '', 'Update', '105', '2017-05-19 09:52:21', 'k', '00000000065', '0');
INSERT INTO `activities` VALUES ('288', 'Created a new Key Result 444', '', 'Create', '105', '2017-05-19 09:54:10', 'k', '00000000066', '0');
INSERT INTO `activities` VALUES ('289', 'Update Key Result Progress From 32% to 32%', '444', 'Update', '105', '2017-05-19 09:54:17', 'k', '00000000066', '0');
INSERT INTO `activities` VALUES ('290', 'Created a new Key Result 543534534', '', 'Create', '105', '2017-05-19 09:58:44', 'k', '00000000067', '0');
INSERT INTO `activities` VALUES ('291', 'Created a new Key Result 666', '', 'Create', '105', '2017-05-19 09:58:51', 'k', '00000000068', '0');
INSERT INTO `activities` VALUES ('292', 'Update Key Result Progress From 35% to 35%', '0-35', 'Update', '105', '2017-05-19 09:59:05', 'k', '00000000068', '0');
INSERT INTO `activities` VALUES ('293', 'Update Key Result Progress From 54% to 54%', '0-54', 'Update', '105', '2017-05-19 09:59:24', 'k', '00000000067', '0');
INSERT INTO `activities` VALUES ('294', 'Created a new Key Result erwerw', '', 'Create', '105', '2017-05-19 10:00:13', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('295', 'Update Key Result Progress From 0% to 49%', 'rrr', 'Update', '105', '2017-05-19 10:00:21', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('296', 'Created a new Key Result 666', '', 'Create', '105', '2017-05-19 10:02:23', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('297', 'Update Key Result Progress From 0% to 68%', '0-68', 'Update', '105', '2017-05-19 10:02:30', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('298', 'Update Key Result Progress From 68% to 54%', '222', 'Update', '105', '2017-05-19 10:08:06', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('299', 'Update Key Result Progress From 68% to 74%', '2222', 'Update', '105', '2017-05-19 10:08:25', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('300', 'Update Key Result Progress From 74% to 50%', 'to 50', 'Update', '105', '2017-05-19 10:13:27', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('301', 'Update Key Result Progress From 74% to 75%', '50-75', 'Update', '105', '2017-05-19 10:13:42', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('302', 'Update Key Result Progress From 75% to 51%', '', 'Update', '105', '2017-05-19 10:14:18', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('303', 'Update Key Result Progress From 75% to 82%', '51-82', 'Update', '105', '2017-05-19 10:14:29', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('304', 'Update Key Result Progress From 82% to 24%', '4444', 'Update', '105', '2017-05-19 10:15:20', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('305', 'Update Key Result Progress From 82% to 80%', '24-80', 'Update', '105', '2017-05-19 10:15:32', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('306', 'Update Key Result Progress From 80% to 24%', '80-24', 'Update', '105', '2017-05-19 10:17:24', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('307', 'Update Key Result Progress From 24% to 56%', '24-56', 'Update', '105', '2017-05-19 10:20:46', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('308', 'Update Key Result Progress From 24% to 27%', '56-27', 'Update', '105', '2017-05-19 10:20:57', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('309', 'Update Key Result Progress From 27% to 64%', '27-64', 'Update', '105', '2017-05-19 10:21:46', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('310', 'Update Key Result Progress From 27% to 26%', '64-26', 'Update', '105', '2017-05-19 10:22:00', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('311', 'Update Key Result Progress From 26% to 46%', '27-46', 'Update', '105', '2017-05-19 10:27:02', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('312', 'Update Key Result Progress From 26% to 20%', '46-20', 'Update', '105', '2017-05-19 10:27:13', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('313', 'Update Key Result Progress From 20% to 58%', '20-58', 'Update', '105', '2017-05-19 10:31:17', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('314', 'Update Key Result Progress From 20% to 77%', '58-77', 'Update', '105', '2017-05-19 10:31:25', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('315', 'Update Key Result Progress From 77% to 43%', '77-58', 'Update', '105', '2017-05-19 10:33:14', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('316', 'Update Key Result Progress From 77% to 24%', '43-24', 'Update', '105', '2017-05-19 10:33:26', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('317', 'Update Key Result Progress From 24% to 44%', '24-44', 'Update', '105', '2017-05-19 10:34:13', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('318', 'Update Key Result Progress From 44% to 75%', '44-75', 'Update', '105', '2017-05-19 10:34:46', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('319', 'Update Key Result Progress From 44% to 38%', '75-38', 'Update', '105', '2017-05-19 10:35:02', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('320', 'Update Key Result Progress From 63% to 63%', '38-63', 'Update', '105', '2017-05-19 10:36:00', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('321', 'Update Key Result Progress From 44% to 44%', '63-44', 'Update', '105', '2017-05-19 10:36:33', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('322', 'Update Key Result Progress From 66% to 66%', '77', 'Update', '105', '2017-05-19 10:38:21', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('323', 'Change Key Result name to: 666888;  ', '', 'Update', '105', '2017-05-19 10:40:10', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('324', 'Change Key Result name From 666888 to: 111;  ', '', 'Update', '105', '2017-05-19 10:41:18', 'k', '00000000070', '0');
INSERT INTO `activities` VALUES ('325', 'Deleted Objective : 111 ( Key Result ID: 70)', '', 'Delete', '105', '2017-05-19 10:41:33', 'o', '00000000085', '0');
INSERT INTO `activities` VALUES ('326', ' Change Key Result description to: 66677;  ', '', 'Update', '105', '2017-05-19 10:44:39', 'k', '00000000068', '0');
INSERT INTO `activities` VALUES ('327', 'Change Key Result name From erwerw to: 123123;   Change Key Result description to: 123444;  ', '', 'Update', '105', '2017-05-19 10:47:47', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('328', 'Update Key Result Progress From 65% to 65%', '49-65', 'Update', '105', '2017-05-19 10:49:22', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('329', 'Update Key Result Progress From 65% to 45%', '65-45', 'Update', '105', '2017-05-19 10:50:52', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('330', 'Update Key Result Progress From 65% to 70%', '45-70', 'Update', '105', '2017-05-19 10:51:22', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('331', 'Update Key Result Progress From 70% to 40%', '70-40', 'Update', '105', '2017-05-19 10:56:11', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('332', 'Update Key Result Progress From 70% to 74%', '40-74', 'Update', '105', '2017-05-19 10:56:25', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('333', 'Update Key Result Progress From 70% to 23%', '74-23', 'Update', '105', '2017-05-19 11:10:16', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('334', 'Update Key Result Progress From 64% to 64%', '23-64', 'Update', '105', '2017-05-19 11:13:12', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('335', 'Update Key Result Progress From 47% to 47%', '25', 'Update', '105', '2017-05-19 11:14:30', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('336', 'Update Key Result Progress From 47% to 76%', '46-76', 'Update', '105', '2017-05-19 11:20:45', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('337', 'Update Key Result Progress From 76% to 46%', '76-46', 'Update', '105', '2017-05-19 11:24:16', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('338', 'Update Key Result Progress From 46% to 79%', '46-79', 'Update', '105', '2017-05-19 11:24:34', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('339', 'Update Key Result Progress From 79% to 53%', '79-53', 'Update', '105', '2017-05-19 11:25:30', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('340', 'Update Key Result Progress From 53% to 32%', '53-32', 'Update', '105', '2017-05-19 11:25:39', 'k', '00000000069', '0');
INSERT INTO `activities` VALUES ('341', 'Deleted Objective : trewtrewert ( Objective ID: 84)', '', 'Delete', '105', '2017-05-19 11:26:59', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('342', 'Created a new Objective 432423', '', 'Create', '105', '2017-05-19 12:22:42', 'o', '00000000086', '0');
INSERT INTO `activities` VALUES ('343', 'Deleted Objective : 423423 ( Objective ID: 85)', '', 'Delete', '105', '2017-05-19 12:23:40', 'g', '00000000014', '0');
INSERT INTO `activities` VALUES ('344', 'Created a new Key Result 4324234', '', 'Create', '105', '2017-05-19 12:23:57', 'k', '00000000071', '0');
INSERT INTO `activities` VALUES ('345', 'Change Objective Team to: Sub-team of Yellow; ', '', 'Update', '105', '2017-05-19 12:26:42', 'o', '00000000086', '0');
INSERT INTO `activities` VALUES ('346', 'Update Key Result Progress From 0% to 35%', '9698654', 'Update', '105', '2017-05-19 12:27:22', 'k', '00000000071', '0');
INSERT INTO `activities` VALUES ('347', 'Update Key Result Progress From 35% to 46%', '46', 'Update', '105', '2017-05-19 12:27:33', 'k', '00000000071', '0');
INSERT INTO `activities` VALUES ('348', 'Created a new Objective ffffds', '', 'Create', '105', '2017-05-19 12:59:07', 'o', '00000000087', '0');
INSERT INTO `activities` VALUES ('349', 'Created a new Key Result fdsfs', '', 'Create', '105', '2017-05-19 12:59:11', 'k', '00000000072', '0');
INSERT INTO `activities` VALUES ('350', 'Update Key Result Progress From 0% to 43%', 'r444', 'Update', '105', '2017-05-19 12:59:15', 'k', '00000000072', '0');
INSERT INTO `activities` VALUES ('351', 'Created a new Objective Accelerate recurring net profit growth', '', 'Create', '1', '2017-05-22 14:22:35', 'o', '00000000088', '0');
INSERT INTO `activities` VALUES ('352', 'Created a new Objective Accelerate recurring net profit growth', '', 'Create', '1', '2017-05-22 14:22:35', 'o', '00000000089', '0');
INSERT INTO `activities` VALUES ('353', 'Deleted Objective : Accelerate recurring net profit growth ( Objective ID: 88)', '', 'Delete', '1', '2017-05-22 14:22:42', 'g', '00000000018', '0');
INSERT INTO `activities` VALUES ('354', 'Created a new Key Result Recurring contract net profit to $50,000 per month', '', 'Create', '1', '2017-05-22 14:29:46', 'k', '00000000073', '0');
INSERT INTO `activities` VALUES ('355', 'Update Key Result Progress From 0% to 16%', '', 'Update', '1', '2017-05-22 14:30:10', 'k', '00000000073', '0');
INSERT INTO `activities` VALUES ('356', 'Update Key Result Progress From 16% to 0%', '', 'Update', '1', '2017-05-22 14:30:56', 'k', '00000000073', '0');
INSERT INTO `activities` VALUES ('357', 'Created a new Key Result Enhance', '', 'Create', '1', '2017-05-22 14:33:19', 'k', '00000000074', '0');
INSERT INTO `activities` VALUES ('358', 'Deleted Objective : Enhance ( Key Result ID: 74)', '', 'Delete', '1', '2017-05-22 14:34:14', 'o', '00000000089', '0');
INSERT INTO `activities` VALUES ('359', 'Created a new Objective Decrease expenses', '', 'Create', '1', '2017-05-22 14:35:53', 'o', '00000000090', '0');
INSERT INTO `activities` VALUES ('360', 'Created a new Key Result Decrease major supplier spend by 5%', '', 'Create', '1', '2017-05-22 14:37:19', 'k', '00000000075', '0');
INSERT INTO `activities` VALUES ('361', 'Update Key Result Progress From 0% to 25%', '', 'Update', '1', '2017-05-22 14:57:18', 'k', '00000000073', '0');
INSERT INTO `activities` VALUES ('362', 'Update Key Result Progress From 25% to 0%', '', 'Update', '1', '2017-05-22 14:57:26', 'k', '00000000073', '0');
INSERT INTO `activities` VALUES ('363', 'Update Key Result Progress From 0% to 26%', '', 'Update', '1', '2017-05-22 14:57:55', 'k', '00000000073', '0');
INSERT INTO `activities` VALUES ('364', 'Update Key Result Progress From 26% to 0%', '', 'Update', '1', '2017-05-22 14:57:59', 'k', '00000000073', '0');
INSERT INTO `activities` VALUES ('365', 'Update Key Result Progress From 0% to 24%', '', 'Update', '1', '2017-05-22 15:20:04', 'k', '00000000075', '0');
INSERT INTO `activities` VALUES ('366', 'Update Key Result Progress From 24% to 0%', '', 'Update', '1', '2017-05-22 15:20:12', 'k', '00000000075', '0');

-- ----------------------------
-- Table structure for company_infos
-- ----------------------------
DROP TABLE IF EXISTS `company_infos`;
CREATE TABLE `company_infos` (
  `company_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` text COLLATE utf8mb4_bin,
  `company_mission` text COLLATE utf8mb4_bin,
  `company_vision` text COLLATE utf8mb4_bin,
  `company_address` text COLLATE utf8mb4_bin,
  `company_phone` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `company_email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`company_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of company_infos
-- ----------------------------

-- ----------------------------
-- Table structure for contribution
-- ----------------------------
DROP TABLE IF EXISTS `contribution`;
CREATE TABLE `contribution` (
  `contribution_id` int(11) NOT NULL AUTO_INCREMENT,
  `contribution_detail` varchar(255) DEFAULT NULL,
  `contribution_progress` int(11) DEFAULT NULL,
  `key_result_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`contribution_id`),
  KEY `fk_key_result_contribution_key_result_id` (`key_result_id`),
  KEY `fk_user_comtribution_user_id` (`user_id`),
  CONSTRAINT `fk_key_result_contribution_key_result_id` FOREIGN KEY (`key_result_id`) REFERENCES `key_results` (`result_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_comtribution_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of contribution
-- ----------------------------

-- ----------------------------
-- Table structure for goals
-- ----------------------------
DROP TABLE IF EXISTS `goals`;
CREATE TABLE `goals` (
  `goal_id` int(11) NOT NULL AUTO_INCREMENT,
  `goal_name` text COLLATE utf8mb4_bin,
  `goal_description` text COLLATE utf8mb4_bin,
  `goal_status` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `time_frame_id` int(11) NOT NULL,
  `goal_unit` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `goal_progress_status` int(255) DEFAULT '0',
  `goal_target` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `reviewed` int(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`goal_id`),
  KEY `goal_id` (`goal_id`),
  KEY `fk_goals_time_frame` (`time_frame_id`),
  CONSTRAINT `fk_goals_time_frame` FOREIGN KEY (`time_frame_id`) REFERENCES `time_frames` (`time_frame_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of goals
-- ----------------------------
INSERT INTO `goals` VALUES ('18', 0x50726F7465637420616E642067726F77206F757220636F726520627573696E657373, 0x50726F7465637420616E642067726F772074686520636F7265206F66206F757220627573696E6573732C20696E636C7564696E6720696E6372656173696E67206F7572206D617267696E73206F6E20726563757272696E6720726576656E756520616E642064656372656173696E67206F757220646570656E64656E6379206F6E2068617264776172652073616C6573, 'None', '65', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for goals_objectives
-- ----------------------------
DROP TABLE IF EXISTS `goals_objectives`;
CREATE TABLE `goals_objectives` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `goal_id` int(11) NOT NULL,
  `objective_id` int(11) NOT NULL,
  PRIMARY KEY (`record_id`),
  KEY `fk_goals_objectives_goals` (`goal_id`),
  KEY `fk_goals_objectives_objectivs` (`objective_id`),
  CONSTRAINT `fk_goals_objectives_goals` FOREIGN KEY (`goal_id`) REFERENCES `goals` (`goal_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_goals_objectives_objectivs` FOREIGN KEY (`objective_id`) REFERENCES `objectives` (`objective_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of goals_objectives
-- ----------------------------
INSERT INTO `goals_objectives` VALUES ('104', '18', '89');
INSERT INTO `goals_objectives` VALUES ('105', '18', '90');

-- ----------------------------
-- Table structure for key_results
-- ----------------------------
DROP TABLE IF EXISTS `key_results`;
CREATE TABLE `key_results` (
  `result_id` int(11) NOT NULL AUTO_INCREMENT,
  `result_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `result_description` text COLLATE utf8mb4_bin,
  `result_unit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `result_status` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `result_progress_status` int(255) DEFAULT '0',
  `result_target` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `objective_id` int(11) NOT NULL,
  PRIMARY KEY (`result_id`),
  KEY `fk_key_results_objectives` (`objective_id`),
  CONSTRAINT `fk_key_results_objectives` FOREIGN KEY (`objective_id`) REFERENCES `objectives` (`objective_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of key_results
-- ----------------------------
INSERT INTO `key_results` VALUES ('12', '9999', 0x39393939, '%', 'None', '30', null, '20');
INSERT INTO `key_results` VALUES ('15', 'KR1', 0x4B5231, '%', 'None', '24', '', '27');
INSERT INTO `key_results` VALUES ('17', 'kr3', 0x6B7233, '%', 'None', '88', '', '27');
INSERT INTO `key_results` VALUES ('18', 'K4', 0x4B34, '%', 'None', '30', '', '27');
INSERT INTO `key_results` VALUES ('22', 'New Key Result 1', 0x4E6577204B657920526573756C742031, '%', 'None', '38', '', '34');
INSERT INTO `key_results` VALUES ('23', 'h1', 0x6831, '%', 'None', '36', '', '34');
INSERT INTO `key_results` VALUES ('24', 'ffsdfsdfsdfsfawfefsdfsf4fase', 0x66736466736461667364666177666473, '%', 'None', '50', '', '34');
INSERT INTO `key_results` VALUES ('25', 'key result  1', 0x6B657920726573756C74202031, '%', 'None', '50', '', '35');
INSERT INTO `key_results` VALUES ('26', 'ddd', 0x646464, '%', 'None', '54', '', '36');
INSERT INTO `key_results` VALUES ('27', '22222', 0x3232323232, '%', 'None', '0', '', '37');
INSERT INTO `key_results` VALUES ('28', '776', 0x3636, '%', 'None', '90', '', '40');
INSERT INTO `key_results` VALUES ('41', 'reviewd', 0x72657669657764, '%', 'None', '58', '', '68');
INSERT INTO `key_results` VALUES ('42', 'reviewed2', 0x726576696577656432, '%', 'None', '52', '', '68');
INSERT INTO `key_results` VALUES ('43', 'reviewed 3', 0x72657669657765642033, '%', 'None', '79', '', '68');
INSERT INTO `key_results` VALUES ('45', 'rrr', 0x776572, '%', 'None', '50', '', '70');
INSERT INTO `key_results` VALUES ('46', '33', 0x3333, '%', 'None', '0', '', '70');
INSERT INTO `key_results` VALUES ('47', '321312', 0x313233313233313233, '%', 'None', '88', '', '72');
INSERT INTO `key_results` VALUES ('48', '123', 0x313233, '%', 'None', '88', '', '72');
INSERT INTO `key_results` VALUES ('49', '12312312', 0x313233313233, '%', 'None', '88', '', '72');
INSERT INTO `key_results` VALUES ('50', '3334', 0x343332343334, '%', 'None', '73', '', '71');
INSERT INTO `key_results` VALUES ('51', '45555', 0x3434353435, '%', 'None', '88', '', '72');
INSERT INTO `key_results` VALUES ('52', '444', 0x34343434, '%', 'None', '88', '', '72');
INSERT INTO `key_results` VALUES ('53', 'dasdasd', 0x6173646173646173, '%', 'None', '88', '', '72');
INSERT INTO `key_results` VALUES ('55', 'dsafsda', 0x61736466617364, '%', 'None', '72', '', '81');
INSERT INTO `key_results` VALUES ('56', '456465', 0x3435363435, '%', 'None', '0', '', '81');
INSERT INTO `key_results` VALUES ('57', '456465', 0x343536343536343635343536, '%', 'None', '74', '', '83');
INSERT INTO `key_results` VALUES ('73', 'Recurring contract net profit to $50,000 per month', 0x5765206E65656420746F20696E637265617365206F757220726563757272696E67206E65742070726F66697420746F202435302C30303020706572206D6F6E74682E, '%', 'None', '0', '', '89');
INSERT INTO `key_results` VALUES ('75', 'Decrease major supplier spend by 5%', 0x52657669657720616C6C20737570706C6965722061677265656D656E747320616E64206761696E206265747465722062757920707269636573, '%', 'None', '0', '', '90');

-- ----------------------------
-- Table structure for objectives
-- ----------------------------
DROP TABLE IF EXISTS `objectives`;
CREATE TABLE `objectives` (
  `objective_id` int(11) NOT NULL AUTO_INCREMENT,
  `objective_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `objective_description` text COLLATE utf8mb4_bin,
  `objective_unit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `objective_status` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `objective_progress_status` int(3) unsigned zerofill DEFAULT '000',
  `objective_target` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `reviewed` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  PRIMARY KEY (`objective_id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of objectives
-- ----------------------------
INSERT INTO `objectives` VALUES ('8', 'objectives 1', 0x7465616D20313233206F626A65637469766531, '%', 'None', '000', null, '0');
INSERT INTO `objectives` VALUES ('19', ' f', 0x66, '%', 'None', '000', null, '0');
INSERT INTO `objectives` VALUES ('20', 'fff', 0x666666, '%', 'None', '000', null, '0');
INSERT INTO `objectives` VALUES ('27', 'find more customer111', 0x492066696E64206F6E6520676F6F64206578616D706C6531, '%', 'Warning', '000', '', '0');
INSERT INTO `objectives` VALUES ('29', 'Personal Objective 1', 0x506572736F6E616C204F626A6563746976652031, '%', 'None', '000', null, '0');
INSERT INTO `objectives` VALUES ('30', 'Personal Objective 1', 0x506572736F6E616C204F626A6563746976652031, null, 'None', '000', null, '0');
INSERT INTO `objectives` VALUES ('34', 'tt1', 0x74747431, '%', 'None', '000', '', '0');
INSERT INTO `objectives` VALUES ('35', 'Objective test1', 0x4F626A656374697665207465737431, '%', 'None', '000', '', '0');
INSERT INTO `objectives` VALUES ('36', 'add tem objective', 0x6164642074656D206F626A656374697665, '%', 'Risk', '000', '', '0');
INSERT INTO `objectives` VALUES ('37', '111', 0x313131, '%', 'None', '000', '', '0');
INSERT INTO `objectives` VALUES ('39', '222', 0x33333332, '%', 'None', '000', '', '0');
INSERT INTO `objectives` VALUES ('40', '33', 0x6664, '%', 'Warning', '090', '', '0');
INSERT INTO `objectives` VALUES ('42', 'objectives 2', 0x7465616D203239, '%', 'None', '000', null, '0');
INSERT INTO `objectives` VALUES ('68', '123', 0x313233, '%', 'Complete', '000', '', '0');
INSERT INTO `objectives` VALUES ('70', '432', 0x333234323334, '%', 'Complete', '025', '', '1');
INSERT INTO `objectives` VALUES ('71', '234', 0x323334, '%', 'Complete', '073', '', '1');
INSERT INTO `objectives` VALUES ('72', '32123', 0x31323331323331, '%', 'Complete', '088', '', '1');
INSERT INTO `objectives` VALUES ('81', 'team blue', 0x7465616D20626C7565, '%', 'Warning', '000', '', '0');
INSERT INTO `objectives` VALUES ('82', '4564564', 0x31353634363534353634363534, '%', 'None', '000', '', '0');
INSERT INTO `objectives` VALUES ('83', '123132123132', 0x35313537363534353334363534, '%', 'None', '000', '', '0');
INSERT INTO `objectives` VALUES ('86', '432423', 0x3233343233343233343233343233343233343332, '%', 'None', '000', '', '0');
INSERT INTO `objectives` VALUES ('87', 'ffffds', 0x7364667364667364667364667364, '%', 'None', '000', '', '0');
INSERT INTO `objectives` VALUES ('89', 'Accelerate recurring net profit growth', 0x416363656C657261746520726563757272696E67206E65742070726F6669742067726F777468, '%', 'None', '000', '', '0');
INSERT INTO `objectives` VALUES ('90', 'Decrease expenses', 0x4465637265617365206F7572206D6F6E74686C7920657870656E736573, '%', 'None', '000', '', '0');

-- ----------------------------
-- Table structure for reviews
-- ----------------------------
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `review_rating` int(11) DEFAULT NULL,
  `review_details` varchar(255) DEFAULT NULL,
  `review_user_id` int(11) NOT NULL,
  `review_object_type` varchar(255) DEFAULT NULL,
  `review_object_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`review_id`),
  KEY `fk_user_reviews_user_id` (`review_user_id`),
  CONSTRAINT `fk_user_reviews_user_id` FOREIGN KEY (`review_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of reviews
-- ----------------------------
INSERT INTO `reviews` VALUES ('1', '7', 'do something good\nnonono', '105', 'o', '72');
INSERT INTO `reviews` VALUES ('2', '8', 'yes!!!', '105', 'o', '71');
INSERT INTO `reviews` VALUES ('3', '7', 'dasdasdas', '105', 'o', '70');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin');
INSERT INTO `roles` VALUES ('2', 'manager');
INSERT INTO `roles` VALUES ('3', 'staff');

-- ----------------------------
-- Table structure for system_setting
-- ----------------------------
DROP TABLE IF EXISTS `system_setting`;
CREATE TABLE `system_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(255) DEFAULT NULL,
  `setting_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of system_setting
-- ----------------------------
INSERT INTO `system_setting` VALUES ('1', 'company_name', 'Rock IT Cloud');
INSERT INTO `system_setting` VALUES ('2', 'company_mission', 'Effortless technology that is Solid, Simple & Secure');
INSERT INTO `system_setting` VALUES ('3', 'company_vision', 'Effortless technology that is Solid, Simp');
INSERT INTO `system_setting` VALUES ('4', 'company_address', ' 66-68 Sackville Street Collingwood VIC 3066 Australia');
INSERT INTO `system_setting` VALUES ('5', 'company_phone', '+613 9415 6320');
INSERT INTO `system_setting` VALUES ('6', 'company_email', 'info@rockitcloud.com.au');
INSERT INTO `system_setting` VALUES ('10', 'fdsa', 'fdfdfdf');

-- ----------------------------
-- Table structure for teams
-- ----------------------------
DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `team_description` text COLLATE utf8mb4_bin,
  `parent_team_id` int(11) unsigned DEFAULT NULL,
  `team_leader_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`team_id`),
  KEY `teams_users` (`team_leader_user_id`),
  CONSTRAINT `teams_users` FOREIGN KEY (`team_leader_user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of teams
-- ----------------------------
INSERT INTO `teams` VALUES ('19', 'All Employees', 0x416C6C20456D706C6F796565732031, '0', '105');
INSERT INTO `teams` VALUES ('22', 'Team blue', 0x205465616D20426C7565, '0', '2');
INSERT INTO `teams` VALUES ('23', 'Team Red', 0x205465616D20546564, '0', '5');
INSERT INTO `teams` VALUES ('24', 'Team Yellow', 0x205465616D2059656C6C6F77, '0', '6');
INSERT INTO `teams` VALUES ('25', 'Sub-team of Yellow', 0x5375622D7465616D206F662059656C6C6F77, '24', '27');
INSERT INTO `teams` VALUES ('28', 'team 123', 0x7465616D20313233, '22', '2');
INSERT INTO `teams` VALUES ('29', 'team 1', 0x7465616D2031, '0', '2');

-- ----------------------------
-- Table structure for teams_objectives
-- ----------------------------
DROP TABLE IF EXISTS `teams_objectives`;
CREATE TABLE `teams_objectives` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `objective_id` int(11) NOT NULL,
  PRIMARY KEY (`record_id`),
  KEY `fk_teams_objectives_teams` (`team_id`),
  KEY `fk_teams_objectives_objectives` (`objective_id`),
  CONSTRAINT `fk_teams_objectives_objectives` FOREIGN KEY (`objective_id`) REFERENCES `objectives` (`objective_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_teams_objectives_teams` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of teams_objectives
-- ----------------------------
INSERT INTO `teams_objectives` VALUES ('15', '28', '27');
INSERT INTO `teams_objectives` VALUES ('20', '28', '34');
INSERT INTO `teams_objectives` VALUES ('21', '28', '35');
INSERT INTO `teams_objectives` VALUES ('22', '28', '36');
INSERT INTO `teams_objectives` VALUES ('23', '19', '37');
INSERT INTO `teams_objectives` VALUES ('25', '19', '39');
INSERT INTO `teams_objectives` VALUES ('26', '25', '40');
INSERT INTO `teams_objectives` VALUES ('48', '25', '68');
INSERT INTO `teams_objectives` VALUES ('50', '25', '70');
INSERT INTO `teams_objectives` VALUES ('51', '25', '71');
INSERT INTO `teams_objectives` VALUES ('52', '23', '72');
INSERT INTO `teams_objectives` VALUES ('61', '22', '81');
INSERT INTO `teams_objectives` VALUES ('62', '23', '82');
INSERT INTO `teams_objectives` VALUES ('63', '19', '83');
INSERT INTO `teams_objectives` VALUES ('74', '28', '86');
INSERT INTO `teams_objectives` VALUES ('75', '23', '86');
INSERT INTO `teams_objectives` VALUES ('76', '23', '86');
INSERT INTO `teams_objectives` VALUES ('77', '25', '86');
INSERT INTO `teams_objectives` VALUES ('78', '25', '86');
INSERT INTO `teams_objectives` VALUES ('79', '22', '87');
INSERT INTO `teams_objectives` VALUES ('81', '22', '89');
INSERT INTO `teams_objectives` VALUES ('82', '19', '90');

-- ----------------------------
-- Table structure for teams_users
-- ----------------------------
DROP TABLE IF EXISTS `teams_users`;
CREATE TABLE `teams_users` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`record_id`),
  KEY `fk_teams_users_teams` (`team_id`),
  KEY `fk_teams_users_user` (`user_id`),
  CONSTRAINT `fk_teams_users_teams` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_teams_users_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=378 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of teams_users
-- ----------------------------
INSERT INTO `teams_users` VALUES ('149', '19', '100');
INSERT INTO `teams_users` VALUES ('150', '19', '99');
INSERT INTO `teams_users` VALUES ('151', '19', '98');
INSERT INTO `teams_users` VALUES ('152', '19', '97');
INSERT INTO `teams_users` VALUES ('153', '19', '96');
INSERT INTO `teams_users` VALUES ('154', '19', '95');
INSERT INTO `teams_users` VALUES ('155', '19', '94');
INSERT INTO `teams_users` VALUES ('156', '19', '93');
INSERT INTO `teams_users` VALUES ('157', '19', '92');
INSERT INTO `teams_users` VALUES ('158', '19', '91');
INSERT INTO `teams_users` VALUES ('159', '19', '90');
INSERT INTO `teams_users` VALUES ('160', '19', '89');
INSERT INTO `teams_users` VALUES ('161', '19', '88');
INSERT INTO `teams_users` VALUES ('162', '19', '87');
INSERT INTO `teams_users` VALUES ('163', '19', '86');
INSERT INTO `teams_users` VALUES ('164', '19', '85');
INSERT INTO `teams_users` VALUES ('165', '19', '84');
INSERT INTO `teams_users` VALUES ('166', '19', '83');
INSERT INTO `teams_users` VALUES ('167', '19', '82');
INSERT INTO `teams_users` VALUES ('168', '19', '81');
INSERT INTO `teams_users` VALUES ('169', '19', '80');
INSERT INTO `teams_users` VALUES ('170', '19', '79');
INSERT INTO `teams_users` VALUES ('171', '19', '78');
INSERT INTO `teams_users` VALUES ('172', '19', '77');
INSERT INTO `teams_users` VALUES ('173', '19', '76');
INSERT INTO `teams_users` VALUES ('174', '19', '75');
INSERT INTO `teams_users` VALUES ('175', '19', '74');
INSERT INTO `teams_users` VALUES ('176', '19', '73');
INSERT INTO `teams_users` VALUES ('177', '19', '72');
INSERT INTO `teams_users` VALUES ('178', '19', '71');
INSERT INTO `teams_users` VALUES ('179', '19', '70');
INSERT INTO `teams_users` VALUES ('180', '19', '69');
INSERT INTO `teams_users` VALUES ('181', '19', '68');
INSERT INTO `teams_users` VALUES ('182', '19', '67');
INSERT INTO `teams_users` VALUES ('183', '19', '66');
INSERT INTO `teams_users` VALUES ('184', '19', '65');
INSERT INTO `teams_users` VALUES ('185', '19', '64');
INSERT INTO `teams_users` VALUES ('186', '19', '63');
INSERT INTO `teams_users` VALUES ('187', '19', '62');
INSERT INTO `teams_users` VALUES ('188', '19', '61');
INSERT INTO `teams_users` VALUES ('189', '19', '60');
INSERT INTO `teams_users` VALUES ('190', '19', '59');
INSERT INTO `teams_users` VALUES ('191', '19', '58');
INSERT INTO `teams_users` VALUES ('192', '19', '57');
INSERT INTO `teams_users` VALUES ('193', '19', '56');
INSERT INTO `teams_users` VALUES ('194', '19', '55');
INSERT INTO `teams_users` VALUES ('195', '19', '54');
INSERT INTO `teams_users` VALUES ('196', '19', '53');
INSERT INTO `teams_users` VALUES ('197', '19', '52');
INSERT INTO `teams_users` VALUES ('198', '19', '51');
INSERT INTO `teams_users` VALUES ('199', '19', '50');
INSERT INTO `teams_users` VALUES ('200', '19', '49');
INSERT INTO `teams_users` VALUES ('201', '19', '48');
INSERT INTO `teams_users` VALUES ('202', '19', '47');
INSERT INTO `teams_users` VALUES ('203', '19', '46');
INSERT INTO `teams_users` VALUES ('204', '19', '45');
INSERT INTO `teams_users` VALUES ('205', '19', '44');
INSERT INTO `teams_users` VALUES ('206', '19', '43');
INSERT INTO `teams_users` VALUES ('207', '19', '42');
INSERT INTO `teams_users` VALUES ('208', '19', '41');
INSERT INTO `teams_users` VALUES ('209', '19', '40');
INSERT INTO `teams_users` VALUES ('210', '19', '39');
INSERT INTO `teams_users` VALUES ('211', '19', '38');
INSERT INTO `teams_users` VALUES ('212', '19', '37');
INSERT INTO `teams_users` VALUES ('213', '19', '36');
INSERT INTO `teams_users` VALUES ('214', '19', '35');
INSERT INTO `teams_users` VALUES ('215', '19', '34');
INSERT INTO `teams_users` VALUES ('216', '19', '33');
INSERT INTO `teams_users` VALUES ('217', '19', '32');
INSERT INTO `teams_users` VALUES ('218', '19', '31');
INSERT INTO `teams_users` VALUES ('219', '19', '30');
INSERT INTO `teams_users` VALUES ('220', '19', '29');
INSERT INTO `teams_users` VALUES ('221', '19', '28');
INSERT INTO `teams_users` VALUES ('222', '19', '27');
INSERT INTO `teams_users` VALUES ('223', '19', '26');
INSERT INTO `teams_users` VALUES ('224', '19', '25');
INSERT INTO `teams_users` VALUES ('225', '19', '24');
INSERT INTO `teams_users` VALUES ('226', '19', '23');
INSERT INTO `teams_users` VALUES ('227', '19', '22');
INSERT INTO `teams_users` VALUES ('228', '19', '21');
INSERT INTO `teams_users` VALUES ('229', '19', '20');
INSERT INTO `teams_users` VALUES ('230', '19', '19');
INSERT INTO `teams_users` VALUES ('231', '19', '18');
INSERT INTO `teams_users` VALUES ('232', '19', '17');
INSERT INTO `teams_users` VALUES ('233', '19', '16');
INSERT INTO `teams_users` VALUES ('234', '19', '15');
INSERT INTO `teams_users` VALUES ('235', '19', '14');
INSERT INTO `teams_users` VALUES ('236', '19', '13');
INSERT INTO `teams_users` VALUES ('237', '19', '12');
INSERT INTO `teams_users` VALUES ('238', '19', '11');
INSERT INTO `teams_users` VALUES ('239', '19', '10');
INSERT INTO `teams_users` VALUES ('240', '19', '9');
INSERT INTO `teams_users` VALUES ('241', '19', '8');
INSERT INTO `teams_users` VALUES ('242', '19', '7');
INSERT INTO `teams_users` VALUES ('243', '19', '6');
INSERT INTO `teams_users` VALUES ('244', '19', '5');
INSERT INTO `teams_users` VALUES ('245', '19', '4');
INSERT INTO `teams_users` VALUES ('246', '19', '3');
INSERT INTO `teams_users` VALUES ('247', '19', '2');
INSERT INTO `teams_users` VALUES ('248', '19', '1');
INSERT INTO `teams_users` VALUES ('249', '22', '1');
INSERT INTO `teams_users` VALUES ('250', '22', '2');
INSERT INTO `teams_users` VALUES ('251', '22', '4');
INSERT INTO `teams_users` VALUES ('252', '22', '6');
INSERT INTO `teams_users` VALUES ('253', '23', '3');
INSERT INTO `teams_users` VALUES ('254', '23', '23');
INSERT INTO `teams_users` VALUES ('255', '23', '39');
INSERT INTO `teams_users` VALUES ('256', '23', '54');
INSERT INTO `teams_users` VALUES ('257', '23', '73');
INSERT INTO `teams_users` VALUES ('258', '24', '7');
INSERT INTO `teams_users` VALUES ('259', '24', '23');
INSERT INTO `teams_users` VALUES ('260', '24', '41');
INSERT INTO `teams_users` VALUES ('261', '25', '3');
INSERT INTO `teams_users` VALUES ('262', '25', '21');
INSERT INTO `teams_users` VALUES ('263', '25', '17');
INSERT INTO `teams_users` VALUES ('264', '25', '14');
INSERT INTO `teams_users` VALUES ('265', '25', '72');
INSERT INTO `teams_users` VALUES ('266', '25', '100');
INSERT INTO `teams_users` VALUES ('267', '28', '1');
INSERT INTO `teams_users` VALUES ('268', '28', '5');
INSERT INTO `teams_users` VALUES ('269', '28', '6');
INSERT INTO `teams_users` VALUES ('270', '28', '19');
INSERT INTO `teams_users` VALUES ('271', '19', '105');
INSERT INTO `teams_users` VALUES ('272', '29', '1');
INSERT INTO `teams_users` VALUES ('273', '29', '2');
INSERT INTO `teams_users` VALUES ('274', '29', '3');
INSERT INTO `teams_users` VALUES ('275', '29', '4');
INSERT INTO `teams_users` VALUES ('276', '29', '5');
INSERT INTO `teams_users` VALUES ('277', '29', '6');
INSERT INTO `teams_users` VALUES ('278', '29', '7');
INSERT INTO `teams_users` VALUES ('279', '29', '8');
INSERT INTO `teams_users` VALUES ('280', '29', '9');
INSERT INTO `teams_users` VALUES ('281', '29', '10');
INSERT INTO `teams_users` VALUES ('282', '29', '11');
INSERT INTO `teams_users` VALUES ('283', '29', '12');
INSERT INTO `teams_users` VALUES ('284', '29', '13');
INSERT INTO `teams_users` VALUES ('285', '29', '14');
INSERT INTO `teams_users` VALUES ('286', '29', '15');
INSERT INTO `teams_users` VALUES ('287', '29', '16');
INSERT INTO `teams_users` VALUES ('288', '29', '17');
INSERT INTO `teams_users` VALUES ('289', '29', '18');
INSERT INTO `teams_users` VALUES ('290', '29', '19');
INSERT INTO `teams_users` VALUES ('291', '29', '20');
INSERT INTO `teams_users` VALUES ('292', '29', '21');
INSERT INTO `teams_users` VALUES ('293', '29', '22');
INSERT INTO `teams_users` VALUES ('294', '29', '23');
INSERT INTO `teams_users` VALUES ('295', '29', '24');
INSERT INTO `teams_users` VALUES ('296', '29', '25');
INSERT INTO `teams_users` VALUES ('297', '29', '26');
INSERT INTO `teams_users` VALUES ('298', '29', '27');
INSERT INTO `teams_users` VALUES ('299', '29', '28');
INSERT INTO `teams_users` VALUES ('300', '29', '29');
INSERT INTO `teams_users` VALUES ('301', '29', '30');
INSERT INTO `teams_users` VALUES ('302', '29', '31');
INSERT INTO `teams_users` VALUES ('303', '29', '32');
INSERT INTO `teams_users` VALUES ('304', '29', '33');
INSERT INTO `teams_users` VALUES ('305', '29', '34');
INSERT INTO `teams_users` VALUES ('306', '29', '35');
INSERT INTO `teams_users` VALUES ('307', '29', '36');
INSERT INTO `teams_users` VALUES ('308', '29', '37');
INSERT INTO `teams_users` VALUES ('309', '29', '38');
INSERT INTO `teams_users` VALUES ('310', '29', '39');
INSERT INTO `teams_users` VALUES ('311', '29', '40');
INSERT INTO `teams_users` VALUES ('312', '29', '41');
INSERT INTO `teams_users` VALUES ('313', '29', '42');
INSERT INTO `teams_users` VALUES ('314', '29', '43');
INSERT INTO `teams_users` VALUES ('315', '29', '44');
INSERT INTO `teams_users` VALUES ('316', '29', '45');
INSERT INTO `teams_users` VALUES ('317', '29', '46');
INSERT INTO `teams_users` VALUES ('318', '29', '47');
INSERT INTO `teams_users` VALUES ('319', '29', '48');
INSERT INTO `teams_users` VALUES ('320', '29', '49');
INSERT INTO `teams_users` VALUES ('321', '29', '50');
INSERT INTO `teams_users` VALUES ('322', '29', '51');
INSERT INTO `teams_users` VALUES ('323', '29', '52');
INSERT INTO `teams_users` VALUES ('324', '29', '53');
INSERT INTO `teams_users` VALUES ('325', '29', '54');
INSERT INTO `teams_users` VALUES ('326', '29', '55');
INSERT INTO `teams_users` VALUES ('327', '29', '56');
INSERT INTO `teams_users` VALUES ('328', '29', '57');
INSERT INTO `teams_users` VALUES ('329', '29', '58');
INSERT INTO `teams_users` VALUES ('330', '29', '59');
INSERT INTO `teams_users` VALUES ('331', '29', '60');
INSERT INTO `teams_users` VALUES ('332', '29', '61');
INSERT INTO `teams_users` VALUES ('333', '29', '62');
INSERT INTO `teams_users` VALUES ('334', '29', '63');
INSERT INTO `teams_users` VALUES ('335', '29', '64');
INSERT INTO `teams_users` VALUES ('336', '29', '65');
INSERT INTO `teams_users` VALUES ('337', '29', '66');
INSERT INTO `teams_users` VALUES ('338', '29', '67');
INSERT INTO `teams_users` VALUES ('339', '29', '68');
INSERT INTO `teams_users` VALUES ('340', '29', '69');
INSERT INTO `teams_users` VALUES ('341', '29', '70');
INSERT INTO `teams_users` VALUES ('342', '29', '71');
INSERT INTO `teams_users` VALUES ('343', '29', '72');
INSERT INTO `teams_users` VALUES ('344', '29', '73');
INSERT INTO `teams_users` VALUES ('345', '29', '74');
INSERT INTO `teams_users` VALUES ('346', '29', '75');
INSERT INTO `teams_users` VALUES ('347', '29', '76');
INSERT INTO `teams_users` VALUES ('348', '29', '77');
INSERT INTO `teams_users` VALUES ('349', '29', '78');
INSERT INTO `teams_users` VALUES ('350', '29', '79');
INSERT INTO `teams_users` VALUES ('351', '29', '80');
INSERT INTO `teams_users` VALUES ('352', '29', '81');
INSERT INTO `teams_users` VALUES ('353', '29', '82');
INSERT INTO `teams_users` VALUES ('354', '29', '83');
INSERT INTO `teams_users` VALUES ('355', '29', '84');
INSERT INTO `teams_users` VALUES ('356', '29', '85');
INSERT INTO `teams_users` VALUES ('357', '29', '86');
INSERT INTO `teams_users` VALUES ('358', '29', '87');
INSERT INTO `teams_users` VALUES ('359', '29', '88');
INSERT INTO `teams_users` VALUES ('360', '29', '89');
INSERT INTO `teams_users` VALUES ('361', '29', '90');
INSERT INTO `teams_users` VALUES ('362', '29', '91');
INSERT INTO `teams_users` VALUES ('363', '29', '92');
INSERT INTO `teams_users` VALUES ('364', '29', '93');
INSERT INTO `teams_users` VALUES ('365', '29', '94');
INSERT INTO `teams_users` VALUES ('366', '29', '95');
INSERT INTO `teams_users` VALUES ('367', '29', '96');
INSERT INTO `teams_users` VALUES ('368', '29', '97');
INSERT INTO `teams_users` VALUES ('369', '29', '98');
INSERT INTO `teams_users` VALUES ('370', '29', '99');
INSERT INTO `teams_users` VALUES ('371', '29', '100');
INSERT INTO `teams_users` VALUES ('372', '29', '103');
INSERT INTO `teams_users` VALUES ('373', '29', '104');
INSERT INTO `teams_users` VALUES ('374', '29', '105');
INSERT INTO `teams_users` VALUES ('375', '29', '106');
INSERT INTO `teams_users` VALUES ('376', '29', '107');
INSERT INTO `teams_users` VALUES ('377', '29', '108');

-- ----------------------------
-- Table structure for time_frames
-- ----------------------------
DROP TABLE IF EXISTS `time_frames`;
CREATE TABLE `time_frames` (
  `time_frame_id` int(11) NOT NULL AUTO_INCREMENT,
  `time_frame_description` text COLLATE utf8mb4_bin,
  `time_frame_start` date DEFAULT NULL,
  `time_frame_end` date DEFAULT NULL,
  PRIMARY KEY (`time_frame_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of time_frames
-- ----------------------------
INSERT INTO `time_frames` VALUES ('63', 0x323031372C205131, '2017-01-01', '2017-04-01');
INSERT INTO `time_frames` VALUES ('64', 0x323031372C205132, '2017-04-01', '2017-07-01');
INSERT INTO `time_frames` VALUES ('65', 0x323031372C205133, '2017-07-01', '2017-10-01');
INSERT INTO `time_frames` VALUES ('66', 0x323031372C205134, '2017-10-01', '2018-01-01');

-- ----------------------------
-- Table structure for user_setting
-- ----------------------------
DROP TABLE IF EXISTS `user_setting`;
CREATE TABLE `user_setting` (
  `setting_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `setting_default_time_frame_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_user_setting_users` (`user_id`),
  CONSTRAINT `fk_user_setting_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of user_setting
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `account_status` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin@admin.com', 'admin', '81dc9bdb52d04dc20036dbd8313ed055', '1');
INSERT INTO `users` VALUES ('2', 'manager@admin.com', 'manager', '81dc9bdb52d04dc20036dbd8313ed055', '1');
INSERT INTO `users` VALUES ('3', 'staff@admin.com', 'staff', '81dc9bdb52d04dc20036dbd8313ed055', '1');
INSERT INTO `users` VALUES ('4', 'director@admin.com', 'director', '1234', '1');
INSERT INTO `users` VALUES ('5', 'nisi.Aenean.eget@posuereenim.ca', 'Morse', '1234', '1');
INSERT INTO `users` VALUES ('6', 'massa@Maurisblanditenim.org', 'Roberts', '1234', '1');
INSERT INTO `users` VALUES ('7', 'orci.luctus@velsapien.co.uk', 'Hays', '1234', '0');
INSERT INTO `users` VALUES ('8', 'facilisis.vitae@Maecenas.net', 'Vaughn', '1234', '0');
INSERT INTO `users` VALUES ('9', 'mauris.ipsum@aliquet.net', 'Miranda', '1234', '1');
INSERT INTO `users` VALUES ('10', 'nec@justoeu.com', 'Finch', '1234', '0');
INSERT INTO `users` VALUES ('11', 'orci.in@mi.ca', 'Mercer', '1234', '1');
INSERT INTO `users` VALUES ('12', 'Ut.semper.pretium@ante.net', 'Roy', '1234', '0');
INSERT INTO `users` VALUES ('13', 'Nullam.vitae@augueSed.edu', 'Anthony', '1234', '1');
INSERT INTO `users` VALUES ('14', 'Integer.aliquam@aliquamadipiscinglacus.com', 'Mckee', '1234', '1');
INSERT INTO `users` VALUES ('15', 'fermentum@DonecnibhQuisque.net', 'Cooper', '1234', '1');
INSERT INTO `users` VALUES ('16', 'primis@acliberonec.com', 'Hall', '1234', '1');
INSERT INTO `users` VALUES ('17', 'Donec.tempus.lorem@pretiumaliquetmetus.net', 'Pate', '1234', '1');
INSERT INTO `users` VALUES ('18', 'Aliquam.ultrices@quis.com', 'Cote', '1234', '1');
INSERT INTO `users` VALUES ('19', 'sed@vitaealiquet.co.uk', 'Duran', '1234', '1');
INSERT INTO `users` VALUES ('20', 'aliquet@vitaeeratVivamus.net', 'Porter', '1234', '1');
INSERT INTO `users` VALUES ('21', 'mauris.a.nunc@penatibuset.ca', 'Washington', '1234', '0');
INSERT INTO `users` VALUES ('22', 'elit.Etiam.laoreet@fermentum.edu', 'Castro', '1234', '0');
INSERT INTO `users` VALUES ('23', 'et.libero@Nuncmauris.org', 'Mcintosh', '1234', '0');
INSERT INTO `users` VALUES ('24', 'iaculis.lacus@rutrumjustoPraesent.org', 'Cruz', '1234', '1');
INSERT INTO `users` VALUES ('25', 'Aliquam.rutrum.lorem@sociosquad.co.uk', 'Francis', '1234', '1');
INSERT INTO `users` VALUES ('26', 'augue.Sed.molestie@acrisusMorbi.com', 'Duran', '1234', '0');
INSERT INTO `users` VALUES ('27', 'sapien@consequatauctornunc.edu', 'Monroe', '1234', '0');
INSERT INTO `users` VALUES ('28', 'in@diam.co.uk', 'Hooper', '1234', '1');
INSERT INTO `users` VALUES ('29', 'eget.mollis.lectus@quam.net', 'Moran', '1234', '1');
INSERT INTO `users` VALUES ('30', 'Integer.mollis@inlobortistellus.org', 'Moreno', '1234', '0');
INSERT INTO `users` VALUES ('31', 'ipsum@nequeetnunc.net', 'Lindsay', '1234', '1');
INSERT INTO `users` VALUES ('32', 'convallis@amet.ca', 'Pratt', '1234', '1');
INSERT INTO `users` VALUES ('33', 'tempus.eu@inhendrerit.edu', 'Murphy', '1234', '1');
INSERT INTO `users` VALUES ('34', 'diam.eu.dolor@Cras.org', 'Arnold', '1234', '1');
INSERT INTO `users` VALUES ('35', 'diam@nonummy.net', 'Reeves', '1234', '0');
INSERT INTO `users` VALUES ('36', 'nec.luctus.felis@parturientmontes.org', 'West', '1234', '0');
INSERT INTO `users` VALUES ('37', 'Vivamus.nibh@mauris.com', 'Ruiz', '1234', '0');
INSERT INTO `users` VALUES ('38', 'mus.Donec@etnetus.co.uk', 'Mooney', '1234', '1');
INSERT INTO `users` VALUES ('39', 'nulla.Donec.non@ipsumSuspendisse.com', 'Mcmillan', '1234', '1');
INSERT INTO `users` VALUES ('40', 'eu.accumsan@nibhPhasellusnulla.ca', 'Holmes', '1234', '1');
INSERT INTO `users` VALUES ('41', 'erat.neque.non@etrisus.ca', 'Hester', '1234', '0');
INSERT INTO `users` VALUES ('42', 'libero.mauris@nequeet.com', 'Slater', '1234', '1');
INSERT INTO `users` VALUES ('43', 'amet.metus@malesuada.edu', 'Coffey', '1234', '0');
INSERT INTO `users` VALUES ('44', 'ac.turpis.egestas@sitametrisus.com', 'Hancock', '1234', '0');
INSERT INTO `users` VALUES ('45', 'torquent.per@Integertincidunt.co.uk', 'Hull', '1234', '1');
INSERT INTO `users` VALUES ('46', 'pellentesque@aliquet.org', 'Brady', '1234', '1');
INSERT INTO `users` VALUES ('47', 'ac.mattis.semper@Duis.ca', 'Harmon', '1234', '0');
INSERT INTO `users` VALUES ('48', 'venenatis.vel.faucibus@dapibus.org', 'Mosley', '1234', '0');
INSERT INTO `users` VALUES ('49', 'risus.odio@variuseteuismod.ca', 'Cardenas', '1234', '1');
INSERT INTO `users` VALUES ('50', 'nunc.nulla@sapienmolestieorci.org', 'Hamilton', '1234', '1');
INSERT INTO `users` VALUES ('51', 'ipsum@dapibusrutrumjusto.net', 'Whitley', '1234', '1');
INSERT INTO `users` VALUES ('52', 'justo.Praesent@arcuimperdiet.org', 'Jenkins', '1234', '1');
INSERT INTO `users` VALUES ('53', 'vel.faucibus.id@enimcondimentum.ca', 'Douglas', '1234', '0');
INSERT INTO `users` VALUES ('54', 'eleifend.egestas@conubianostraper.com', 'Hart', '1234', '0');
INSERT INTO `users` VALUES ('55', 'faucibus@Nam.org', 'Valencia', '1234', '0');
INSERT INTO `users` VALUES ('56', 'metus.Vivamus@convallis.edu', 'Nguyen', '1234', '0');
INSERT INTO `users` VALUES ('57', 'ac.orci@semperauctor.co.uk', 'Bowman', '1234', '0');
INSERT INTO `users` VALUES ('58', 'ipsum@posuere.ca', 'Wiggins', '1234', '1');
INSERT INTO `users` VALUES ('59', 'non.sollicitudin@lectusCum.ca', 'Strong', '1234', '0');
INSERT INTO `users` VALUES ('60', 'aliquet.nec@faucibus.com', 'Sosa', '1234', '0');
INSERT INTO `users` VALUES ('61', 'vitae.posuere.at@vestibulumloremsit.net', 'Whitley', '1234', '0');
INSERT INTO `users` VALUES ('62', 'In@egestasblanditNam.co.uk', 'Hicks', '1234', '0');
INSERT INTO `users` VALUES ('63', 'varius.orci@ipsumSuspendisse.edu', 'Gaines', '1234', '0');
INSERT INTO `users` VALUES ('64', 'aliquam@aodio.com', 'Henderson', '1234', '0');
INSERT INTO `users` VALUES ('65', 'neque.Nullam.ut@acturpisegestas.edu', 'Dale', '1234', '0');
INSERT INTO `users` VALUES ('66', 'vitae@turpis.co.uk', 'Nielsen', '1234', '0');
INSERT INTO `users` VALUES ('67', 'neque.sed.dictum@porttitorscelerisqueneque.com', 'Moody', '1234', '0');
INSERT INTO `users` VALUES ('68', 'pede@Phasellus.co.uk', 'Greer', '1234', '0');
INSERT INTO `users` VALUES ('69', 'vehicula.Pellentesque@utaliquam.ca', 'Wilkinson', '1234', '1');
INSERT INTO `users` VALUES ('70', 'a.ultricies.adipiscing@elitpretium.ca', 'Riddle', '1234', '1');
INSERT INTO `users` VALUES ('71', 'purus.accumsan@eueuismodac.co.uk', 'Sweet', '1234', '0');
INSERT INTO `users` VALUES ('72', 'turpis@eutellus.org', 'Obrien', '1234', '1');
INSERT INTO `users` VALUES ('73', 'ornare.In@egestasblandit.co.uk', 'Faulkner', '1234', '1');
INSERT INTO `users` VALUES ('74', 'Aenean@nasceturridiculusmus.org', 'Moran', '1234', '1');
INSERT INTO `users` VALUES ('75', 'et.malesuada@ornaresagittis.com', 'Dennis', '1234', '0');
INSERT INTO `users` VALUES ('76', 'erat.in.consectetuer@Fuscediamnunc.ca', 'Gregory', '1234', '0');
INSERT INTO `users` VALUES ('77', 'Proin.eget@arcueu.org', 'Kim', '1234', '1');
INSERT INTO `users` VALUES ('78', 'congue@acturpis.edu', 'Berry', '1234', '1');
INSERT INTO `users` VALUES ('79', 'odio.Aliquam@vulputatedui.com', 'Fitzpatrick', '1234', '0');
INSERT INTO `users` VALUES ('80', 'lorem@quislectusNullam.edu', 'Montgomery', '1234', '1');
INSERT INTO `users` VALUES ('81', 'pede.et.risus@nonsollicitudina.edu', 'Lee', '1234', '1');
INSERT INTO `users` VALUES ('82', 'Fusce.fermentum.fermentum@Nullaeu.co.uk', 'Gates', '1234', '1');
INSERT INTO `users` VALUES ('83', 'Mauris@odioEtiam.net', 'Mccormick', '1234', '0');
INSERT INTO `users` VALUES ('84', 'mollis.Phasellus.libero@nislelementum.net', 'Bond', '1234', '1');
INSERT INTO `users` VALUES ('85', 'auctor.velit.Aliquam@lobortisquis.com', 'Miranda', '1234', '0');
INSERT INTO `users` VALUES ('86', 'Praesent.interdum@convalliserateget.co.uk', 'Crosby', '1234', '0');
INSERT INTO `users` VALUES ('87', 'et.magnis.dis@libero.ca', 'Christian', '1234', '0');
INSERT INTO `users` VALUES ('88', 'facilisis.non@ultrices.net', 'Rocha', '1234', '0');
INSERT INTO `users` VALUES ('89', 'ipsum.Donec.sollicitudin@vulputate.com', 'Chavez', '1234', '1');
INSERT INTO `users` VALUES ('90', 'pede.Praesent.eu@Donec.com', 'Compton', '1234', '0');
INSERT INTO `users` VALUES ('91', 'euismod.urna@aptent.com', 'Alford', '1234', '1');
INSERT INTO `users` VALUES ('92', 'tincidunt@duiquisaccumsan.net', 'Sherman', '1234', '1');
INSERT INTO `users` VALUES ('93', 'dui@Proinmi.net', 'Randall', '1234', '0');
INSERT INTO `users` VALUES ('94', 'eu.metus.In@mollisvitae.edu', 'Pittman', '1234', '0');
INSERT INTO `users` VALUES ('95', 'hendrerit.consectetuer@sem.co.uk', 'Bray', '1234', '1');
INSERT INTO `users` VALUES ('96', 'lectus.sit.amet@sedsem.com', 'Miller', '1234', '0');
INSERT INTO `users` VALUES ('97', 'turpis.nec@odioNaminterdum.net', 'Mcleod', '1234', '0');
INSERT INTO `users` VALUES ('98', 'Praesent.eu.dui@arcu.ca', 'Bullock', '1234', '0');
INSERT INTO `users` VALUES ('99', 'natoque@egetmassa.com', 'Forbes', '1234', '0');
INSERT INTO `users` VALUES ('100', 'vel@Namligula.ca', 'Garrett', '1234', '1');
INSERT INTO `users` VALUES ('103', 'vel@Namligula.ca', null, '1234', '1');
INSERT INTO `users` VALUES ('104', 'admin@admin.com', null, '81dc9bdb52d04dc20036dbd8313ed055', '1');
INSERT INTO `users` VALUES ('105', 'dreamli1121@gmail.com', null, 'e19d5cd5af0378da05f63f891c7467af', '1');
INSERT INTO `users` VALUES ('106', 'lllkkk@gmail.com', null, '7baf81aa0577408c44eb14e8bd8973de', '1');
INSERT INTO `users` VALUES ('107', 'aaa@bbb.com', null, '02f28385d123144ff3413879bcfe1695', '1');
INSERT INTO `users` VALUES ('108', 'dreamli1121@gmail.com', null, 'e19d5cd5af0378da05f63f891c7467af', '1');

-- ----------------------------
-- Table structure for users_details
-- ----------------------------
DROP TABLE IF EXISTS `users_details`;
CREATE TABLE `users_details` (
  `user_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `mobile_number` varchar(11) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_details_id`),
  KEY `fk_user_details_user` (`user_id`),
  KEY `fk_from_user_roles` (`role_id`),
  CONSTRAINT `fk_from_user_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `fk_user_details_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of users_details
-- ----------------------------
INSERT INTO `users_details` VALUES ('1', 'Kiona', 'Jimenez', '1955-10-23', null, '1', '1', '1');
INSERT INTO `users_details` VALUES ('2', 'Elizabeth', 'Avery', '1953-12-13', null, '2', '1', '2');
INSERT INTO `users_details` VALUES ('3', 'staff', 'staff', '1969-04-03', null, '3', '1', '3');
INSERT INTO `users_details` VALUES ('4', 'Anika', 'Houston', '1971-03-02', null, '4', '2', '1');
INSERT INTO `users_details` VALUES ('5', 'Tyler', 'Hess', '1959-06-14', null, '5', '2', '2');
INSERT INTO `users_details` VALUES ('6', 'Dean', 'Marks', '1984-05-17', null, '6', '3', '2');
INSERT INTO `users_details` VALUES ('7', 'Christian', 'Mcfadden', '1996-02-04', null, '7', '3', '3');
INSERT INTO `users_details` VALUES ('8', 'Chadwick', 'Maxwell', '1968-03-05', null, '8', '3', '2');
INSERT INTO `users_details` VALUES ('9', 'Merritt', 'Morse', '1964-09-18', null, '9', '3', '2');
INSERT INTO `users_details` VALUES ('10', 'Marvin', 'Day', '1953-10-26', null, '10', '3', '2');
INSERT INTO `users_details` VALUES ('11', 'Lionel', 'Levine', '1935-04-17', null, '11', '3', '2');
INSERT INTO `users_details` VALUES ('12', 'Skyler', 'Garrett', '1953-10-26', null, '12', '3', '2');
INSERT INTO `users_details` VALUES ('13', 'Zena', 'Clayton', '1935-05-14', null, '13', '3', '3');
INSERT INTO `users_details` VALUES ('14', 'Yuri', 'Bowers', '1977-05-01', null, '14', '3', '3');
INSERT INTO `users_details` VALUES ('15', 'Daryl', 'Moon', '1981-01-31', null, '15', '3', '3');
INSERT INTO `users_details` VALUES ('16', 'Lane', 'Ruiz', '1976-02-16', null, '16', '3', '3');
INSERT INTO `users_details` VALUES ('17', 'Unity', 'Whitney', '1936-07-28', null, '17', '3', '3');
INSERT INTO `users_details` VALUES ('18', 'Wade', 'Russo', '1997-05-30', null, '18', '3', '3');
INSERT INTO `users_details` VALUES ('19', 'Breanna', 'Huber', '1944-04-18', null, '19', '3', '3');
INSERT INTO `users_details` VALUES ('20', 'Thomas', 'Reyes', '1996-07-08', null, '20', '3', '3');
INSERT INTO `users_details` VALUES ('21', 'Grace', 'Brady', '1967-12-23', null, '21', '3', '3');
INSERT INTO `users_details` VALUES ('22', 'Chandler', 'Caldwell', '1938-10-30', null, '22', '3', '3');
INSERT INTO `users_details` VALUES ('23', 'Kenneth', 'Clayton', '1950-05-28', null, '23', '3', '3');
INSERT INTO `users_details` VALUES ('24', 'Chantale', 'Myers', '1993-01-02', null, '24', '3', '3');
INSERT INTO `users_details` VALUES ('25', 'Giselle', 'Boyer', '1982-07-07', null, '25', '3', '3');
INSERT INTO `users_details` VALUES ('26', 'Catherine', 'Sexton', '1950-08-10', null, '26', '3', '3');
INSERT INTO `users_details` VALUES ('27', 'Tashya', 'Young', '1976-04-20', null, '27', '3', '3');
INSERT INTO `users_details` VALUES ('28', 'Claire', 'Jordan', '1992-04-06', null, '28', '3', '3');
INSERT INTO `users_details` VALUES ('29', 'Urielle', 'Huffman', '1945-08-31', null, '29', '3', '3');
INSERT INTO `users_details` VALUES ('30', 'Oprah', 'Floyd', '1945-10-22', null, '30', '3', '3');
INSERT INTO `users_details` VALUES ('31', 'Hyacinth', 'Sargent', '1995-09-17', null, '31', '3', '3');
INSERT INTO `users_details` VALUES ('32', 'Christopher', 'Whitney', '1962-03-21', null, '32', '3', '3');
INSERT INTO `users_details` VALUES ('33', 'Danielle', 'Lowery', '1985-12-15', null, '33', '3', '3');
INSERT INTO `users_details` VALUES ('34', 'Dominique', 'Thornton', '1941-05-31', null, '34', '3', null);
INSERT INTO `users_details` VALUES ('35', 'Donna', 'Chandler', '1967-03-27', null, '35', '3', '3');
INSERT INTO `users_details` VALUES ('36', 'Gretchen', 'Duffy', '1948-10-26', null, '36', '3', '3');
INSERT INTO `users_details` VALUES ('37', 'Basil', 'Richard', '1948-04-20', null, '37', '3', '3');
INSERT INTO `users_details` VALUES ('38', 'Travis', 'Villarreal', '1984-05-30', null, '38', '3', '3');
INSERT INTO `users_details` VALUES ('39', 'Winter', 'Brooks', '1937-05-23', null, '39', '3', '3');
INSERT INTO `users_details` VALUES ('40', 'Geraldine', 'Burton', '1942-12-10', null, '40', '3', '3');
INSERT INTO `users_details` VALUES ('41', 'Paula', 'Richards', '1988-07-01', null, '41', '3', '3');
INSERT INTO `users_details` VALUES ('42', 'Darrel', 'Hinton', '1980-10-23', null, '42', '3', '3');
INSERT INTO `users_details` VALUES ('43', 'Quentin', 'Espinoza', '1960-12-29', null, '43', '3', '3');
INSERT INTO `users_details` VALUES ('44', 'Tate', 'Mckinney', '1975-10-15', null, '44', '3', '3');
INSERT INTO `users_details` VALUES ('45', 'Quamar', 'Booker', '1961-03-20', null, '45', '3', '3');
INSERT INTO `users_details` VALUES ('46', 'Nigel', 'Jefferson', '1958-01-22', null, '46', '3', '3');
INSERT INTO `users_details` VALUES ('47', 'Chaney', 'Leonard', '1964-02-22', null, '47', '3', '3');
INSERT INTO `users_details` VALUES ('48', 'Rachel', 'Wiley', '1968-02-05', null, '48', '3', '3');
INSERT INTO `users_details` VALUES ('49', 'Wing', 'Collins', '1945-11-02', null, '49', '3', '3');
INSERT INTO `users_details` VALUES ('50', 'Dawn', 'Wallace', '1947-11-11', null, '50', '3', '3');
INSERT INTO `users_details` VALUES ('51', 'Zephania', 'Hunt', '1981-12-16', null, '51', '3', '3');
INSERT INTO `users_details` VALUES ('52', 'Nigel', 'Mckinney', '1957-12-30', null, '52', '3', '3');
INSERT INTO `users_details` VALUES ('53', 'Iola', 'Roberts', '1938-03-24', null, '53', '3', '3');
INSERT INTO `users_details` VALUES ('54', 'Yetta', 'Albert', '1995-12-27', null, '54', '3', '3');
INSERT INTO `users_details` VALUES ('55', 'Amelia', 'Vasquez', '1998-04-15', null, '55', '3', '3');
INSERT INTO `users_details` VALUES ('56', 'Brock', 'Crawford', '1945-06-19', null, '56', '3', '3');
INSERT INTO `users_details` VALUES ('57', 'Jorden', 'Stephens', '1936-08-07', null, '57', '3', '1');
INSERT INTO `users_details` VALUES ('58', 'Lucius', 'Travis', '1984-10-31', null, '58', '3', '2');
INSERT INTO `users_details` VALUES ('59', 'Armand', 'Barron', '1960-06-07', null, '59', '3', '2');
INSERT INTO `users_details` VALUES ('60', 'Rahim', 'Estes', '1968-09-26', null, '60', '3', '3');
INSERT INTO `users_details` VALUES ('61', 'Harper', 'Lynn', '1947-08-28', null, '61', '3', '2');
INSERT INTO `users_details` VALUES ('62', 'Jillian', 'Mullen', '1988-07-15', null, '62', '3', '1');
INSERT INTO `users_details` VALUES ('63', 'Adam', 'Ramirez', '1990-04-11', null, '63', '3', '1');
INSERT INTO `users_details` VALUES ('64', 'Kylan', 'Reynolds', '1976-02-17', null, '64', '3', '3');
INSERT INTO `users_details` VALUES ('65', 'Gloria', 'Baxter', '1978-12-25', null, '65', '3', '3');
INSERT INTO `users_details` VALUES ('66', 'Madeline', 'Murphy', '1987-04-18', null, '66', '3', '3');
INSERT INTO `users_details` VALUES ('67', 'Yuli', 'Todd', '1997-09-22', null, '67', '3', '3');
INSERT INTO `users_details` VALUES ('68', 'Raymond', 'Lawson', '1993-07-03', null, '68', '3', '3');
INSERT INTO `users_details` VALUES ('69', 'Nayda', 'Mcpherson', '1938-12-07', null, '69', '3', '3');
INSERT INTO `users_details` VALUES ('70', 'Cheryl', 'Trujillo', '1988-04-30', null, '70', '3', '3');
INSERT INTO `users_details` VALUES ('71', 'Jakeem', 'Hines', '1987-06-03', null, '71', '3', '3');
INSERT INTO `users_details` VALUES ('72', 'Salvador', 'Coffey', '1976-01-02', null, '72', '3', '3');
INSERT INTO `users_details` VALUES ('73', 'Tad', 'Melendez', '1972-03-15', null, '73', '3', '3');
INSERT INTO `users_details` VALUES ('74', 'Sade', 'Hebert', '1979-07-04', null, '74', '3', '3');
INSERT INTO `users_details` VALUES ('75', 'Kieran', 'Bass', '1975-11-07', null, '75', '3', '3');
INSERT INTO `users_details` VALUES ('76', 'Lars', 'Castillo', '1987-02-08', null, '76', '3', '3');
INSERT INTO `users_details` VALUES ('77', 'August', 'Weiss', '1982-11-12', null, '77', '3', '3');
INSERT INTO `users_details` VALUES ('78', 'Vladimir', 'Reilly', '1992-06-25', null, '78', '3', '3');
INSERT INTO `users_details` VALUES ('79', 'Quamar', 'Lindsay', '1966-12-12', null, '79', '3', '3');
INSERT INTO `users_details` VALUES ('80', 'Troy', 'Hughes', '1992-01-05', null, '80', '3', '3');
INSERT INTO `users_details` VALUES ('81', 'Connor', 'Waller', '1985-11-19', null, '81', '3', '3');
INSERT INTO `users_details` VALUES ('82', 'Phyllis', 'Vincent', '1976-03-16', null, '82', '3', '3');
INSERT INTO `users_details` VALUES ('83', 'Maisie', 'Graves', '1990-05-13', null, '83', '3', '3');
INSERT INTO `users_details` VALUES ('84', 'Ima', 'Browning', '1979-09-01', null, '84', '3', '3');
INSERT INTO `users_details` VALUES ('85', 'Alexander', 'Nieves', '1954-04-12', null, '85', '3', '3');
INSERT INTO `users_details` VALUES ('86', 'Indigo', 'Hopper', '1985-01-01', null, '86', '3', '3');
INSERT INTO `users_details` VALUES ('87', 'Ali', 'Griffith', '1970-09-21', null, '87', '3', '3');
INSERT INTO `users_details` VALUES ('88', 'Geoffrey', 'Stone', '1957-10-01', null, '88', '3', '3');
INSERT INTO `users_details` VALUES ('89', 'Jaime', 'Cannon', '1936-05-05', null, '89', '3', '3');
INSERT INTO `users_details` VALUES ('90', 'Frances', 'Giles', '1968-09-25', null, '90', '3', '3');
INSERT INTO `users_details` VALUES ('91', 'Breanna', 'Livingston', '1943-09-04', null, '91', '3', '3');
INSERT INTO `users_details` VALUES ('92', 'Elaine', 'Parks', '1983-12-06', null, '92', '3', '3');
INSERT INTO `users_details` VALUES ('93', 'Yoko', 'Kirby', '1936-06-12', null, '93', '3', '3');
INSERT INTO `users_details` VALUES ('94', 'Odette', 'Day', '1939-10-28', null, '94', '3', '3');
INSERT INTO `users_details` VALUES ('95', 'Hollee', 'Sears', '1962-10-27', null, '95', '3', '3');
INSERT INTO `users_details` VALUES ('96', 'Lunea', 'Livingston', '1947-05-25', null, '96', '3', '3');
INSERT INTO `users_details` VALUES ('97', 'Thomas', 'Pace', '1969-12-16', null, '97', '3', '3');
INSERT INTO `users_details` VALUES ('98', 'Emi', 'Dyer', '1972-11-30', null, '98', '3', '3');
INSERT INTO `users_details` VALUES ('99', 'Yeo', 'Hardy', '1938-02-15', null, '99', '3', '3');
INSERT INTO `users_details` VALUES ('100', 'Aline', 'Greene', '1989-04-23', null, '100', '3', '3');
INSERT INTO `users_details` VALUES ('103', 'Alie++', 'Greene+', '0000-00-00', '0', '103', '', '3');
INSERT INTO `users_details` VALUES ('104', 'Kiona1', 'Jimenez', '1955-10-23', '0', '104', '1', '1');
INSERT INTO `users_details` VALUES ('105', 'Nan', 'Li', '0000-00-00', '0430305838', '105', '2332', '1');
INSERT INTO `users_details` VALUES ('106', 'Lim', 'Ink', '0000-00-00', '0', '106', '', '3');
INSERT INTO `users_details` VALUES ('107', 'aaa', 'bbb', '0000-00-00', '0', '107', '', '3');
INSERT INTO `users_details` VALUES ('108', 'Nan', 'Li', '0000-00-00', '', '108', '', '3');

-- ----------------------------
-- Table structure for users_objectives
-- ----------------------------
DROP TABLE IF EXISTS `users_objectives`;
CREATE TABLE `users_objectives` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `objective_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`record_id`),
  KEY `fk_usersid_users` (`user_id`),
  KEY `fk_objectiveid_objectives` (`objective_id`),
  CONSTRAINT `fk_objectiveid_objectives` FOREIGN KEY (`objective_id`) REFERENCES `objectives` (`objective_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_usersid_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of users_objectives
-- ----------------------------
INSERT INTO `users_objectives` VALUES ('1', '30', '1');
SET FOREIGN_KEY_CHECKS=1;
