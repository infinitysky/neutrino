import { Component, OnInit } from '@angular/core';

import {MY_CONFIG} from  '../../app-config';
@Component({
  selector: 'app-footerpage',
  templateUrl: './footerpage.component.html',
  styleUrls: ['./footerpage.component.css']
})
export class FooterpageComponent implements OnInit {

  public currentYear: Date;
  public companyName: string;

  constructor( ) {
    this.currentYear = new Date();
    this.companyName = '©' + MY_CONFIG.companyName;

  }



  ngOnInit() {

  }

}
