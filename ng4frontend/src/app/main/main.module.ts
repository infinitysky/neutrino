import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';




import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';

import { FooterpageComponent } from './footerpage/footerpage.component';
import { NavigationComponent } from './navigation/navigation.component';

import { SharedModulesRegisterModule } from '../shared/shared-modules-register.module';

@NgModule({
  imports: [
    SharedModulesRegisterModule,

    CommonModule,
    MainRoutingModule
  ],
  declarations: [
    MainComponent,

    FooterpageComponent,
    NavigationComponent
  ]
})
export class MainModule { }
