import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';



import 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
// Observable class extensions
import 'rxjs/add/observable/of';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { Subscription } from 'rxjs/Subscription';


import { ModalDirective } from 'ngx-bootstrap/modal';



import {AlertService} from '../../../../shared/services/alert.service';
import { SettingGoalService } from '../../okr-shared/services/okr-goal.service';
import { Goalclass } from '../../okr-shared/classes/goal-class';
import { SettingTimeFrameService } from '../../okr-shared/services/okr-time-frame.service';
import { SettingTeamService } from '../../okr-shared/services/okr-team.service';
import {OkrActivitiesService} from '../../okr-shared/services/okr-activities.service';
import {UserInfoContainerService} from '../../../../shared/services/user-info-container.service';
import { ShareCompanyOkrinfoService } from '../share-company-okrinfo.service';
import {ReviewsService} from '../../okr-shared/services/reviews.service';


import { Timeframeclass } from '../../okr-shared/classes/time-frame-class';
import { Teamclass } from '../../okr-shared/classes/team-class';
import {Activityclass} from '../../okr-shared/classes/activitie-class';
import { Reviewclass } from '../../okr-shared/classes/review-class';
import { Userclass } from '../../../../shared/classes/user-class';



@Component({
  selector: 'app-company-okrs',
  providers: [SettingGoalService, SettingTimeFrameService, SettingTeamService, OkrActivitiesService, ReviewsService],
  templateUrl: './company-okrs.component.html',
  styleUrls: ['./company-okrs.component.css']
})
export class CompanyOkrsComponent implements OnInit {





  public modalTitle: string;

  public goals: Goalclass[];
  public displayGoals: Goalclass[];
  public timeFrames: Timeframeclass[];
  public teams: Teamclass[];
  public submitActivityInfo: Activityclass;

  public isAdmin: boolean;

  //goalModal parameter
  public goalsData: any;
  public errorMessage: any;

  public isLoaded: boolean;
  public selectedGoal: Goalclass;
  public selectedValue: any;
  public tempData: any;

  animation: boolean = true;
  keyboard: boolean = true;
  backdrop: string | boolean = "static";


  //current model
  public editModeIO: number;


  //edit mode parameter
  editGoal: Goalclass;
  goalNameInputBoxValue: string;
  goalDescriptionInputBoxValue: string;


  //Dropdownlist;
  public timeFrameDropdownListOptions: any;
  public selectedTimeFrame: any;


  public tagDropdownListOptions: any;
  public selectedTag: any;




  //For sharing service
  public toalGoalsNumber: any;

  public overallProgressNumber: any;

  private overallProgressNumberSubscription: Subscription;
  private overallGoalNumberSubscription: Subscription;


  public selfInfo: any;
  public userInfoSubscription: Subscription;


  private currentTimeFrameId: any;
  private timeFrameIdSubscription: any;

  public reviewInfo: Reviewclass;



  // RATINGS METHODS

  public max: number;
  public rate: number;
  public isReadonly: boolean;
  public overStar: number;
  public percent: number;
  public reviewDescription: string;
  @ViewChild('reviewGoalModal') reviewGoalModal: ModalDirective;



  constructor(
    private _reviewsService: ReviewsService,
    private _alertService: AlertService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _settingGoalService: SettingGoalService,
    private _settingTimeFrameService: SettingTimeFrameService,
    private _shareCompanyOkrinfoService: ShareCompanyOkrinfoService,
    private _userInfoContainerService: UserInfoContainerService,
    private _okrActivitiesService: OkrActivitiesService
  ) {
    this.isLoaded = true;
    this.modalTitle = '';
    this.goals = new Array<Goalclass>();
    this.isAdmin = false;
    this.timeFrames = new Array<Timeframeclass>();
    this.editModeIO = 0;
    this.editGoal = new Goalclass();
    this.goalNameInputBoxValue = '';
    this.goalDescriptionInputBoxValue = '';
    this.timeFrameDropdownListOptions = [];
    this.selectedTimeFrame = [];
    this.toalGoalsNumber = ' - ';
    this.submitActivityInfo=new Activityclass();
    this.selfInfo= '';





    this.tagDropdownListOptions=[{ id: "None", text: "None" },{ id: "Warning", text: "Warning" },{ id: "Risk", text: "Risk" },{ id: "Complete", text: "Complete" }];
    this.selectedTag=[{ id: "None", text: "None" }];



    // rating
    this.isReadonly = true;
    this.max = 10;
    this.rate = 0;
    this.overStar = 0;
    this.percent = 0;
    this.reviewDescription = '';


  }



  //component functions
  ngOnInit() {
    this.timeFrameIdParameterSubscribe();
    this.getSelfInfo();


    this.getAllTimeFrames();
    this.getOverallProgressNumber();
    this.getTotalGoalNumber();


  }

  ngOnDestroy() {
    this.timeFrameIdSubscription.unsubscribe();
    this.overallGoalNumberSubscription.unsubscribe();
    this.overallProgressNumberSubscription.unsubscribe();
    this.userInfoSubscription.unsubscribe();

  }





  refreshButton(){
    this.timeFrameIdParameterSubscribe();
    this.getSelfInfo();
    this.getAllTimeFrames();
    this.getOverallProgressNumber();
    this.getTotalGoalNumber();
  }





  editButton() {
    this.isLoaded = !this.isLoaded;
  }

  addGoalButton() {

    this.modalTitle="Create A Goal";


    this.editModeIO = 0;

   //this.getAllTimeFrames();
    this.selectedTag = [{ id: "None", text:"None"}];

    this.selectedTimeFrame = [];
    this.goalNameInputBoxValue = "";
    this.goalDescriptionInputBoxValue = "";

    this.goalModal.show();

  }




  deleteGoalButton(Goal) {
    //this.showAlert();
    this._settingGoalService
      .delete(Goal)
      .subscribe(
        data => { this.tempData = data },
        error => { this.errorMessage = <any>error },
        () => {

          if (this.tempData.data.affectRows > 0) {
            this._alertService.displaySuccessMessage('Your goal has been deleted.');

            this.goals = this.goals.filter(currentGoals => currentGoals !== Goal);

            this.updateOverallNumbers();

          } else {
            this._alertService.displayErrorMessage('Your goal did not been deleted successfully.');

          }
        }
      );
  }


  modalSaveChangeButton(goalNameInput: string, goalDescription: string) {


    if (0 == this.editModeIO) {
      this.createNewGoal(goalNameInput, goalDescription);
    } else {
      this.updateGoal(this.editGoal, goalNameInput, goalDescription);
    }
  }


  editGoalsButton(Goal) {
    this.modalTitle="Update A Goal";

    this.editModeIO = 1;
    this.editGoal = Goal;
    this.goalNameInputBoxValue = Goal.goal_name;
    this.goalDescriptionInputBoxValue = Goal.goal_description;



    var timeFrameName = Goal.time_frame_description
      + "    --- (" + Goal.time_frame_start +
      " To " + Goal.time_frame_end + ")";

    // var tempInfo={id:teams[i].team_id, name:teams[i].team_name};
    //var tempInfo1={id:timeframes[i].time_frame_id, text:timeFrameName};
    this.selectedTimeFrame = [{ id: Goal.time_frame_id, text: timeFrameName }];
    this.selectedTag = [{ id: Goal.goal_status, text: Goal.goal_status }];

    this.getAllTimeFrames();



    this.goalModal.show();

  }




  closeReviewGoalButton(){
    this.reviewInfo = new Reviewclass();
    this.reviewGoalModal.hide();
  }







  //--------------------------------- Button ----------------------------------------









  getSelfInfo() {
    this.userInfoSubscription = this._userInfoContainerService.userInfo$.subscribe( userData => this.selfInfo = userData );
    if( this.selfInfo){
      if( this.selfInfo.role == 'admin'){
        this.isAdmin = true;
        this.isReadonly = false;
      }
    }else{
      this.selfInfo = new Userclass();
    }

  }



  getAllTimeFrames() {
    this._settingTimeFrameService.getAllTimeFrames()
      .subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {


          if (this.tempData.status == "success" && this.tempData.data) {
            this.timeFrames =<Timeframeclass[]> this.tempData.data;
            this.setTimeFrameDropdownList(this.timeFrames);
          }
        }
      );

  }









  updateGoal(editGoal, goalNameInput: string, goalDescription: string) {

    if (!goalNameInput) {
      //alert("Do not leave any empty!");
      // swal("Warning", "you did not change any time!", "warning");\
      return;
    } else {

      let originalGoal = editGoal;


      editGoal.goal_description = goalDescription;
      editGoal.goal_name = goalNameInput;
      var timeFrameId = this.selectedTimeFrame[0].id;
      var goalStatusTag =this.selectedTag[0].id;



      editGoal.time_frame_id = timeFrameId;

      editGoal.goal_status = goalStatusTag;



      this._settingGoalService.update(editGoal)
        .subscribe(
          data => { this.tempData = data },
          error => this.errorMessage = <any>error,
          () => {
           // console.log("update Members this.tempData + " + JSON.stringify(this.tempData));
            //console.log(this.tempData.data);

            if(this.tempData.status == "success" && this.tempData.data)  {
              if (editGoal.time_frame_id != this.currentTimeFrameId){
                this.goals = this.goals.filter(currentGoals => currentGoals !== editGoal);

               // this.updateOverallNumbers();
              }



              this._alertService.displaySuccessMessage('Your goal has been updated. <br> affectRows: ' + this.tempData.data.affectRows);
              // this.updateTeamMembers(editTeam,this.memberSelectedOptions);
              this.goalNameInputBoxValue = "";
              this.goalDescriptionInputBoxValue = "";
              this.updateOverallNumbers();

              var submitANewActivity= new Activityclass();

              var modifyLog = "";
              if (originalGoal.goal_description!=editGoal.goal_description){
                modifyLog=modifyLog+" Change Goal description  to"+ editGoal.goal_description+"; ";
              }
              if (originalGoal.goal_name!=editGoal.goal_name){
                modifyLog=modifyLog+"Change goal name to"+ editGoal.goal_name+"; ";
              }
              if (originalGoal.goal_status!=editGoal.goal_status){
                modifyLog=modifyLog+"Change goal tag to"+ editGoal.goal_status+"; ";
              }

              submitANewActivity.user_id=this.selfInfo.user_id;
              submitANewActivity.activity_detail = "Updated goal : "
                + editGoal.goal_name+ " update log : "+modifyLog ;
              submitANewActivity.activity_type="Update";
              this.submitActivity(submitANewActivity);




            }else {
              //swal("Warning", this.tempData.errorMessage, "warning");
              this._alertService.displayErrorMessage(this.tempData.errorMessage);

            }

          }
        );


    }

    this.goalModal.hide();

  }




  getGoals() {
    // this._settingGoalService.getAll()
    this._settingGoalService.getAllDetailed()
      .subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {
          if (this.tempData.status.toLowerCase() == "success" && this.tempData.data) {
            this.goals = this.tempData.data;
            this.goals.sort();
            this.updateOverallNumbers();
          }

        }
      );

  }

  getGoalsByTimeFrameId(timeFrameId) {
    // this._settingGoalService.getAll()

    this.goals = new Array<Goalclass>();
    this._settingGoalService.getByTimeFrameId(timeFrameId)
      .subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {
          if (this.tempData.status.toLowerCase() == 'success' && this.tempData.data) {
            this.goals = this.tempData.data;
            this.goals.sort();
            this.updateOverallNumbers();
          }else{

            //console.log('empty');
            this.updateOverallNumbers();
          }

        }
      );

  }





  calculateOverallProgress():number{
    var totalNumber =0;
    var OverallProgress = 0;
    var i=0;
    if(this.goals.length != 0){
      for (i = 0; i < this.goals.length; i++) {
        totalNumber = totalNumber + Number(this.goals[i].goal_progress_status);
      }
      OverallProgress = totalNumber / this.goals.length;
    }else {

      OverallProgress = 0;
    }

    return OverallProgress;

  }





  createNewGoal(goalNameInput: string, goalDescription: string) {



    if (!goalNameInput || !goalDescription) {
      //alert("Do not leave any empty!");
      this._alertService.displayWarningMessage('Do not leave any empty!');

      return;
    }
    else {

     // const timeFrameId = this.selectedTimeFrame[0].id;
      const timeFrameId = this.currentTimeFrameId;
      const goalStatusTag = this.selectedTag[0].id;



      this._settingGoalService.addNew(goalNameInput, goalDescription, timeFrameId, goalStatusTag ).subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {

          if (this.tempData.status == "success" && this.tempData.data) {

            let tempInfo = <Goalclass>this.tempData.data;
            let searchedTimeFrame = this.timeFrames.find(x => x.time_frame_id == tempInfo.time_frame_id);
           // const searchedTimeFrame = this.timeFrames.find(x => x.time_frame_id == this.currentTimeFrameId);

            tempInfo.time_frame_description = searchedTimeFrame.time_frame_description;
            tempInfo.time_frame_start = searchedTimeFrame.time_frame_start;
            tempInfo.time_frame_end = searchedTimeFrame.time_frame_end;
            let tempArray = [];
            tempArray.push(tempInfo);
            let i = 0;
            for (i = 0; i < this.goals.length;i++){
              tempArray.push(this.goals[i]);
            }
            this.goals = tempArray;
            this.updateOverallNumbers();
            this.goalNameInputBoxValue = "";
            this.goalDescriptionInputBoxValue = "";

            var submitANewActivity= new Activityclass();
            submitANewActivity.user_id=this.selfInfo.user_id;
            submitANewActivity.activity_detail = " Created a new goal : " + tempInfo.goal_name;
            submitANewActivity.activity_type="Create";
            this.submitActivity(submitANewActivity);

            this._alertService.displaySuccessMessage('');

          } else {
            this._alertService.displayErrorMessage(this.tempData.errorMessage);

          }

        }
      );
    }





    this.goalModal.hide();
  }



  getTotalGoalNumber(){


    this.overallGoalNumberSubscription = this._shareCompanyOkrinfoService._shareGoals$.subscribe(data => this.toalGoalsNumber = data);
    if (!this.toalGoalsNumber) {
      this.toalGoalsNumber = ' - ';
    }

  }

  getOverallProgressNumber(){

    this.overallProgressNumberSubscription = this._shareCompanyOkrinfoService._shareOverallProgressNumber$.subscribe(data => this.overallProgressNumber = data);

    if (!this.overallProgressNumber) {
      this.overallProgressNumber = ' - ';
    }
  }






  timeFrameIdParameterSubscribe(){

    this.timeFrameIdSubscription = this._activatedRoute.queryParams.subscribe(params => {
      this.currentTimeFrameId =  +params['timeFrameId'] || 0;
      //this.getGoals();
      //console.log('current time frame : ' + this.currentTimeFrameId);

      if (this.currentTimeFrameId==0){
        this.getGoals();
      }else {
        this.getGoalsByTimeFrameId(this.currentTimeFrameId);
      }

    });

  }







  setTimeFrameDropdownList(timeframes: Timeframeclass[]) {
    var i = 0;
    var tempArray = [];

    //var NonInfo={id:"0", text:"None"};
    for (i = timeframes.length - 1; i > 0; i--) {
      var timeFrameName = timeframes[i].time_frame_description
        + "   --- (" + timeframes[i].time_frame_start +
        " To " + timeframes[i].time_frame_end + ")";

      // var tempInfo={id:teams[i].team_id, name:teams[i].team_name};
      var tempInfo1 = { id: timeframes[i].time_frame_id, text: timeFrameName };
      tempArray.push(tempInfo1);

    }
    // This way is working...
    this.timeFrameDropdownListOptions = tempArray;

  }



  updateOverallNumbers() {

    const overAllProgressNumber = this.calculateOverallProgress();

    //console.log(overAllProgressNumber);

    this._shareCompanyOkrinfoService.setOverAllProgressSubject(overAllProgressNumber);


    this._shareCompanyOkrinfoService.setGoalsSubject(this.goals.length);
  }







  //goalModal setting and control


  //Modal actions
  @ViewChild('goalModal')  goalModal: ModalDirective;


  closeGoalModal() {
    this.goalDescriptionInputBoxValue = "";
    this.goalNameInputBoxValue = "";
    this.goalModal.hide();
  }

  openGoalModal() {

    this.goalModal.show();
  }













  // Rate
  public hoveringOver(value:number):void {
    this.overStar = value;
    this.percent = 100 * (value / this.max);
  };

  public resetStar():void {
    this.overStar = void 0;
  }

  // Review Modals
  reviewGoalButton( reviewGoal: Goalclass){
    // console.log(reviewObjective);
    this.editGoal = new Goalclass();

    this.editGoal = reviewGoal;
    if(reviewGoal && reviewGoal.reviewed == 1 ){
      this.loadReviewInfo(reviewGoal);
    }else{
      this.reviewInfo = new Reviewclass();
      this.rate = this.reviewInfo.review_rating;
      this.reviewDescription =  this.reviewInfo.review_details;
    }

    this.reviewGoalModal.show();
  }


  loadReviewInfo(reviewGoal: Goalclass){

    if (reviewGoal.reviewed == 1) {
      this._reviewsService.getByObjectiveId(reviewGoal.goal_id).subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any> error,
        () => {
          if (this.tempData.status == 'success' && this.tempData.data) {
            this.reviewInfo = <Reviewclass>this.tempData.data;
            this.rate = this.reviewInfo.review_rating;
            this.reviewDescription =  this.reviewInfo.review_details;
          }
        }
      );
    }else{
      this.reviewInfo = new Reviewclass();
      this.rate = this.reviewInfo.review_rating;
      this.reviewDescription =  this.reviewInfo.review_details;
    }
  }

  saveReviewGoalButton( reviewGoal: Goalclass){



    if(reviewGoal ){
      if (reviewGoal.reviewed == 0) {
        if( !this.rate || this.rate == 0){
          this._alertService.displayWarningMessage('Please rate this Goal');
        }else if (!this.reviewDescription || this.reviewDescription == ''){
          this._alertService.displayWarningMessage('Please leave your comments for this Goal');
        } else {

          this.reviewInfo = new Reviewclass();
          this.reviewInfo.review_details = this.reviewDescription;
          this.reviewInfo.review_rating = this.rate;
          this.reviewInfo.review_user_id = this.selfInfo.user_id;
          this.reviewInfo.review_object_type = 'o';
          this.reviewInfo.review_object_id = reviewGoal.goal_id;
          this.createNewReview(this.reviewInfo, reviewGoal);

        }



      }else{
        this.reviewInfo.review_rating = this.rate;
        this.reviewInfo.review_details = this.reviewDescription ;
        this.updateReviewInfo(this.reviewInfo, reviewGoal);
      }

    }

    this.reviewGoalModal.show();
  }


  createNewReview(newReviewInfo: Reviewclass, reviewGoal: Goalclass){
    this._reviewsService.addNewByClass(newReviewInfo).subscribe(
      data => this.tempData = data,
      error => this.errorMessage =<any>error,
      () => {
        if(this.tempData.status == 'success' && this.tempData.data) {
          this.updateGoalReviewStatus(reviewGoal);

        }else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }
      }
    );

  }

  updateReviewInfo(ReviewInfo: Reviewclass, reviewGoal: Goalclass){
    this._reviewsService.update(ReviewInfo).subscribe(
      data => this.tempData = data,
      error => this.errorMessage =<any>error,
      () => {
        if(this.tempData.status == 'success' && this.tempData.data) {
          this._alertService.displaySuccessMessage('Your Review Infomation has been updated. <br> affectRows: ' + this.tempData.data.affectRows);

          this.createActivity(
            'Updated',
            'Updated reviewed Info for Objective ' + reviewGoal.goal_name,
            '', // activity comment
            'g',
            reviewGoal.goal_id
          ); //action: string, activityDetails: string, groupType: string, groupId: string

        }else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }
        this.reviewGoalModal.hide();
      }
    );
  }

  updateGoalReviewStatus( goal: Goalclass ){
    goal.reviewed = 1;
    goal.goal_status = 'Complete';
    this._settingGoalService.update(goal).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if(this.tempData.status == 'success' && this.tempData.data) {
          this.createActivity(
            'Review',
            'Reviewed Objective ' + goal.goal_name,
            '', // activity comment
            'g',
            goal.goal_id
          ); //action: string, activityDetails: string, groupType: string, groupId: string
          this._alertService.displaySuccessMessage('Success!');

        }else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }
        this.reviewGoalModal.hide();
      }
    );

  }


  createActivity(action: string, activityDetails: string, activityComment: string, groupType: string, groupId: number){

    const submitANewActivity = new Activityclass();
    submitANewActivity.user_id =  this.selfInfo.user_id;
    submitANewActivity.activity_comment =  activityComment;
    submitANewActivity.activity_detail = activityDetails; // "Created a new Objective : " + newObjective.objective_name;
    submitANewActivity.activity_type = action;
    submitANewActivity.activity_group = groupType;
    submitANewActivity.activity_group_id = groupId;
    this.submitActivity(submitANewActivity);

  }


  submitActivity(activity: any) {
    this._okrActivitiesService.addNewByClass(activity).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if(this.tempData.data && this.tempData && <Activityclass>this.tempData.data){
          // swal("Success!", "Your goal has been created.", 'success');

        }
      }
    );
  }













}
