﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { UsersInfoService } from '../../../shared/services/users-info.service';

import { OkrCompanyService } from '../okr-shared/services/okr-company.service';
import { SystemSettingService } from '../../../shared/services/system-setting.service';


import { ShareCompanyOkrinfoService } from './share-company-okrinfo.service';
import { CompanyDetailClass } from '../../../shared/classes/company-detail-class';
import { Systemsettingclass } from '../../../shared/classes/system-setting-class';


import {CompanyActivityComponent} from './company-activity/company-activity.component';

@Component({
  selector: 'app-okr-company',
  templateUrl: './okr-company.component.html',
  providers: [OkrCompanyService, UsersInfoService, SystemSettingService],
  styleUrls: ['./okr-company.component.css']
})
export class OkrCompanyComponent implements OnInit {

  public companyInfo: CompanyDetailClass;
  public systemSettingInfo: Systemsettingclass[];
  public tempData: any;
  public goals: any;


  public errorMessage: string;
  public totalMemberNumber: any;
  public totalGoalsNumber: any;

  public overallProgressNumber: any;
  private overallProgressNumberSubscription: Subscription;
  private overallGoalNumberSubscription: Subscription;

  private currentTimeFrameId: any;
  private timeFrameIdSubscription: any;

  @ViewChild(CompanyActivityComponent)  CompanyActivityComponent: CompanyActivityComponent;


  constructor(
    private _systemSettingService: SystemSettingService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _okrCompanyService: OkrCompanyService,
    private _usersInfoService: UsersInfoService,
    private _shareCompanyOkrinfoService: ShareCompanyOkrinfoService
  ) {
    this.companyInfo = new CompanyDetailClass;
    this.totalMemberNumber = ' - ';
    this.totalGoalsNumber = ' - ';
    this.overallProgressNumber = ' - ';
    this.goals = '';

  }

  ngOnInit() {
    this.getCompanyInfo();
    this.getTotalNumber();
    this.getTotalGoalNumber();
    this.getOverallPragressNumber();
    this.timeFrameIdParameterSubscribe();


  }

  ngOnDestroy() {
    this.timeFrameIdSubscription.unsubscribe();
    this.overallGoalNumberSubscription.unsubscribe();
    this.overallProgressNumberSubscription.unsubscribe();

  }

  timeFrameIdParameterSubscribe(){

    this.timeFrameIdSubscription = this._activatedRoute.queryParams.subscribe(params => {
      this.currentTimeFrameId =  +params['timeFrameId'] || 0;

    });

  }

  getTotalGoalNumber() {

    this.overallGoalNumberSubscription = this._shareCompanyOkrinfoService._shareGoals$.subscribe(data => this.totalGoalsNumber = data);

    if (!this.totalGoalsNumber) {
      this.totalGoalsNumber = ' - ';
    }
  }

  getOverallPragressNumber() {

    this.overallProgressNumberSubscription = this._shareCompanyOkrinfoService._shareOverallProgressNumber$.subscribe(data => this.overallProgressNumber = data);

    if (!this.overallProgressNumber) {
      this.overallProgressNumber = '0';
    }
  }





  getTotalNumber() {
    this._usersInfoService.getTotalNumber().subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData.data) {
          this.totalMemberNumber = this.tempData.data.membersNumber;
        }

      }
    );

  }

  getCompanyInfo() {
    this.systemSettingInfo = new Array<Systemsettingclass>();
    this.companyInfo = new CompanyDetailClass();

    this._systemSettingService.getCompanyInfo().subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any> error,

      () => {
        if (this.tempData.status.toLowerCase() == 'success' && this.tempData.data) {
          this.systemSettingInfo = <Systemsettingclass[]>this.tempData.data;
          // this.goals.sort();

          let i = 0;
          for (i =0; i < this.systemSettingInfo.length; i++ ) {
            switch (this.systemSettingInfo[i].setting_name){
              case 'company_name':
                this.companyInfo.company_name = this.systemSettingInfo[i].setting_value;
                break;

              case 'company_address':
                this.companyInfo.company_address = this.systemSettingInfo[i].setting_value;
                break;

              case 'company_email':
                this.companyInfo.company_email = this.systemSettingInfo[i].setting_value;
                break;

              case 'company_mission':
                this.companyInfo.company_mission = this.systemSettingInfo[i].setting_value;
                break;

              case 'company_phone':
                this.companyInfo.company_phone = this.systemSettingInfo[i].setting_value;
                break;

              case 'company_vision':
                this.companyInfo.company_vision = this.systemSettingInfo[i].setting_value;
                break;

              default:
                break;

            }

          }

        }else{
          this.systemSettingInfo = new Array<Systemsettingclass>();
          this.companyInfo = new CompanyDetailClass();

        }

      }
    );

  }

  /*
   changeName() {
   this.companyinfo.company_name = 'lololo';

   console.log(this.toalGoalsNumber);
   }

   updateOverallNumber(event) {
   this.toalGoalsNumber = event;
   }
   */

}
