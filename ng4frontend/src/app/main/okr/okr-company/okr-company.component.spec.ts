﻿/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { OkrCompanyComponent } from './okr-company.component';

describe('OkrCompanyComponent', () => {
  let component: OkrCompanyComponent;
  let fixture: ComponentFixture<OkrCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OkrCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
     

    fixture = TestBed.createComponent(OkrCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
