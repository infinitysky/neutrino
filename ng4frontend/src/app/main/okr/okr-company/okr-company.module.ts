import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';





//3rd party library module

import {SharedModulesRegisterModule} from '../../../shared/shared-modules-register.module';



import { OkrCompanyRoutingModule } from './okr-company-routing.module';

import { CompanyOkrsComponent } from './company-okrs/company-okrs.component';
import { OkrCompanyComponent } from './okr-company.component';
import { CompanyActivityComponent } from './company-activity/company-activity.component';
import { CompanyTeamsComponent } from './company-teams/company-teams.component';

import {ShareCompanyOkrinfoService} from './share-company-okrinfo.service';


@NgModule({
    imports: [

      SharedModulesRegisterModule,
        CommonModule,

        CommonModule,
        OkrCompanyRoutingModule
    ],
    providers:[ShareCompanyOkrinfoService],
    declarations: [OkrCompanyComponent, CompanyActivityComponent, CompanyTeamsComponent, CompanyOkrsComponent]
})
export class OkrCompanyModule { }
