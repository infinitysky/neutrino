import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//import 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
// Observable class extensions
import 'rxjs/add/observable/of';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { Subscription } from 'rxjs/Subscription';




import {Activityclass} from '../../okr-shared/classes/activitie-class';
import {OkrActivitiesService} from '../../okr-shared/services/okr-activities.service';
import {UserInfoContainerService} from '../../../../shared/services/user-info-container.service';

@Component({
  selector: 'app-company-activity',
  templateUrl: './company-activity.component.html',
  providers:[OkrActivitiesService],
  styleUrls: ['./company-activity.component.css']
})
export class CompanyActivityComponent implements OnInit {
  public content: any;
  public activitiesInfo: Activityclass[];

  public retryTimes: number;






  private selfUserInfoData: any;
  private selfInfoSubscription: Subscription;
  private currentTimeFrameId: any;
  private timeFrameIdSubscription: Subscription;

  public isLoading: boolean;




  public tempData: any;
  public errorMessage: any;



  constructor(
    private _activatedRoute: ActivatedRoute,
    private _okrActivitiesService: OkrActivitiesService,
    private _userInfoContainerService: UserInfoContainerService,
  ) {
    this.content = '';

    this.activitiesInfo = [];

    this.selfUserInfoData = '';

    this.retryTimes = 5;
    this.isLoading = true;

  }

  ngOnInit() {

    this.loadCompanyActivitiesInfo();

  }

  ngOnDestroy() {
    this.timeFrameIdSubscription.unsubscribe();
    this.selfInfoSubscription.unsubscribe();
  }

  loadCompanyActivitiesInfo(){
    this.getCurrentUserInfo();
    this.timeFrameIdParameterSubscribe();
  }


  getTimeFrameActivities(timeFrameId: any){
    this.isLoading = true;
    this.activitiesInfo = new Array<Activityclass>();

    this._okrActivitiesService.getByTimeFrame(timeFrameId).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any> error,
      () => {
        this.isLoading = false;
        if (this.tempData && this.tempData.data) {

          this.activitiesInfo = this.tempData.data;
        }
      }

    );
  }


  getCurrentUserInfo(){
    this.selfInfoSubscription=this._userInfoContainerService.userInfo$.subscribe(userInfo => this.selfUserInfoData = userInfo);

  }

  timeFrameIdParameterSubscribe(){

    this.timeFrameIdSubscription = this._activatedRoute.queryParams.subscribe(params => {
      this.currentTimeFrameId =  +params['timeFrameId'] || 0;
      //this.getGoals();
      //console.log('current time frame : ' + this.currentTimeFrameId);
      this.getTimeFrameActivities(this.currentTimeFrameId );

    });

  }





}
