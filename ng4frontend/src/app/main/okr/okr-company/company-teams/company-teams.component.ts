import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';



import {SettingTeamService} from '../../okr-shared/services/okr-team.service';
import {UserDetailsService} from '../../../../shared/services/user-details.service';
import { UserInfoContainerService } from '../../../../shared/services/user-info-container.service';

import {Userclass} from '../../../../shared/classes/user-class';
import {Teamclass} from '../../okr-shared/classes/team-class';

@Component({
  selector: 'app-company-teams',
  templateUrl: './company-teams.component.html',
  providers:[SettingTeamService, UserDetailsService],
  styleUrls: ['./company-teams.component.css']
})
export class CompanyTeamsComponent implements OnInit {

  public teams: Teamclass[];
  public users: Userclass[];

  private tempData: any;
  private errorMessage: any;

  public teamLength: number;

  private currentTimeFrameId: any;
  private timeFrameIdSubscription: any;

  private selfInfo: Userclass;
  private userInfoSubscription: Subscription;
  public isAdmin: boolean;


  constructor(
    private _userInfoContainerService: UserInfoContainerService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _settingTeamService: SettingTeamService
  ){
    this.isAdmin = false;
    this.teams = [];
    this.users = [];
    this.teamLength = 0;
  }

  ngOnInit(){
    this.getSelfInfo();
    this.timeFrameIdParameterSubscribe();
    //this.getTeams();

  }



  ngOnDestroy(){
    this.timeFrameIdSubscription.unsubscribe();
  }


  timeFrameIdParameterSubscribe(){

    this.timeFrameIdSubscription = this._activatedRoute.queryParams.subscribe(params => {
      this.currentTimeFrameId =  +params['timeFrameId'] || 0;
      // console.log('Query param currentTimeFrame: ', this.currentTimeFrameId);
      this.getTeamsByTimeFrame(this.currentTimeFrameId );
    });

  }



  getSelfInfo() {
    this.userInfoSubscription = this._userInfoContainerService.userInfo$.subscribe( userData => this.selfInfo = userData );
    if( this.selfInfo){
      if( this.selfInfo.role == 'admin'){
        this.isAdmin = true;
      }
    }else{
      this.selfInfo = new Userclass();
    }

  }



  getTeams() {
    // console.log("get All teams");
    this._settingTeamService.getAll()
      .subscribe(
        data => this.tempData = data,
        error =>  this.errorMessage = <any>error,
        ()=>{


          if(this.tempData.data && <Teamclass[]>this.tempData.data){
            this.teams=<Teamclass[]>this.tempData.data;
            this.teamLength=this.teams.length;
          }

        }
      );

  }


  getTeamsByTimeFrame(timeFrameId){


    this._settingTeamService.getCurrentTeamProgressAndMember(timeFrameId)
      .subscribe(
        data => this.tempData = data,
        error =>  this.errorMessage = <any>error,
        () => {
          // console.log( "this.TeamsData + "+JSON.stringify(this.TeamsData.data));
          if(this.tempData && this.tempData.data ){

            this.teams = <Teamclass[]>this.tempData.data;
            this.teamLength = this.teams.length;
          }

        }
      );

  }


}
