import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';


import { Subscription } from 'rxjs/Subscription';

import {UsersInfoService} from '../../../shared/services/users-info.service';
import {UserDetailsService} from '../../../shared/services/user-details.service';

import {ShareUserOkrinfoService} from './share-user-okrinfo.service';//provide has been set in okr-users.module
import {UserInfoContainerService} from '../../../shared/services/user-info-container.service';
import {Userclass} from '../../../shared/classes/user-class';




@Component({

  selector: 'app-okr-users',
  templateUrl: './okr-users.component.html',
  providers:[UsersInfoService, UserDetailsService],
  styleUrls: ['./okr-users.component.css']
})
export class OkrUsersComponent implements OnInit {
  pageTitle = 'User Info';


  public displayUserInfoData: Userclass;

  public selfUserId:number;
  public viewUserID:any;
  public tempData:any;
  public errorMessage:any;
  public routerParamsSubscription:any;


  public objectivesNumber:any;
  public lastUpdate:any;


  private selfUserInfoData:Userclass;
  private selfInfoSubscription:Subscription;

  private targetUserInfoData: Userclass;
  private targetUserInfoSubscription:Subscription;

  public letterAvatar:string='';


  public overallObjectivesNumberSubscription: Subscription;
  public overallProgressNumberSubscription: Subscription;
  public totalObjectivesNumber:any;
  public overallProgressNumber:any;


  constructor(
    private _usersInfoService: UsersInfoService,
    private _userInfoContainerService: UserInfoContainerService,
    private _userDetailsService: UserDetailsService,
    private _shareUserOkrinfoService: ShareUserOkrinfoService,
    private _activatedRoute: ActivatedRoute
  ) {

    this.selfUserInfoData = new Userclass();
    this.displayUserInfoData=new Userclass();

    this.selfUserId=0;
    this.viewUserID=0;

    this.overallProgressNumber=' - ';
    this.objectivesNumber=' - ';
    this.lastUpdate=' - ';
    this.letterAvatar="a";
  }


  ngOnInit() {

    this.getSelfInfo();
    this.routerSubscription();
    this.getTotalObjectivesNumber();
    this.getOverallProgressNumber();

  }


  ngOnDestroy() {
    this.selfInfoSubscription.unsubscribe();
    this.routerParamsSubscription.unsubscribe();
    this.overallObjectivesNumberSubscription.unsubscribe();
    this.overallProgressNumberSubscription.unsubscribe();
  }

  routerSubscription(){

    this.routerParamsSubscription = this._activatedRoute.params.subscribe(params => {
      this.viewUserID = ''+params['userid']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
      this.viewUserID=Number(this._activatedRoute.snapshot.params['userid']);
      if(this.selfUserInfoData.user_id !=this.viewUserID ){

        this.getTargetUserInfo();
      }else{
        this.displayUserInfoData=this.selfUserInfoData;
        this.letterAvatar=(this.displayUserInfoData.first_name.charAt(0) + this.displayUserInfoData.last_name.charAt(0)).toUpperCase();

        this._shareUserOkrinfoService.setTargetUserInfoSubject(this.selfUserInfoData);
      }
    });
  }


  getSelfInfo(){
    this.selfInfoSubscription=this._userInfoContainerService.userInfo$.subscribe(selfInformation=>this.selfUserInfoData=selfInformation);

  }
  getTargetUserInfo(){
    this._userDetailsService.getById(this.viewUserID).subscribe(
      data=>this.tempData = data,
      error =>  this.errorMessage = <any>error,
      ()=>{
        this.displayUserInfoData = <Userclass>this.tempData.data;
        this.letterAvatar=(this.displayUserInfoData.first_name.charAt(0)+this.displayUserInfoData.last_name.charAt(0)).toUpperCase();
        this._shareUserOkrinfoService.setTargetUserInfoSubject(this.displayUserInfoData);

      }
    );
  }



  getTotalObjectivesNumber() {

    this.overallObjectivesNumberSubscription = this._shareUserOkrinfoService._shareObjectivesNumber$.subscribe(data => this.totalObjectivesNumber = data);
    if (!this.totalObjectivesNumber) {
      this.totalObjectivesNumber = 0;
    }
  }

  getOverallProgressNumber() {

    this.overallProgressNumberSubscription = this._shareUserOkrinfoService._shareOverallProgressNumber$.subscribe(data => this.overallProgressNumber = data);

    if (!this.overallProgressNumber) {
      this.overallProgressNumber = 0;
    }
  }







}
