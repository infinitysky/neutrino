import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
// Observable class extensions
import 'rxjs/add/observable/of';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { Subscription } from 'rxjs/Subscription';



import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { NouisliderModule } from 'ng2-nouislider';


//import swal from 'sweetalert2'
declare var swal: any;

import {UserInfoContainerService} from '../../../../shared/services/user-info-container.service';
import {OkrUserTeamsObjectivesService} from '../../okr-shared/services/okr-user-teams-objectives.service';
// goal data service
import { SettingGoalService } from '../../okr-shared/services/okr-goal.service';
// time frame data service
import { SettingTimeFrameService } from '../../okr-shared/services/okr-time-frame.service';
// team data service
import { SettingTeamService } from '../../okr-shared/services/okr-team.service';
// objective data service
import { SettingObjectiveService } from '../../okr-shared/services/okr-objective.service';
// key result data service
import { SettingKeyResultService } from '../../okr-shared/services/okr-key-result.service';
// activity data service. submit 'create' and 'update'
import {OkrActivitiesService} from '../../okr-shared/services/okr-activities.service';
import {AlertService} from '../../../../shared/services/alert.service';
import {OverallCalculatorService} from '../../okr-shared/services/overall-calculator.service';
import { ReviewsService } from '../../okr-shared/services/reviews.service';
import {ShareUserOkrinfoService} from '../share-user-okrinfo.service';
// classes here
import { Reviewclass } from '../../okr-shared/classes/review-class';
import { Timeframeclass } from '../../okr-shared/classes/time-frame-class';
import { Teamclass } from '../../okr-shared/classes/team-class';
import { Goalclass } from '../../okr-shared/classes/goal-class';
import { Objectiveclass } from '../../okr-shared/classes/objective-class';
import {Keyresultclass} from '../../okr-shared/classes/key-restult-class';
import {Activityclass} from '../../okr-shared/classes/activitie-class';
import {Userclass} from '../../../../shared/classes/user-class';



@Component({
  selector: 'app-okr-user-okr-team',
  templateUrl: './okr-user-okr-team.component.html',
  providers:[
    SettingGoalService,
    SettingTimeFrameService,
    SettingTeamService,
    SettingObjectiveService,
    SettingKeyResultService,
    OkrActivitiesService,
    ShareUserOkrinfoService,
    OkrUserTeamsObjectivesService,
    ReviewsService
  ],
  styleUrls: ['./okr-user-okr-team.component.css']
})
export class OkrUserOkrTeamComponent implements OnInit {


  public modalTitle: string;

  private userInfoSubscription: Subscription;
  private selfInfo: Userclass;
  public isAdmin: boolean;


  public goals: Goalclass[];
  public timeFrames: Timeframeclass[];
  public teams: Teamclass[];
  public teamObjectives: Objectiveclass[];

  public keyresults: Keyresultclass[];
  public teamkeyresults: Keyresultclass[];

  public keyResultsArray: Keyresultclass[];

  public newSubmitActivity: Activityclass;


  //goalModal parameter

  public errorMessage: any;

  public isLoaded: boolean = true;


  public tempData: any;


  //goalModal action control
  animation: boolean = true;
  keyboard: boolean = true;
  backdrop: string | boolean = "static";
  public modalType: string = ''; //objective | keyresult


  //current model
  public editModeIO: number;


  //edit mode parameter
  public editObjective: any;
  public parentObjective: any;
  public editKeyResult: any;
  public editReview: any;


  public objectiveNameInputBoxValue: string;
  public objectiveDescriptionInputBoxValue: string;

  public keyResultNameInputBoxValue: string;
  public keyResultDescriptionInputBoxValue: string;


  public keyresultProgressValue: any;
  public progressUpdateDescription: string;


  //Dropdownlist;
  public timeFrameDropDownListOptions: any;
  public selectedTimeFrame: any;

  public goalsDropDownListOptions: any;
  public selectedGoal: any;


//TODO: DO we have to set the tag on key-result?


  public tagDropDownListOptions: any;
  public selectedTag: any;


  //For sharing service
  public totalObjectivesNumber: any;

  public overallProgressNumber: any;

  public overallProgressNumberSubscription: Subscription;
  public overallObjectivesNumberSubscription: Subscription;

  public selfUserInfoData: Userclass;
  public selfInfoSubscription: Subscription;
  public targetUserInfoData: Userclass;
  public targetInfoSubscription: Subscription;


  public isLoading: boolean;

  public activitiesInfo: any;
  public activitiesLoading: boolean;

  public reviewInfo: Reviewclass;


  public currentTimeFrameId: any;
  public timeFrameIdSubscription: any;


  public routerParamsSubscription: any;
  public viewUserID: any;

//Multi Selection for Goals
  public goalsSelectorSettings: IMultiSelectSettings = {
    pullRight: false,
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default',
    selectionLimit: 0,
    closeOnSelect: false,
    showCheckAll: true,
    showUncheckAll: true,
    dynamicTitleMaxItems: 2,
    maxHeight: '300px',
  };

  public goalsSelectorTexts: IMultiSelectTexts = {
    checkAll: 'Check all',
    uncheckAll: 'Uncheck all',
    checked: 'checked',
    checkedPlural: 'checked',
    searchPlaceholder: 'Search...',
    defaultTitle: 'Select Goals',
  };



  // RATINGS METHODS

  public max: number;
  public rate: number;
  public isReadonly: boolean;
  public overStar: number;
  public percent: number;
  public reviewDescription: string;



  constructor(private _reviewsService: ReviewsService,
              private _alertService: AlertService,
              private _okrUserTeamsObjectivesService: OkrUserTeamsObjectivesService,
              private _settingGoalService: SettingGoalService,
              private _settingTimeFrameService: SettingTimeFrameService,
              private _settingTeamService: SettingTeamService,
              private _settingObjectiveService: SettingObjectiveService,
              private _settingKeyResultService: SettingKeyResultService,
              private _shareUserOkrinfoService: ShareUserOkrinfoService,
              private _userInfoContainerService: UserInfoContainerService,
              private _okrActivitiesService: OkrActivitiesService,
              private _activatedRoute: ActivatedRoute) {


    this.viewUserID = '';
    this.modalTitle = '';
    this.isAdmin = false;
    this.isReadonly = true;

    this.goals = [];
    this.timeFrames = [];


    this.teams = [];
    this.teamObjectives = [];
    this.teamObjectives = [];
    this.keyresults = [];


    this.editModeIO = 0;
    this.editObjective = new Objectiveclass();

    this.objectiveNameInputBoxValue = '';
    this.objectiveDescriptionInputBoxValue = '';

    this.editKeyResult = new Keyresultclass();
    this.keyResultNameInputBoxValue = '';
    this.keyResultDescriptionInputBoxValue = '';


    //Drop Down List
    this.timeFrameDropDownListOptions = [];
    this.selectedTimeFrame = [];


    this.goalsDropDownListOptions = [];
    this.selectedGoal = []; // multi selector is using different plugin

    this.totalObjectivesNumber = ' - ';
    this.newSubmitActivity = new Activityclass();
    this.selfUserInfoData = new Userclass();


    this.tagDropDownListOptions = [{id: "None", text: "None"}, {id: "Warning", text: "Warning"}, {
      id: "Risk",
      text: "Risk"
    }, {id: "Complete", text: "Complete"}];

    this.selectedTag = [{id: "None", text: "None"}];


  }


  ngOnInit() {
    this.routerSubscription();
    this.getCurrentUserInfo();
    this.getOverallProgressNumber();
    this.getTotalObjectivesNumber();
    this.targetSubscription();
    this.getSelfInfo();
    this.getAllKeyResult();

  }

  ngOnDestroy() {
    this.timeFrameIdSubscription.unsubscribe();
    this.overallObjectivesNumberSubscription.unsubscribe();
    this.overallProgressNumberSubscription.unsubscribe();
    this.routerParamsSubscription.unsubscribe();
    this.targetInfoSubscription.unsubscribe();
  }


  getSelfInfo() {
    this.selfInfo = new Userclass();
    this.userInfoSubscription = this._userInfoContainerService.userInfo$.subscribe(userData => this.selfInfo = userData);
    if (this.selfInfo) {
      if (this.selfInfo.role == 'admin') {
        this.isAdmin = true;
        this.isReadonly = false;
      }
    } else {
      this.selfInfo = new Userclass();
    }

  }

  getAllKeyResult() {
    this.keyResultsArray = new Array<Keyresultclass>();
    this._settingKeyResultService.getAll().subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any> error,
      () => {
        if (this.tempData.status == 'success' && this.tempData.data) {
          this.keyResultsArray = <Keyresultclass[]> this.tempData.data;

        }

      }
    );

  }


  routerSubscription() {
    //console.log("Router params userID:"+ this._activatedRoute.snapshot.params['userid']);
    this.routerParamsSubscription = this._activatedRoute.params.subscribe(params => {
      this.viewUserID = '' + params['userid']; // (+) converts string 'id' to a number
      // console.log("User OKRs this.viewUserID"+this.viewUserID);
      // In a real app: dispatch action to load the details here.
      this.viewUserID = Number(this._activatedRoute.snapshot.params['userid']);


      this.timeFrameIdSubscribe();


    });
  }


  timeFrameIdSubscribe() {

    this.timeFrameIdSubscription = this._activatedRoute.queryParams.subscribe(params => {
      // Defaults to 0 if no query param provided.
      this.currentTimeFrameId = +params['timeFrameId'] || 0;

      //  console.log('Query param currentTimeFrame: ', this.currentTimeFrameId);


      this.getTargetUserTeamOKRs(this.viewUserID, this.currentTimeFrameId);
      // this.getCurrentInfo();
    });

  }


  modalSaveChangeButton() {
    // read the 2 way binding;
    let objectiveNameInput = this.objectiveNameInputBoxValue;
    let objectiveDescription = this.objectiveDescriptionInputBoxValue;

    if (0 == this.editModeIO) {
      this.createNewObjective(objectiveNameInput, objectiveDescription);
    } else {
      this.updateObjective(this.editObjective, objectiveNameInput, objectiveDescription);
    }
  }


  modalSaveObjectiveChangeButton() {
    // read the 2 way binding;
    var objectiveNameInput = this.objectiveNameInputBoxValue;
    var objectiveDescription = this.objectiveDescriptionInputBoxValue;

    if (0 == this.editModeIO) {
      this.createNewObjective(objectiveNameInput, objectiveDescription);
    } else {
      this.updateObjective(this.editObjective, objectiveNameInput, objectiveDescription);
    }
  }


  modalSaveKeyResultChangeButton(keyResultNameInput, keyResultDescription) {
    // read the 2 way binding;
    // const keyResultNameInput = this.keyresultNameInputBoxValue;
    // const keyResultDescription = this.keyresultDescriptionInputBoxValue;
    console.log(keyResultNameInput);
    console.log(keyResultDescription);

    if (0 == this.editModeIO) {
      this.createNewKeyResult(keyResultNameInput, keyResultDescription, this.parentObjective);
    } else {
      this.updateKeyResult(this.editKeyResult, keyResultNameInput, keyResultDescription);
    }
  }


  modalSaveKeyResultProgressChangeButton() {
    // read the 2 way binding;
    if(!this.progressUpdateDescription){
      this._alertService.displayWarningMessage('Please leave your update log');
    }else{
      const progressUpdateDescription = this.progressUpdateDescription;



      this.keyResultProgressUpdate( this.editKeyResult, progressUpdateDescription );

      // this.updateKeyResultArray(this.editKeyResult);

      this.modalType = '';
      this.modalTitle = '';
      this.progressUpdateDescription = '';
      this.editModeIO = 0;
      this.editKeyResult = new Keyresultclass();


      this.updateProgressModal.hide();
    }



  }



  editObjectiveButton(objective: Objectiveclass) {
    this.modalTitle = "Update A Objective";

    this.editModeIO = 1;
    this.editObjective = objective;
    this.objectiveNameInputBoxValue = objective.objective_name;
    this.objectiveDescriptionInputBoxValue = objective.objective_description;


    this.selectedTag = [{id: objective.objective_status, text: objective.objective_status}];

    this.getAllTimeFrames();


  }


  addObjectiveButton() {
    this.getAllGoals();

    this.modalTitle = "Create A Objective";
    this.editModeIO = 0;
    this.selectedTag = [{id: "None", text: "None"}];
    this.selectedGoal = [];
    this.objectiveNameInputBoxValue = '';
    this.objectiveDescriptionInputBoxValue = '';

    this.objectiveModal.show();

  }

  addKeyResultButton(parentObjective) {
    this.modalType = 'keyresult';
    this.modalTitle = 'Create A Key Result';
    this.editModeIO = 0;

    this.parentObjective = new Objectiveclass();
    this.parentObjective = parentObjective;


    this.keyResultNameInputBoxValue = '';
    this.objectiveDescriptionInputBoxValue = '';
    this.keyresultProgressValue = 0;
    this.keyResultModal.show();

  }

  closeObjectiveButton() {

    this.modalType = '';
    this.modalTitle = '';
    this.editModeIO = 0;
    this.objectiveNameInputBoxValue = '';
    this.objectiveDescriptionInputBoxValue = '';
    this.objectiveModal.hide();

  }

  closeKeyResultButton() {
    this.modalType = '';
    this.modalTitle = '';
    this.editModeIO = 0;
    this.keyResultNameInputBoxValue = '';
    this.keyResultDescriptionInputBoxValue = '';
    this.keyresultProgressValue = 0;
    this.keyResultModal.hide();
  }


  closeKeyResultProgressChangeButton() {
    this.updateProgressModal.hide();


    this.rollBackObjectiveKeyResults();

    this.progressUpdateDescription = '';
    this.modalType = '';
    this.modalTitle = '';
    this.editModeIO = 0;


  }


  closeReviewObjectiveButton(){
    this.reviewInfo = new Reviewclass();
    this.reviewObjectiveModal.hide();
  }

  createNewKeyResult(keyResultNameInput: string, keyResultDescription: string, parentObjective: Objectiveclass) { // now I start use 2-way binding to process this

    if (!keyResultNameInput || !keyResultDescription || !parentObjective) {
      //alert("Do not leave any empty!");

      this._alertService.displayWarningMessage("Key Result Name or Key Result Description empty!");
      return;
    }
    else {

      var newKeyResult = new Keyresultclass();
      newKeyResult.result_name = keyResultNameInput;
      newKeyResult.result_description = keyResultDescription;
      newKeyResult.result_progress_status = 0;
      newKeyResult.objective_id = parentObjective.objective_id;
      this._settingKeyResultService.addNewbyResult(newKeyResult).subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {

          if (this.tempData.status == "success" && this.tempData.data) {
            let tempInfo = <Keyresultclass>this.tempData.data;

            let i = 0;
            for (i = 0; i < this.teamObjectives.length; i++) {
              if (this.teamObjectives[i].objective_id == tempInfo.objective_id) {
                this.teamObjectives[i].keyResult_array.push(tempInfo);

                break;
              }
            }

            this.keyResultDescriptionInputBoxValue = '';
            this.keyResultNameInputBoxValue = '';
            this.editModeIO = 0;
            this.modalTitle = '';
            this.modalType = '';

            var submitANewActivity = new Activityclass();
            submitANewActivity.user_id = this.selfUserInfoData.user_id;
            submitANewActivity.activity_detail = ' Created a new Key Result :' + tempInfo.result_name;
            submitANewActivity.activity_type = 'Create';
            this.submitActivity(submitANewActivity);

            this.calculateObjectivesProgress();
          }

          else {
            this.displayErrorMessage(this.tempData.errorMessage);
          }
        }
      );


    }

    this.keyResultModal.hide();
  }


  updateKeyResult(editObjective, objectiveNameInput: string, objectiveDescription: string) {

    if (!objectiveNameInput || !objectiveDescription) {

      this.displayWarningMessage("Objective Name or Objective Description empty!");

      return;
    } else {

      let originalObjective = editObjective;


      editObjective.objective_description = objectiveDescription;
      editObjective.objective_name = objectiveNameInput;
      var goalIds = this.selectedGoal;
      var goalStatusTag = this.selectedTag[0].id;

      // editObjective.object = timeFrameId;

      editObjective.goal_status = goalStatusTag;

      this._settingObjectiveService.update(editObjective)
        .subscribe(
          data => {
            this.tempData = data
          },
          error => this.errorMessage = <any>error,
          () => {


            if (this.tempData.status == "success" && this.tempData.data) {



              // this.updateTeamMembers(editTeam,this.memberSelectedOptions);
              this.objectiveNameInputBoxValue = '';
              this.objectiveDescriptionInputBoxValue = '';

              var submitANewActivity = new Activityclass();

              var modifyLog = " ";
              if (originalObjective.objective_description != editObjective.objective_description) {
                modifyLog = modifyLog + " Change Objective description  to" + editObjective.objective_description + "; ";
              }
              if (originalObjective.objective_name != editObjective.objective_name) {
                modifyLog = modifyLog + "Change Objective name to" + editObjective.objective_name + "; ";
              }
              if (originalObjective.objective_status != editObjective.objective_status) {
                modifyLog = modifyLog + "Change Objective tag to" + editObjective.objective_status + "; ";
              }

              submitANewActivity.user_id = this.selfUserInfoData.user_id;
              submitANewActivity.activity_detail = "Updated key Result : "
                + editObjective.goal_name + " update log : " + modifyLog;
              submitANewActivity.activity_type = "Update";
              this.submitActivity(submitANewActivity);

              this.calculateObjectivesProgress();

              this.displaySuccessMessage("Your key Result has been updated. <br> affectRows: " + this.tempData.data.affectRows);
            } else {

              //  swal("Error!", this.tempData.errorMassage, "error");
              this.displayErrorMessage(this.tempData.errorMessage);
            }

          }
        );

    }

    this.keyResultModal.hide();

  }


  progressUpdate(keyResult: Keyresultclass, progressUpdateDescription) {
    this._settingKeyResultService.update(keyResult).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any> error,
      () => {
        if (this.tempData.status == 'success' && this.tempData.data) {
          if (this.tempData.data.affectRows && this.tempData.data.affectRows > 0) {
            let submitANewActivity = new Activityclass();
            submitANewActivity.user_id = this.selfUserInfoData.user_id;
            submitANewActivity.activity_detail = ' Updated a new Key Result Progress Status :' + progressUpdateDescription;
            submitANewActivity.activity_type = 'Update';
            this.submitActivity(submitANewActivity);
            this.displaySuccessMessage('Success Update Progress');


            this.calculateObjectivesProgress();
          }
        }
      }
    );


  }


  targetSubscription() {
    this.targetInfoSubscription = this._shareUserOkrinfoService._targetUserInfo$.subscribe(targetInfo => this.targetUserInfoData = targetInfo);
  }


  getCurrentUserInfo() {
    this.selfInfoSubscription = this._userInfoContainerService.userInfo$.subscribe(userInfo => this.selfUserInfoData = userInfo);

  }


  getTargetUserTeamOKRs(targetUserId: any, timeFrameId: any) {


    this._okrUserTeamsObjectivesService.getUserTeamsInfoByTimeFrameId(targetUserId, timeFrameId).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData.data && <Objectiveclass[]>this.tempData.data) {
          this.teamObjectives = new Array<Objectiveclass>();
          this.teamObjectives = this.tempData.data;
          this.updateOverallNumbers();

        }

      }
    );


  }


  getAllTimeFrames() {
    this._settingTimeFrameService.getAllTimeFrames()
      .subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {


          if (this.tempData.status == "success" && this.tempData.data) {
            this.timeFrames = <Timeframeclass[]> this.tempData.data;
            this.setTimeFrameDropDownList(this.timeFrames);
          }
        }
      );

  }


  //TODO: Fix the date format handling issue.
  updateObjective(editObjective, objectiveNameInput: string, objectiveDescription: string) {
    let modifyLog = '';

    if (!objectiveNameInput || !objectiveDescription || !this.selectedGoal) {
      this._alertService.displayWarningMessage("Objective Name or Objective Description empty!");
      return;
    } else {


      let goalIds = this.selectedGoal;
      let objectiveStatusTag = this.selectedTag[0].text;


      if (editObjective.objective_name != objectiveNameInput) {

        modifyLog = modifyLog + "Change Objective name to: " + objectiveNameInput + "; ";
      }
      if (editObjective.objective_description != objectiveDescription) {


        modifyLog = modifyLog + " Change Objective description to: " + objectiveDescription + "; ";
      }

      if (editObjective.objective_status != objectiveStatusTag) {

        modifyLog = modifyLog + "Change Objective tag to: " + objectiveStatusTag + "; ";
      }


      let updateObjective = new Objectiveclass();
      updateObjective = editObjective;
      updateObjective.objective_description = objectiveDescription;
      updateObjective.objective_name = objectiveNameInput;

      // editObjective.object = timeFrameId;
      updateObjective.objective_status = objectiveStatusTag;


      this._settingObjectiveService.update(updateObjective)
        .subscribe(
          data => {
            this.tempData = data
          },
          error => this.errorMessage = <any>error,
          () => {


            if (this.tempData.status == 'success' && this.tempData.data) {



              //this.updateObjectiveTeams();


              this.createActivity(
                'Update',
                modifyLog,
                '', // activity comment
                'o',
                updateObjective.objective_id
              ); //action: string, activityDetails: string, groupType: string, groupId: string


              this._alertService.displaySuccessMessage('Your Objective has been updated.');


              this.objectiveNameInputBoxValue = '';
              this.objectiveDescriptionInputBoxValue = '';

              this.objectiveModal.hide();


            } else {

              this._alertService.displayErrorMessage(this.tempData.errorMessage);

            }
            this.updateOverallNumbers();
            this.editObjective = new Objectiveclass();


          }
        );

    }
  }


  createTeamObjectiveRelationship(objective: Objectiveclass, teamId: any): any {
    this._settingObjectiveService.setTeamObjectives(objective, teamId).subscribe(
      data => {
        this.tempData = data
      },
      error => this.errorMessage = <any>error,
      () => {

        if (this.tempData.status == 'success' && this.tempData.data) {

          objective.team_info.team_id = teamId;
        } else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }

      }
    );
  }


  createGoalObjectiveRelationship(objective: Objectiveclass, golasArray: any): any {
    this._settingObjectiveService.setGoalsObjectives(objective, golasArray).subscribe(
      data => {
        this.tempData = data
      },
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData.status == 'success' && this.tempData.data) {

        } else {

        }
      }
    );
  }


  getAllGoals() {
    this._settingGoalService.getAll()
      .subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {
          if (this.tempData.status == "success" && this.tempData.data) {
            this.goals = <Goalclass[]> this.tempData.data;
            this.setGoalsDropDownList(this.goals);
            //this.goals.sort();
          }
        }
      );
  }


  setGoalsDropDownList(goals: Goalclass[]) {
    var tempArray = [];
    var i = 0;
    for (i = 0; i < goals.length; i++) {
      if (goals[i].goal_status != 'Complete') {
        var info = {id: goals[i].goal_id.toString(), name: goals[i].goal_name};
        tempArray.push(info);
      }

    }

    this.goalsDropDownListOptions = tempArray;


  }




  createNewObjective(objectiveNameInput: string, objectiveDescription: string) { // now I start use 2-way binding to process this


    if (!objectiveNameInput || !objectiveDescription) {
      //alert("Do not leave any empty!");

      this.displayWarningMessage("Objective Name or Objective Description empty!");
      return;
    }
    else {


      var goalIds = this.selectedGoal;

      var objectiveStatusTag = this.selectedTag[0].name;


      var newObjective = new Objectiveclass();

      newObjective.objective_name = objectiveNameInput;
      newObjective.objective_description = objectiveDescription;
      newObjective.objective_progress_status = 0;

      newObjective.objective_status = objectiveStatusTag;


      this._settingObjectiveService.addNewByObjective(newObjective).subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {
          if (this.tempData.status == "success" && this.tempData.data) {
            var tempInfo = <Objectiveclass>this.tempData.data;
            var tempArray = [];
            tempArray.push(tempInfo);
            var i = 0;
            for (i = 0; i < this.goals.length; i++) {
              tempArray.push(this.goals[i]);
            }
            this.goals = tempArray;
            this.updateOverallNumbers();
            this.objectiveNameInputBoxValue = "";
            this.objectiveDescriptionInputBoxValue = "";
            var submitANewActivity = new Activityclass();
            submitANewActivity.user_id = this.selfUserInfoData.user_id;
            submitANewActivity.activity_detail = " Created a new Objective : " + tempInfo.objective_name;
            submitANewActivity.activity_type = "Create";
            this.submitActivity(submitANewActivity);
          } else {

            this.displayErrorMessage(this.tempData.errorMessage);


          }

        }
      );
    }

    this.objectiveModal.show();
  }


//Start a subscription
  getTotalObjectivesNumber() {
    this.overallObjectivesNumberSubscription = this._shareUserOkrinfoService._shareObjectivesNumber$.subscribe(data => this.totalObjectivesNumber = data);
    if (!this.totalObjectivesNumber) {
      this.totalObjectivesNumber = ' - ';
    }
  }

  getOverallProgressNumber() {
    this.overallProgressNumberSubscription = this._shareUserOkrinfoService._shareOverallProgressNumber$.subscribe(data => this.overallProgressNumber = data);
    if (!this.overallProgressNumber) {
      this.overallProgressNumber = ' - ';
    }
  }


  rollBackObjectiveKeyResults() {

    this.updateProgressModal.hide();
    // if(this.editKeyResult){
    // Find out the original value from backup group

    var rollbackKeyResult = this.teamkeyresults.find(keyResult => keyResult.result_id == this.editKeyResult.result_id);
    console.log('roll back key' + rollbackKeyResult.result_progress_status);
    var i = 0;
    var j = 0;


    for (i = 0; i < this.teamObjectives.length; i++) {
      for (j = 0; j < this.teamObjectives[i].keyResult_array.length; j++) {
        if (this.teamObjectives[i].keyResult_array[j].result_id == rollbackKeyResult.result_id) {

          this.teamObjectives[i].keyResult_array[j].result_progress_status = rollbackKeyResult.result_progress_status;

        }
      }
    }


    // }


  }


  keyResultEditable(CurrentObjective: Objectiveclass): boolean {
    let editableIO = false;

    if (this.isAdmin && this.isAdmin == true) {
      editableIO = true;
    } else {
      if (CurrentObjective) {
        let i = 0;

        while (i < CurrentObjective.team_members.length) {
          if (CurrentObjective.team_members[i].user_id == this.selfInfo.user_id) {
            editableIO = true;
            break;
          } else {
            // console.log(CurrentObjective.team_members[i]);
            i++;
          }
        }
      }
    }

    return editableIO;
  }


  setTimeFrameDropDownList(timeframes: Timeframeclass[]) {
    var i = 0;
    var tempArray = [];
    //var NonInfo={id:"0", text:"None"};
    for (i = timeframes.length - 1; i > 0; i--) {
      var timeFrameName = timeframes[i].time_frame_description
        + "   --- (" + timeframes[i].time_frame_start +
        " To " + timeframes[i].time_frame_end + ")";
      // var tempInfo={id:teams[i].team_id, name:teams[i].team_name};
      var tempInfo1 = {id: timeframes[i].time_frame_id, text: timeFrameName};
      tempArray.push(tempInfo1);
    }
    // This way is working...
    this.timeFrameDropDownListOptions = tempArray;
  }

  setGoalDropDownList(goals: Goalclass[]) {
    var i = 0;
    var tempArray = [];

    //var NonInfo={id:"0", text:"None"};
    for (i = goals.length - 1; i > 0; i--) {
      var goalName = goals[i].goal_name;
      var goalId = goals[i].goal_id;

      var tempInfo1 = {id: goalId[i].time_frame_id, text: goalName};
      tempArray.push(tempInfo1);

    }
    // This way is working...
    this.goalsDropDownListOptions = tempArray;

  }

  openPOPup(keyResult) {
    this.activitiesLoading = true;
    this.activitiesInfo = new Array<Activityclass>();
    this._okrActivitiesService.getByKeyResultId(keyResult).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData && this.tempData.status.toLowerCase() == 'success') {
          this.activitiesInfo = <Activityclass[]> this.tempData.data;
        }
        this.activitiesLoading = false;
      }
    );

  }


  calculateObjectivesProgress() {
    var i = 0;
    var j = 0;
    if (this.teamObjectives) {
      for (i = 0; i < this.teamObjectives.length; i++) {

        var averageProgress = 0;
        var currentResultProgress = 0;

        if (this.teamObjectives[i].keyResult_array.length < 1) {
          this.teamObjectives[i].objective_progress_status = 0;
        } else {
          for (j = 0; j < this.teamObjectives[i].keyResult_array.length; j++) {
            currentResultProgress = currentResultProgress + Number(this.teamObjectives[i].keyResult_array[j].result_progress_status);

          }
          averageProgress = currentResultProgress / this.teamObjectives[i].keyResult_array.length;
          this.teamObjectives[i].objective_progress_status = averageProgress;

        }

      }
    }
    this.updateOverAllNumbers();
  }

  updateOverAllNumbers() {
    var overAllProgressNumber = this.calculateOverallProgress();

    // this._shareTeamsOkrinfoService.setOverAllProgressSubject(overAllProgressNumber);
    // this._shareTeamsOkrinfoService.setObjectivesSubjectNumber(this.teamObjectives.length);

  }


//--------- After objective and key result process -------------------------
  calculateOverallProgress(): number {
    var totalNumber = 0;
    var i = 0;
    for (i = 0; i < this.teamObjectives.length; i++) {
      totalNumber = totalNumber + Number(this.teamObjectives[i].objective_progress_status);
    }
    for (i = 0; i < this.teamObjectives.length; i++) {
      totalNumber = totalNumber + Number(this.teamObjectives[i].objective_progress_status);
    }
    var overallProgress = totalNumber / (this.teamObjectives.length + this.teamObjectives.length);
    return overallProgress;
  }


  updateOverallNumbers() {
    const overAllProgressNumber = this.calculateOverallProgress();
    //console.log(overAllProgressNumber);
    this._shareUserOkrinfoService.setOverAllProgressSubject(overAllProgressNumber);
    this._shareUserOkrinfoService.setObjectivesSubjectNumber(this.teamObjectives.length);


  }

  submitActivity(activity: any) {
    this._okrActivitiesService.addNewByClass(activity).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData.data && this.tempData && <Activityclass>this.tempData.data) {
          //swal("Success!", "Your goal has been created.", "success");
          console.log("activity success");
        }
      }
    );
  }


  displayWarningMessage(warningMessage: string) {
    swal("Warning", warningMessage, "warning");
  }

  displayErrorMessage(errorMessage: string) {
    swal("Error!", errorMessage, "error");
  }

  displaySuccessMessage(successMessage: string) {
    swal("Success!", successMessage, "success");
  }


  //goalModal setting and control

  //Modal actions


  @ViewChild('objectiveModal') public objectiveModal: ModalDirective;
  @ViewChild('keyResultModal') public keyResultModal: ModalDirective;
  @ViewChild('updateProgressModal') public updateProgressModal: ModalDirective;
  @ViewChild('activitiesModal') public activitiesModal: ModalDirective;
  @ViewChild('reviewObjectiveModal') public reviewObjectiveModal: ModalDirective;


  createActivity(action: string, activityDetails: string, activityComment: string, groupType: string, groupId: number) {

    const submitANewActivity = new Activityclass();
    submitANewActivity.user_id = this.selfInfo.user_id;
    submitANewActivity.activity_comment = activityComment;
    submitANewActivity.activity_detail = activityDetails; // "Created a new Objective : " + newObjective.objective_name;
    submitANewActivity.activity_type = action;
    submitANewActivity.activity_group = groupType;
    submitANewActivity.activity_group_id = groupId;
    this.submitActivity(submitANewActivity);

  }


// slider

  keyResultProgressOnChange(keyResult: Keyresultclass, event: any) {
    //console.log(event);


    this.editKeyResult = new Keyresultclass();
    this.modalType = 'keyResultProgressUpdate';
    this.modalTitle = 'Progress update confirmation';
    this.editModeIO = 1;

    this.editKeyResult = keyResult;

//    console.log('change progress : ' + JSON.stringify(this.editKeyResult.result_id));


    this.updateProgressModal.show();

  }

  keyResultProgressUpdate(keyResult: Keyresultclass, progressUpdateDescription) {


    const originalKeyResult = this.keyResultsArray.find(result => keyResult.result_id == result.result_id);
    const myLog = 'Update Key Result Progress From ' + originalKeyResult.result_progress_status + '% to ' + keyResult.result_progress_status + '%';

    this._settingKeyResultService.update(keyResult).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any> error,
      () => {
        if (this.tempData.status == 'success' && this.tempData.data) {
          if (this.tempData.data.affectRows && this.tempData.data.affectRows > 0) {

            if (!keyResult.result_progress_status) {
              keyResult.result_progress_status = 0;
            }
            if (!originalKeyResult.result_progress_status) {
              originalKeyResult.result_progress_status = 0;
            }

            this.updateKeyResultArray(keyResult);


            this.createActivity(
              'Update',
              myLog,
              progressUpdateDescription, // activity comment
              'k',
              keyResult.result_id
            ); // action: string, activityDetails: string, groupType: string, groupId: string

            this._alertService.displaySuccessMessage('Success Update Progress');


            this.updateOverallNumbers();
          }
        }
      }
    );


  }


  updateKeyResultArray(editKeyResult: Keyresultclass) {
    let i = 0;
    if (this.keyResultsArray) {

      while (i < this.keyResultsArray.length) {
        if (this.keyResultsArray[i].result_id == editKeyResult.result_id) {


          this.keyResultsArray[i].result_progress_status = Number(editKeyResult.result_progress_status);
          this.keyResultsArray[i].result_description = editKeyResult.result_description;
          this.keyResultsArray[i].result_target = editKeyResult.result_target;
          this.keyResultsArray[i].result_unit = editKeyResult.result_unit;
          this.keyResultsArray[i].result_name = editKeyResult.result_name;


          // console.log( this.keyResultsArray[i]);
          break;
        } else {
          i++;
        }
      }

    }

  }



  public showActivitiesModal(keyResult): void {
    this.activitiesLoading = true;
    this.activitiesInfo = new Array<Activityclass>();
    this._okrActivitiesService.getByKeyResultId(keyResult).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData && this.tempData.status.toLowerCase() == 'success'){
          this.activitiesInfo = <Activityclass[]> this.tempData.data;
        }
        this.activitiesLoading = false;
      }
    );
    this.activitiesModal.show();
  }

  public hideActivitiesModal(): void {
    this.activitiesModal.hide();
  }


  saveReviewObjectiveButton( reviewObjective: Objectiveclass){



    if(reviewObjective ){
      if (reviewObjective.reviewed == 0) {
        if( !this.rate || this.rate == 0){
          this._alertService.displayWarningMessage('Pleaes rate this Objective');
        }else{

          this.reviewInfo = new Reviewclass();
          this.reviewInfo.review_details = this.reviewDescription;
          this.reviewInfo.review_rating = this.rate;
          this.reviewInfo.review_user_id = this.selfInfo.user_id;
          this.reviewInfo.review_object_type = 'o';
          this.reviewInfo.review_object_id = reviewObjective.objective_id;
          this.createNewReview(this.reviewInfo, reviewObjective);

        }



      }else{
        this.reviewInfo.review_rating = this.rate;
        this.reviewInfo.review_details = this.reviewDescription ;
        this.updateReviewInfo(this.reviewInfo, reviewObjective);
      }

    }

    this.reviewObjectiveModal.show();
  }

  createNewReview(newReviewInfo: Reviewclass, reviewObjective: Objectiveclass){
    this._reviewsService.addNewByClass(newReviewInfo).subscribe(
      data => this.tempData = data,
      error => this.errorMessage =<any>error,
      () => {
        if(this.tempData.status == 'success' && this.tempData.data) {
          this.updateObjectiveReviewStatus(reviewObjective);

        }else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }
      }
    );

  }
  updateReviewInfo(ReviewInfo: Reviewclass, reviewObjective: Objectiveclass){
    this._reviewsService.update(ReviewInfo).subscribe(
      data => this.tempData = data,
      error => this.errorMessage =<any>error,
      () => {
        if(this.tempData.status == 'success' && this.tempData.data) {
          this._alertService.displaySuccessMessage('Your Review Infomation has been updated. <br> affectRows: ' + this.tempData.data.affectRows);

          this.createActivity(
            'Updated',
            'Updated reviewed Info for Objective ' + reviewObjective.objective_name,
            '', // activity comment
            'o',
            reviewObjective.objective_id
          ); //action: string, activityDetails: string, groupType: string, groupId: string

        }else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }
        this.reviewObjectiveModal.hide();
      }
    );
  }

  updateObjectiveReviewStatus( objective: Objectiveclass ){
    objective.reviewed = 1;
    objective.objective_status = 'Complete';
    this._settingObjectiveService.update(objective).subscribe(
      data => this.tempData = data,
      error => this.errorMessage =<any>error,
      () => {
        if(this.tempData.status == 'success' && this.tempData.data) {
          this.createActivity(
            'Review',
            'Reviewed Objective ' + objective.objective_name,
            '', // activity comment
            'o',
            objective.objective_id
          ); //action: string, activityDetails: string, groupType: string, groupId: string
          this._alertService.displaySuccessMessage('Success!');

        }else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }
        this.reviewObjectiveModal.hide();
      }
    );

  }




  public hoveringOver(value:number):void {
    this.overStar = value;
    this.percent = 100 * (value / this.max);
  };

  public resetStar():void {
    this.overStar = void 0;
  }



}
