import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import {SharedModulesRegisterModule} from '../../../../shared/shared-modules-register.module';



import { OkrSettingTimeFrameRoutingModule } from './okr-setting-time-frame-routing.module';
import { OkrSettingTimeFrameComponent } from './okr-setting-time-frame.component';





@NgModule({
  imports: [
    SharedModulesRegisterModule,




    FormsModule,
    HttpModule,


    CommonModule,
    OkrSettingTimeFrameRoutingModule
  ],
  declarations: [OkrSettingTimeFrameComponent]
})
export class OkrSettingTimeFrameModule { }
