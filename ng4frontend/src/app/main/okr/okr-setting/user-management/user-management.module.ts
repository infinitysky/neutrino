import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { SelectModule} from 'ng2-select';

import { UserManagementRoutingModule } from './user-management-routing.module';
import { UserManagementComponent } from './user-management.component';
import { ControlMessagesComponent } from './control-messages/control-messages.component';


@NgModule({
  imports: [
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    NgxMyDatePickerModule,
    SelectModule,

    CommonModule,
    UserManagementRoutingModule,
  ],
  declarations: [UserManagementComponent, ControlMessagesComponent]
})
export class UserManagementModule { }
