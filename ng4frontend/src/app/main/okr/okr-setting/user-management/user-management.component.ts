import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';


import { Subscription } from 'rxjs/Subscription';

import {Md5} from 'ts-md5/dist/md5';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';

import {ValidationService} from '../../../../shared/services/validation.service';

import { AlertService } from '../../../../shared/services/alert.service';
import { UserDetailsService } from '../../../../shared/services/user-details.service';
import { UsersInfoService } from '../../../../shared/services/users-info.service';

// Do not put this into providers --- it already set at app.module
import { UserInfoContainerService } from '../../../../shared/services/user-info-container.service';


import { Userclass } from '../../../../shared/classes/user-class';





@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  providers: [UsersInfoService, UserDetailsService, AlertService],
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {


  public isAdmin: boolean; // Check the user's role and control the edit button
  public editModeIO: boolean; // display the edit button group
  public users: Userclass[]; // ALL users information, it will pass to table

  public tempData: any; // data holder
  public errorMessage: any;

  public selfInfoFromCookie: Userclass; // get current login users information, which is stored in cookie
  public editUser: Userclass; // temp edit user

  public selfInfo: Userclass;
  public registerInfo: Userclass;

  private selfInfoSubscription: Subscription;
  public loading: boolean;

  public userInfo: FormGroup;
  public registerForm: FormGroup;
  public passwordForm: FormGroup;


  public permissionDropDownListOptions = [{ id: '1', text: 'admin' }, { id: '2', text: 'manager' }, { id: '3', text: 'staff' }];
  public selectedPermissionLevel = [];


  @ViewChild('userInfoModal') public userInfoModal: ModalDirective;

  @ViewChild('newUserModal') public newUserModal: ModalDirective;
  @ViewChild('passwordModal') public passwordModal: ModalDirective;



  constructor(
    private _usersInfoService: UsersInfoService,
    private _alertService: AlertService,
    private _formBuilder: FormBuilder,
    private _userInfoContainerService: UserInfoContainerService,
    private _userDetailsService: UserDetailsService,



  ) {
    // -------------------- init data ------------------------------
    this.tempData = '';
    this.users = [];
    this.isAdmin = false;
    this.editModeIO = true;

    this.editUser = new Userclass();

    this.selectedPermissionLevel = [];
  }

  ngOnInit() {
    this.selfInfoSubscribe();
    this.setupForm();
    this.loadAllUsersData();
  }

  ngOnDestroy() {
    this.selfInfoSubscription.unsubscribe();
  }


  selfInfoSubscribe() {

    this.selfInfoSubscription = this._userInfoContainerService.userInfo$.subscribe(
      data => this.selfInfo = data
    );
    if(this.selfInfo) {


      if (this.selfInfo.role == 'admin') {
        this.isAdmin = true;
      } else {

        this.isAdmin = false;
      }
    }

  }

// ---------- buttons -----------
  addUserButton(){

    this.openNewUserModal();

  }

  editButton(){
    this.editModeIO = !this.editModeIO;

  }

  editUserButton(userInfo: Userclass){
    this.editUser = new Userclass();
    this.editUser = userInfo;
    this.openUserInfoModal(userInfo);
    // console.log(JSON.stringify(userInfo));
  }


  saveUserInfoButton(updateUserInfo: Userclass){

    // console.log(updateUserInfo);
    const userRoleId = this.selectedPermissionLevel[0].id;
    const userRole = this.selectedPermissionLevel[0].text;
    updateUserInfo.role_id = userRoleId;
    updateUserInfo.role = userRole;
    //console.log(updateUserInfo);
    this._userDetailsService.update(updateUserInfo).subscribe(
      data => this.tempData = data,
      error => {

        this.errorMessage = <any>error;
        this._alertService.displayErrorMessage(this.errorMessage);
        this.loading = false;
      },
      () => {

        if (this.tempData.status.toLowerCase() == 'success' && this.tempData.data ){
          if( Number(this.tempData.data.affectRows) >= 1){
            this._alertService.displaySuccessMessage( 'Update Success' );
          }

        }

      }
    );
    // console.log(updateUserInfo);

  }

  openPasswordModal(updateUserInfo){
    this.editUser = updateUserInfo;
    this.passwordModal.show();

  }


  closePasswordModal () {

    this.setupForm();
    this.loading = false;
    this.passwordModal.hide();
  }

  submitNewPassword () {
    // console.log('save');
    this.loading = true;
    const passwordInfo = this.passwordForm.value;


    if (passwordInfo.newPassword != passwordInfo.repeatPassword ) {
      this._alertService.displayErrorMessage('New Password And Repeat New Password Not Match');
      this.loading = false;
    }else {

     // const currentPassword =  Md5.hashStr(passwordInfo.currentPassword).toString();
      const newPassword =  Md5.hashStr(passwordInfo.newPassword).toString();


      this._usersInfoService.adminUpdatePassword(this.editUser.user_id, this.selfInfo.user_id, newPassword).subscribe(
        data => this.tempData = data,
        error => {
          this.errorMessage = <any>error;
          this._alertService.displayErrorMessage(this.errorMessage);
          this.loading = false;
        },
        () => {

          if (this.tempData.status.toLowerCase() == 'success' && this.tempData.data ){

            this._alertService.displaySuccessMessage( this.tempData.data.Message );
            this.loading = false;
            this.passwordModal.hide();
          }else{
            this._alertService.displayErrorMessage( this.tempData.errorMessage );
            this.loading = false;
          }

        }
      );
      // console.log(updateUserInfo);

    }

  }







  // ---------------Logic--------------
  loadAllUsersData(){
    this.users = new Array <Userclass>();
    this._usersInfoService.getAll()
      .subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any> error,
        () =>{
          if (this.tempData && this.tempData.status.toLowerCase() === 'success'){
            this.users =  <Userclass[]> this.tempData.data;
          }
        }
      );
  }

  updateInfo(userInfo: Userclass){
    this._usersInfoService.update(userInfo)
      .subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {
          if (this.tempData && this.tempData.status === 'success'){
            this._alertService.displaySuccessMessage(this.tempData.data);

          }
        }
      );

  }





  closeUserInfoModal(){
    this.setupForm();
    this.newUserModal.hide();
  }

  openNewUserModal(){

    this.newUserModal.show();
  }



  closeNewUserModal(){
    this.userInfoModal.hide();
  }


  setSelectedPermissionLevel(userInfo: Userclass){
    this.selectedPermissionLevel = [{ id: userInfo.role_id, text: userInfo.role }];

  }

  openUserInfoModal(userInfo: Userclass){
    this.setSelectedPermissionLevel(userInfo);
    this.userInfoModal.show();
  }



// ------------------ Modal --------------------------------------

  setupForm() {
    this.registerForm = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, ValidationService.emailValidator]],
      password: ['', [Validators.required, ValidationService.passwordValidator]],
    });

    this.passwordForm = this._formBuilder.group({
     // currentPassword: ['',[Validators.required]],
      newPassword: ['', [Validators.required, ValidationService.passwordValidator]],
      repeatPassword: ['', [Validators.required, ValidationService.passwordValidator]],
    });
  }

  register() {
    this.loading = true;
    this.registerInfo = new Userclass();
    this.registerInfo.first_name = this.registerForm.value.firstName;
    this.registerInfo.last_name = this.registerForm.value.lastName;
    this.registerInfo.email = this.registerForm.value.email;
    this.registerInfo.password = Md5.hashStr(this.registerForm.value.password).toString();
    // console.log(JSON.stringify(this.registerInfo));
    this._usersInfoService.addNewUserByClass(this.registerInfo).subscribe(
      data => this.tempData = data,
      error => {
        // this.alertService.error(error);
        this.errorMessage = <any>error;
        this._alertService.displayErrorMessage(this.errorMessage);
        this.loading = false;
      },
      () => {
        this.loading = false;
        if (this.tempData.data && this.tempData.status == 'Success') {
          this._alertService.displaySuccessMessage('Add New User Success!');
          this.loadAllUsersData();
          this.closeUserInfoModal();
        } else {
          this._alertService.displayErrorMessage(this.tempData.errorMessage);
        }
      }
    );
  }








}
