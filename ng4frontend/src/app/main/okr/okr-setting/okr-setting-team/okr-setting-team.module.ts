import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';

import {SharedModulesRegisterModule} from '../../../../shared/shared-modules-register.module';

import { OkrSettingTeamRoutingModule } from './okr-setting-team-routing.module';
import { OkrSettingTeamComponent } from './okr-setting-team.component';




@NgModule({
  imports: [
    SharedModulesRegisterModule,

    Ng2Bs3ModalModule,




    FormsModule,
    HttpModule,
    CommonModule,



    OkrSettingTeamRoutingModule,

  ],
  declarations: [OkrSettingTeamComponent]
})
export class OkrSettingTeamModule { }
