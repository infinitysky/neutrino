import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {SharedModulesRegisterModule} from '../../../../shared/shared-modules-register.module';

import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';

import { OkrSettingKeyResultRoutingModule } from './okr-setting-key-result-routing.module';
import { OkrSettingKeyResultComponent } from './okr-setting-key-result.component';

@NgModule({
  imports: [

    SharedModulesRegisterModule,
    Ng2Bs3ModalModule,




    FormsModule,
    HttpModule,
    CommonModule,
    OkrSettingKeyResultRoutingModule
  ],
  declarations: [OkrSettingKeyResultComponent]
})
export class OkrSettingKeyResultModule { }
