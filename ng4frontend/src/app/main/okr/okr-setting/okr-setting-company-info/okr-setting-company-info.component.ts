import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';


import { Userclass } from '../../../../shared/classes/user-class';
import { CompanyDetailClass } from '../../../../shared/classes/company-detail-class';
import { Systemsettingclass } from '../../../../shared/classes/system-setting-class';

import { AlertService } from '../../../../shared/services/alert.service';
import { SystemSettingService } from '../../../../shared/services/system-setting.service';
import { UserInfoContainerService } from '../../../../shared/services/user-info-container.service';
import { OkrCompanyService } from  '../../okr-shared/services/okr-company.service';
import { MyCookieService } from '../../../../shared/services/my-cookie.service';

import { NavigationComponent } from '../../../navigation/navigation.component';

@Component({
  selector: 'app-okr-setting-company-info',
  templateUrl: './okr-setting-company-info.component.html',
  providers: [MyCookieService, OkrCompanyService, SystemSettingService],

  styleUrls: ['./okr-setting-company-info.component.css']
})
export class OkrSettingCompanyInfoComponent implements OnInit {

  public isAdmin: boolean;
  public disabled: boolean;
  private selfInfo: Userclass;
  private selfInfoSubscription : Subscription;

  public systemSettingInfo: Systemsettingclass[];

  public companyInfo: CompanyDetailClass;
  private tempData: any;
  public errorMessage: any;

  @ViewChild(NavigationComponent) public navigationComponent: NavigationComponent;

  constructor(
    private _alertService: AlertService,
    private _systemSettingService: SystemSettingService,
    private _userInfoContainerService: UserInfoContainerService,
    private _okrCompanyService: OkrCompanyService,
    private _cookieService: MyCookieService
  ) {
    this.isAdmin = false;
    this.selfInfo = new Userclass;

    this.systemSettingInfo = new Array<Systemsettingclass>();
    this.companyInfo = new CompanyDetailClass();

  }

  ngOnInit() {
    this.getCompanyInfo();
    this.setEditable();

  }
  ngOnDestroy() {
    this.selfInfoSubscription.unsubscribe();
  }

  setEditable(){
    this.selfInfoSubscription = this._userInfoContainerService.userInfo$.subscribe(selfInformation=> this.selfInfo = selfInformation);

    if ( this.selfInfo){
      if ( this.selfInfo.role == 'admin' ||  this.selfInfo.role == 'manager'){
        this.isAdmin = true;

        //console.log('set good ');
       // console.log( this.selfInfo.role);
      }else {
        this.isAdmin = false;

      }
    }
  }

  getCompanyInfo() {
    this.systemSettingInfo = new Array<Systemsettingclass>();
    this.companyInfo = new CompanyDetailClass();

    this._systemSettingService.getCompanyInfo().subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any> error,

      () =>{
        if (this.tempData.status.toLowerCase() == 'success' && this.tempData.data) {
          this.systemSettingInfo = <Systemsettingclass[]>this.tempData.data;
          // this.goals.sort();

          let i = 0;
          for (i =0; i < this.systemSettingInfo.length; i++ ) {
            switch (this.systemSettingInfo[i].setting_name){
              case 'company_name':
                this.companyInfo.company_name = this.systemSettingInfo[i].setting_value;
                break;

              case 'company_address':
                this.companyInfo.company_address = this.systemSettingInfo[i].setting_value;
                break;

              case 'company_email':
                this.companyInfo.company_email = this.systemSettingInfo[i].setting_value;
                break;

              case 'company_mission':
                this.companyInfo.company_mission = this.systemSettingInfo[i].setting_value;
                break;

              case 'company_phone':
                this.companyInfo.company_phone = this.systemSettingInfo[i].setting_value;
                break;

              case 'company_vision':
                this.companyInfo.company_vision = this.systemSettingInfo[i].setting_value;
                break;

              default:
                break;

            }

          }

        }else{
          this.systemSettingInfo = new Array<Systemsettingclass>();
          this.companyInfo = new CompanyDetailClass();




        }


      }
    );

  }

  cancelButton(){

    let i = 0;
    for (i =0; i < this.systemSettingInfo.length; i++ ) {
      switch (this.systemSettingInfo[i].setting_name) {
        case 'company_name':
          this.companyInfo.company_name = this.systemSettingInfo[i].setting_value;
          break;

        case 'company_address':
          this.companyInfo.company_address = this.systemSettingInfo[i].setting_value;
          break;

        case 'company_email':
          this.companyInfo.company_email = this.systemSettingInfo[i].setting_value;
          break;

        case 'company_mission':
          this.companyInfo.company_mission = this.systemSettingInfo[i].setting_value;
          break;

        case 'company_phone':
          this.companyInfo.company_phone = this.systemSettingInfo[i].setting_value;
          break;

        case 'company_vision':
          this.companyInfo.company_vision = this.systemSettingInfo[i].setting_value;
          break;

        default:
          break;

      }
    }

  }

  saveChangeButton(){

    const updateCompanyInfo = new Array<any>();
    let updateInfo = new Systemsettingclass();

    updateInfo.setting_name = 'company_name';
    updateInfo.setting_value = this.companyInfo.company_name;
    updateCompanyInfo.push(updateInfo);

    updateInfo = new Systemsettingclass();
    updateInfo.setting_name = 'company_vision';
    updateInfo.setting_value = this.companyInfo.company_vision;
    updateCompanyInfo.push(updateInfo);

    updateInfo = new Systemsettingclass();
    updateInfo.setting_name = 'company_phone';
    updateInfo.setting_value = this.companyInfo.company_phone;
    updateCompanyInfo.push(updateInfo);


    updateInfo = new Systemsettingclass();
    updateInfo.setting_name = 'company_mission';
    updateInfo.setting_value = this.companyInfo.company_mission;
    updateCompanyInfo.push(updateInfo);

    updateInfo = new Systemsettingclass();
    updateInfo.setting_name = 'company_email';
    updateInfo.setting_value = this.companyInfo.company_email;
    updateCompanyInfo.push(updateInfo);

    updateInfo = new Systemsettingclass();
    updateInfo.setting_name = 'company_address';
    updateInfo.setting_value = this.companyInfo.company_address;
    updateCompanyInfo.push(updateInfo);


    this._systemSettingService.updateCompanyInfo(updateCompanyInfo).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any> error,
      () =>{
        if (this.tempData.status.toLowerCase() == 'success' && this.tempData.data) {
          this._alertService.displaySuccessMessage('affectRows: ' + this.tempData.data.affectRows + '<br/> Please Restart This Web Application to Apply the Changes');
          this.getCompanyInfo();

        }
      }
    );

  }


}
