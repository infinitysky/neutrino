import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { OkrSettingCompanyInfoRoutingModule } from './okr-setting-company-info-routing.module';
import { OkrSettingCompanyInfoComponent } from './okr-setting-company-info.component';

import {SharedModulesRegisterModule} from '../../../../shared/shared-modules-register.module';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    HttpModule,
    OkrSettingCompanyInfoRoutingModule,
    SharedModulesRegisterModule
  ],
  declarations: [OkrSettingCompanyInfoComponent]
})
export class OkrSettingCompanyInfoModule { }
