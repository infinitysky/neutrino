import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {SettingSelfInfoComponent} from './setting-self-info.component';
const routes: Routes = [
  { path: '', component: SettingSelfInfoComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingSelfInfoRoutingModule { }
