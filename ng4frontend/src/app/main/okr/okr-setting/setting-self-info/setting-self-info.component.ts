import { Component, OnInit, ViewChild } from '@angular/core';

import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';




import { Subscription } from 'rxjs/Subscription';

import {Md5} from 'ts-md5/dist/md5';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';

import {ValidationService} from '../../../../shared/services/validation.service';

import { AlertService } from '../../../../shared/services/alert.service';
import { UserDetailsService } from '../../../../shared/services/user-details.service';
import { UsersInfoService } from '../../../../shared/services/users-info.service';

// Do not put this into providers --- it already set at app.module
import { UserInfoContainerService } from '../../../../shared/services/user-info-container.service';


import { Userclass } from '../../../../shared/classes/user-class';


@Component({
  selector: 'app-setting-self-info',
  templateUrl: './setting-self-info.component.html',
  providers: [UserDetailsService, UsersInfoService],
  styleUrls: ['./setting-self-info.component.css']
})
export class SettingSelfInfoComponent implements OnInit {

  public userInfo: Userclass;
  public userInfoBackup: Userclass;
  private editUser: Userclass;

  private selfInfoSubscription: Subscription;
  private selfInfo: any;
  private tempData: any;
  public errorMessage: any;
  public loading: boolean;


  public passwordForm: FormGroup;



  @ViewChild('passwordModal') public passwordModal: ModalDirective;

  constructor(
    private _alertService: AlertService,
    private _userInfoContainerService: UserInfoContainerService,
    private _userDetailsService: UserDetailsService,
    private _usersInfoService: UsersInfoService,
    private _formBuilder: FormBuilder

  ) {
    this.loading = false;
    this.userInfo = new Userclass();
    this.userInfoBackup = new Userclass();

    this.editUser = new Userclass();






  }

  ngOnInit() {
    this.loadInfo();
  }
  loadInfo(){
    this.setupForm();
    this.selfInfoSubscribe();
  }


  ngOnDestroy() {
    this.selfInfoSubscription.unsubscribe();
  }


  setupForm() {

    this.passwordForm = this._formBuilder.group({
      currentPassword: ['',[Validators.required]],
      newPassword: ['', [Validators.required, ValidationService.passwordValidator]],
      repeatPassword: ['', [Validators.required, ValidationService.passwordValidator]],
    });

  }



  cancelButton(){

    //
    // this.userInfo = new Userclass();
    //
    // this.userInfo.user_id = this.userInfoBackup.user_id;
    // this.userInfo.role = this.userInfoBackup.role;
    // this.userInfo.role_id = this.userInfoBackup.role_id;
    // this.userInfo.dob = this.userInfoBackup.dob;
    // this.userInfo.email = this.userInfoBackup.email;
    // this.userInfo.first_name = this.userInfoBackup.first_name;
    // this.userInfo.last_name = this.userInfoBackup.last_name;
    // this.userInfo.position = this.userInfoBackup.position;
    this.loadInfo();
  }


  saveUserInfoButton(updateUserInfo: Userclass){
    //console.log(updateUserInfo);

    this._userDetailsService.update(updateUserInfo).subscribe(
      data => this.tempData = data,
      error => {

        this.errorMessage = <any>error;
        this._alertService.displayErrorMessage(this.errorMessage);
        this.loading = false;
      },
      () => {

        if (this.tempData.status.toLowerCase() == 'success' && this.tempData.data ){
          if( Number(this.tempData.data.affectRows) >= 1){
            this._alertService.displaySuccessMessage( 'Update Success' );
          }

        }

      }
    );
    // console.log(updateUserInfo);

  }


  selfInfoSubscribe() {

    this.selfInfoSubscription = this._userInfoContainerService.userInfo$.subscribe(
      data => this.selfInfo = data
    );
    this.userInfo = new Userclass();
    this._userDetailsService.getById(this.selfInfo.user_id).subscribe(
      data => this.tempData = data,
      error =>  this.errorMessage = <any>error,
      () => {

        if(this.tempData && this.tempData.data ){

          this.userInfo = <Userclass>this.tempData.data;
          this.userInfoBackup = <Userclass>this.tempData.data;

        }

      }


    );
  }



  openPasswordModal(updateUserInfo){
    this.editUser = updateUserInfo;
    this.setupForm();
    this.passwordModal.show();

  }
  closePasswordModal () {
    this.editUser  = new Userclass();
    this.setupForm();
    this.loading = false;
    this.passwordModal.hide();
  }

  submitNewPassword () {
    //console.log('save');
    this.loading = true;
    const passwordInfo = this.passwordForm.value;


    if (passwordInfo.newPassword != passwordInfo.repeatPassword ) {
      this._alertService.displayErrorMessage('New Password And Repeat New Password Not Match');
      this.loading = false;
    }else {

      const currentPassword =  Md5.hashStr(passwordInfo.currentPassword).toString();
      const newPassword =  Md5.hashStr(passwordInfo.newPassword).toString();


      this._usersInfoService.updatePassword(this.editUser.user_id, currentPassword, newPassword).subscribe(
        data => this.tempData = data,
        error => {
          this.errorMessage = <any>error;
          this._alertService.displayErrorMessage(this.errorMessage);
          this.loading = false;
        },
        () => {

          if (this.tempData.status.toLowerCase() == 'success' && this.tempData.data ){

              this._alertService.displaySuccessMessage( this.tempData.data.Message );
              this.loading = false;
              this.passwordModal.hide();
            }else{
            this._alertService.displayErrorMessage( this.tempData.errorMessage );
            this.loading = false;
          }

        }
      );
      // console.log(updateUserInfo);

    }

  }





}
