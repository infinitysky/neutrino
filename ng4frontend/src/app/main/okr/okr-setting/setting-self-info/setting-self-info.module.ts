import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';




import { SettingSelfInfoRoutingModule } from './setting-self-info-routing.module';
import { SettingSelfInfoComponent } from './setting-self-info.component';

import {SharedModulesRegisterModule} from '../../../../shared/shared-modules-register.module';

import {ValidationService} from '../../../../shared/services/validation.service';


@NgModule({
  imports: [

    CommonModule,



    SettingSelfInfoRoutingModule,

    SharedModulesRegisterModule

  ],
  declarations: [SettingSelfInfoComponent]

})
export class SettingSelfInfoModule { }
