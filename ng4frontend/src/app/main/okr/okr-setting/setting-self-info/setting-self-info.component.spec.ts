import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingSelfInfoComponent } from './setting-self-info.component';

describe('SettingSelfInfoComponent', () => {
  let component: SettingSelfInfoComponent;
  let fixture: ComponentFixture<SettingSelfInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingSelfInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingSelfInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
