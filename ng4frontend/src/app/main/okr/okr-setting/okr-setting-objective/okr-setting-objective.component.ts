import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';


import {  SettingObjectiveService } from '../../okr-shared/services/okr-objective.service';
import {  SettingGoalService } from '../../okr-shared/services/okr-goal.service';
import { UserInfoContainerService } from '../../../../shared/services/user-info-container.service';
import {OkrActivitiesService} from '../../okr-shared/services/okr-activities.service';
import {SettingTeamService} from '../../okr-shared/services/okr-team.service';
import { AlertService } from '../../../../shared/services/alert.service';


import {Activityclass} from '../../okr-shared/classes/activitie-class';
import {Userclass} from '../../../../shared/classes/user-class';
import {Objectiveclass} from '../../okr-shared/classes/objective-class';
import {Goalclass} from '../../okr-shared/classes/goal-class';
import {Teamclass} from '../../okr-shared/classes/team-class';


@Component({
  selector: 'app-okr-setting-objective',
  templateUrl: './okr-setting-objective.component.html',


  providers: [
    SettingGoalService,
    SettingTeamService,
    SettingObjectiveService,
    OkrActivitiesService,
    AlertService


  ],
  styleUrls: ['./okr-setting-objective.component.css']
})
export class OkrSettingObjectiveComponent implements OnInit {

  public isHide: boolean;

 // public goalInfo: Goalclass;


  private userInfoSubscription: Subscription;
  private selfInfo: Userclass;
  public isAdmin: boolean;
  private tempData: any;
  public errorMessage: any;

  public isLoading: boolean;

  public activitiesInfo: any;
  public activitiesLoading: boolean;


  public modalTitle: string;


  public goals: Goalclass[];


  public teams: Teamclass[];

  public objectives: Objectiveclass[];




  //--------------goalModal parameter------------------

  //goalModal action control
  animation: boolean = true;
  keyboard: boolean = true;
  backdrop: string | boolean = "static";
  public modalType:string=''; //objective | keyresult


  //current model
  public editModeIO: number;


  //edit mode parameter
  public editObjective: any;
  public parentObjective: any;
  public editKeyResult: any;

  public editReview: any;


  public objectiveNameInputBoxValue: string;
  public objectiveDescriptionInputBoxValue: string;

  //Dropdownlist;


  public goalsDropDownListOptions: any;
  public selectedGoal: Array<any>;

  public newSubmitActivity: Activityclass;

// TODO: DO we have to set the tag on key-result?
  public tagDropDownListOptions: any;
  public selectedTag: any;

  public teamDropDownListOptions: any;
  public selectedTeam: any;




// Multi Selection for Goals
  public goalsSelectorSettings: IMultiSelectSettings = {
    pullRight: false,
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default',
    selectionLimit: 0,
    closeOnSelect: false,
    showCheckAll: true,
    showUncheckAll: true,
    dynamicTitleMaxItems: 0,
    maxHeight: '300px',
  };

  public goalsSelectorTexts: IMultiSelectTexts = {
    checkAll: 'Check all',
    uncheckAll: 'Uncheck all',
    checked: 'checked',
    checkedPlural: 'checked',
    searchPlaceholder: 'Search...',
    defaultTitle: 'Select Goals',
  };




  @ViewChild('objectiveModal') public objectiveModal: ModalDirective;



  constructor(

    private _alertService: AlertService,
    private _userInfoContainerService: UserInfoContainerService,
    private _settingGoalService: SettingGoalService,
    /*
     * --------------Modal----------------------------------------
     *
     * */
    private _settingTeamService: SettingTeamService,
    private _settingObjectiveService: SettingObjectiveService,
    private _okrActivitiesService: OkrActivitiesService,

  ){
    this.selfInfo = new Userclass();
    //this.goalInfo =  new Goalclass();

    this.isAdmin = false;
    this.isLoading = true;
    this.isHide = true;


    this.activitiesInfo =  new Array<Activityclass>();


    /*
     * pop up
     * */
    this.activitiesLoading = true;




    /*
     * --------------------------Modal-----------------------------------------
     *
     * */
    this.modalTitle = '';

    this.goals = [];


    this.teams = [];
    this.objectives = [];


    this.editModeIO = 0;
    this.editObjective = new Objectiveclass();
    this.parentObjective = new Objectiveclass();

    this.objectiveNameInputBoxValue = '';
    this.objectiveDescriptionInputBoxValue = '';

    //Drop Down List

    this.goalsDropDownListOptions = [];
    this.selectedGoal = []; // multi selector is using different plugin
    this.newSubmitActivity = new Activityclass();

    this.tagDropDownListOptions = [{ id: 'None', text: 'None' }, { id: 'Warning', text: 'Warning' }, { id: 'Risk', text: 'Risk' }, { id: 'Complete', text: 'Complete' }];
    this.selectedTag = [{ id: 'None', text: 'None' }];

    this.teamDropDownListOptions = [];
    this.selectedTeam = [];

    this.getAllGoals();
    this.getTeamsInfo();
    this.setGoalsDropDownList(this.goals);
    this.setTeamDropDownOptions(this.teams);

  }

  //component functions
  ngOnInit() {

    //this.goalInfo =  new Goalclass();
    this.isAdmin = false;
    this.getSelfInfo();
    this.getObjectives();

  }
  ngOnDestroy() {
    this.userInfoSubscription.unsubscribe();
  }

  getSelfInfo() {
    this.selfInfo = new Userclass();
    this.userInfoSubscription = this._userInfoContainerService.userInfo$.subscribe( userData => this.selfInfo = userData );
    if( this.selfInfo){
      if( this.selfInfo.role == 'admin'){
        this.isAdmin = true;

      }
    }else{
      this.selfInfo = new Userclass();
    }

  }



  getAllGoals() {
    this._settingGoalService.getAll()
      .subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {
          if (this.tempData.status == 'success' && this.tempData.data) {
            this.goals = <Goalclass[]> this.tempData.data;

            this.setGoalsDropDownList(this.goals);
            //this.goals.sort();
          }
        }
      );
  }


  getObjectives() {
    this._settingObjectiveService.getAll()
      .subscribe(
        data => this.tempData = data,
        error =>  this.errorMessage = <any>error,
        () => {
          if (this.tempData) {
            if (this.tempData.status.toLowerCase() == 'success' && this.tempData.data) {
              this.objectives = new Array<Objectiveclass>();
              this.objectives = <Objectiveclass[]> this.tempData.data;
            }
          }
        }
      );

  }

  getTeamsInfo () {
    this.teams = new Array <Teamclass>();
    this._settingTeamService.getAll().subscribe(
      data => this.tempData = data,
      error =>  this.errorMessage =  <any>error,
      () =>{
        if (this.tempData.status.toLowerCase() == 'success' && this.tempData.data) {
          this.teams = <Teamclass[]> this.tempData.data;
        }
        this.setTeamDropDownOptions(this.teams);
      }
    );
  }

  setGoalsDropDownList(goals: Goalclass[]) {
    let tempArray = [];
    let i = 0;
    for (i = 0; i < goals.length;i++){

      if(goals[i].goal_status != 'Complete' ){
        const info = {id: goals[i].goal_id.toString(), name: goals[i].goal_name};
        tempArray.push(info);
      }
    }
    this.goalsDropDownListOptions = tempArray;
  }
  setTeamDropDownOptions (teams: Teamclass[]) {
    const tempArray = [];
    let i = 0;
    if (teams) {
      for (i = 0; i < teams.length; i ++){
        const info = {id: teams[i].team_id.toString(), text: teams[i].team_name};
        //const info = {value: teams[i].team_id.toString(), label: teams[i].team_name};
        tempArray.push(info);
      }
      this.teamDropDownListOptions = tempArray;

    }

  }


  addObjectiveButton() {
    this.getAllGoals();
    // this.setCurrentGoal(this.goalInfo);

    this.getTeamsInfo();

    this.modalType = 'objective';
    this.modalTitle = 'Create A Objective';
    this.editModeIO = 0;

    this.selectedTag = [{ id: 'None', text: 'None'}];
    this.selectedTeam = [];
    this.objectiveNameInputBoxValue = '';
    this.objectiveDescriptionInputBoxValue = '';

    this.objectiveModal.show();

  }

  closeObjectiveButton() {

    this.modalType = '';
    this.modalTitle = '';
    this.editModeIO = 0;
    this.selectedTag = [{ id: 'None', text: 'None'}];
    this.selectedTeam = [];
    this.objectiveNameInputBoxValue = '';
    this.objectiveDescriptionInputBoxValue = '';
    this.objectiveModal.hide();

  }

  editButton(){
    this.isHide =! this.isHide;
  }
  refreshButton(){

    this.getObjectives();
  }


  modalSaveObjectiveChangeButton() {
    // read the 2 way binding;
    let objectiveNameInput = this.objectiveNameInputBoxValue;
    let objectiveDescription = this.objectiveDescriptionInputBoxValue;

    if (!objectiveNameInput || !objectiveDescription || !this.selectedGoal) {
      //alert("Do not leave any empty!");

      this._alertService.displayWarningMessage("Objective Name, Objective Description or Goal selector empty!");

    } else if  ( this.selectedTeam.length == 0  ){
      this._alertService.displayWarningMessage("Team selector empty!");
    }
    else{
      if (0 == this.editModeIO) {
        this.createNewObjective(objectiveNameInput, objectiveDescription, this.selectedTeam);
      } else {
        this.updateObjective(this.editObjective, objectiveNameInput, objectiveDescription, this.selectedTeam);
      }
    }


  }











  deleteObjectiveButton(deletedObjective) {
    this._settingObjectiveService
      .delete(deletedObjective)
      .subscribe(
        data => { this.tempData = data},
        error => { this.errorMessage = <any>error},
        () => {

          if (this.tempData.status=='success' && this.tempData.data.affectRows) {
            if( this.tempData.data.affectRows>0){
              this.createActivity(
                'Delete',
                'Deleted Objective : ' + deletedObjective.objective_name + ' ( Objective ID: '+ deletedObjective.objective_id + ')',
                '', // activityComment
                'o',
                deletedObjective.objective_id
              ); //action: string, activityDetails: string, groupType: string, groupId: string

              this.objectives = this.objectives.filter(currentObjectives => currentObjectives !== deletedObjective);





              this._alertService.displaySuccessMessage( 'Your Objective has been deleted.');
            }else {
              this._alertService.displayErrorMessage('Your Objective did not deleted successfully.');
            }

          } else {
            this._alertService.displayErrorMessage('Your Objective did not deleted successfully.');
          }
        }
      );
  }



  editObjectiveButton(objective: Objectiveclass) {
    this.getAllGoals();
    this.modalType = 'objective';
    this.modalTitle = 'Update A Objective';
    this.editModeIO = 1;
    this.editObjective = new Objectiveclass();
    this.editObjective = objective;
    this.objectiveNameInputBoxValue = objective.objective_name;
    this.objectiveDescriptionInputBoxValue = objective.objective_description;

    this.selectedTag = [{ id: objective.objective_status, text: objective.objective_status }];

    this.setCurrentTeamObjectivesRelationship(objective);
    this.objectiveModal.show();

  }

  createNewObjective(objectiveNameInput: string, objectiveDescription: string, selectedTeam:any) { // now I start use 2-way binding to process this

    // if(!this.viewTeamID){
    //   this.errorMessage("Internal error! Please refresh your page!");
    // }


    if (!objectiveNameInput || !objectiveDescription || !this.selectedGoal) {
      //alert("Do not leave any empty!");

      this._alertService.displayWarningMessage("Objective Name, Objective Description or Goal selector empty!");
      return;
    }
    else {
      const goalIds = this.selectedGoal;
      const objectiveStatusTag = this.selectedTag[0].text;
      const newObjective = new Objectiveclass();
      newObjective.objective_name = objectiveNameInput;
      newObjective.objective_description = objectiveDescription;
      newObjective.objective_progress_status = 0;
      newObjective.objective_status = objectiveStatusTag;

      let teamid = this.teams[0].team_id;
      let teamName = this.teams[0].team_name;

      if(selectedTeam){
        teamid = selectedTeam[0].id;
        teamName = selectedTeam[0].text;
      }



      this._settingObjectiveService.addNewByObjective(newObjective ).subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {
          if (this.tempData.status == 'success' && this.tempData.data) {
            let newObjective = <Objectiveclass>this.tempData.data;

            let checkStatus = 0;
            this.createGoalObjectiveRelationship(newObjective, goalIds);

            this.createTeamObjectiveRelationship(newObjective, teamid);

            newObjective.team_info.team_id = teamid;
            newObjective.team_info.team_name = teamName;


            newObjective.keyResult_array=[];
            const tempArray = [];

            tempArray.push(newObjective);
            let i = 0;
            for (i = 0; i < this.objectives.length; i ++) {
              tempArray.push(this.objectives[i]);
            }

            this.objectives = tempArray;

            this._alertService.displaySuccessMessage( 'Your Team Objective create successfully' );
            // " Created a new Objective : " + newObjective.objective_name;
            this.createActivity(
              'Create',
              'Created a new Objective ' + newObjective.objective_name,
              '', // activity comment
              'o',
              newObjective.objective_id
            ); //action: string, activityDetails: string, groupType: string, groupId: string

            this.objectiveModal.hide();
            this.objectiveNameInputBoxValue = '';
            this.objectiveDescriptionInputBoxValue = '';
            // this.getTeamObjectiveByTimeFrameId(this.viewTeamID,this.currentTimeFrameId);
          }

        }
      );
    }


  }

  updateObjective(editObjective, objectiveNameInput: string, objectiveDescription: string, selectedTeam: any) {
    let modifyLog = '';

    if (!objectiveNameInput || !objectiveDescription||!this.selectedGoal) {
      this._alertService.displayWarningMessage("Objective Name or Objective Description empty!");
      return;
    } else {


      let goalIds = this.selectedGoal;
      let objectiveStatusTag = this.selectedTag[0].text;


      if (editObjective.objective_name != objectiveNameInput){

        modifyLog =  modifyLog +  "Change Objective name to: "+ objectiveNameInput+"; ";
      }
      if (editObjective.objective_description != objectiveDescription){


        modifyLog = modifyLog + " Change Objective description to: "+ objectiveDescription + "; ";
      }

      if (editObjective.objective_status != objectiveStatusTag){

        modifyLog =  modifyLog +  "Change Objective tag to: "+ objectiveStatusTag+"; ";
      }
      if (editObjective.team_info.team_name != this.selectedTeam[0].text){

        modifyLog =  modifyLog +  "Change Objective Team to: "+  this.selectedTeam[0].text +"; ";
      }

      let updateObjective = new Objectiveclass();
      updateObjective = editObjective;
      updateObjective.objective_description = objectiveDescription;
      updateObjective.objective_name = objectiveNameInput;

      // editObjective.object = timeFrameId;
      updateObjective.objective_status = objectiveStatusTag;


      this._settingObjectiveService.update(updateObjective)
        .subscribe(
          data => { this.tempData = data },
          error => this.errorMessage = <any>error,
          () => {



            if(this.tempData.status == 'success' && this.tempData.data)  {

              updateObjective.team_info.team_name = this.selectedTeam[0].text;

              //this.updateObjectiveTeams();
              this.updateTeamObjectiveRelationship(updateObjective, this.selectedTeam);



              this.createActivity(
                'Update',
                modifyLog,
                '', // activity comment
                'o',
                updateObjective.objective_id
              ); //action: string, activityDetails: string, groupType: string, groupId: string


              this._alertService.displaySuccessMessage('Your Objective has been updated.');




              this.objectiveNameInputBoxValue = '';
              this.objectiveDescriptionInputBoxValue = '';

              this.objectiveModal.hide();


            }else {

              this._alertService.displayErrorMessage(this.tempData.errorMessage);

            }

            this.editObjective = new Objectiveclass();


          }
        );

    }
  }


  createTeamObjectiveRelationship(objective: Objectiveclass, teamId: any ): any {
    this._settingObjectiveService.setTeamObjectives(objective, teamId).subscribe(
      data => { this.tempData = data },
      error => this.errorMessage = <any>error,
      () => {

        if (this.tempData.status == 'success' && this.tempData.data) {

          objective.team_info.team_id = teamId;
        } else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }

      }
    );
  }


  updateTeamObjectiveRelationship(objective: Objectiveclass, selectedTeams: any): any {
    let i = 0;
    const teamIdArray = [];

    if (selectedTeams){
      for ( i = 0; i < selectedTeams.length; i++){
        teamIdArray.push(selectedTeams[i].id);
      }
    }

    // console.log(teamIdArray);
    this._settingObjectiveService.updateTeamsObjectivesRelationship(objective, teamIdArray).subscribe(
      data => { this.tempData = data },
      error => this.errorMessage = <any>error,
      () => {

        if (this.tempData.status == 'success' && this.tempData.data) {

          // objective.team_info.team_id = teamId;
        } else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }

      }
    );
  }


  createGoalObjectiveRelationship(objective: Objectiveclass, golasArray:any ): any{
    this._settingObjectiveService.setGoalsObjectives(objective, golasArray).subscribe(
      data => { this.tempData = data },
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData.status == 'success' && this.tempData.data) {

        } else {

        }
      }
    );
  }



  setCurrentTeamObjectivesRelationship(objective: Objectiveclass){
    // console.log(objective);

    let currentObjective = new Objectiveclass();
    currentObjective = this.objectives.find( current => objective == current);

    if (!currentObjective.team_info.team_id || currentObjective.team_info.team_id == undefined ){
      this.selectedTeam = [{ id: this.teams[0].team_id.toString(), text: this.teams[0].team_name }];

      //this.selectedTag = [{ id: 'None', text: 'None'}];

    }else{
      this.selectedTeam = [];
      this.selectedTeam = [{ id: currentObjective.team_info.team_id.toString(), text: currentObjective.team_info.team_name  }];
    }
  }



  createActivity(action: string, activityDetails: string, activityComment: string, groupType: string, groupId: number){

    const submitANewActivity = new Activityclass();
    submitANewActivity.user_id =  this.selfInfo.user_id;
    submitANewActivity.activity_comment =  activityComment;
    submitANewActivity.activity_detail = activityDetails; // "Created a new Objective : " + newObjective.objective_name;
    submitANewActivity.activity_type = action;
    submitANewActivity.activity_group = groupType;
    submitANewActivity.activity_group_id = groupId;
    this.submitActivity(submitANewActivity);

  }


  submitActivity(activity: any) {
    this._okrActivitiesService.addNewByClass(activity).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if(this.tempData.data && this.tempData && <Activityclass>this.tempData.data){
          // swal("Success!", "Your goal has been created.", 'success');

        }
      }
    );
  }






}
