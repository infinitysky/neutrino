import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModulesRegisterModule } from '../../../shared/shared-modules-register.module';

import { OkrGoalsRoutingModule } from './okr-goals-routing.module';
import { OkrGoalsComponent } from './okr-goals.component';

@NgModule({
  imports: [
    SharedModulesRegisterModule.forRoot(),

    CommonModule,
    OkrGoalsRoutingModule
  ],
  declarations: [OkrGoalsComponent]
})
export class OkrGoalsModule { }
