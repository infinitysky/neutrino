import { Component, OnInit, ViewChild  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
// Observable class extensions
import 'rxjs/add/observable/of';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { Subscription } from 'rxjs/Subscription';


// 3rd-party libraries
import { TabsetComponent } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';




// Services
import {SettingGoalService} from '../../okr-shared/services/okr-goal.service';
import {ShareGoalDetailsService} from './share-goal-details.service';
import { UserInfoContainerService } from '../../../../shared/services/user-info-container.service';


import {AlertService} from '../../../../shared/services/alert.service';
import { SettingTimeFrameService } from '../../okr-shared/services/okr-time-frame.service';
import { SettingTeamService } from '../../okr-shared/services/okr-team.service';
import {ReviewsService} from '../../okr-shared/services/reviews.service';
import {OkrActivitiesService} from '../../okr-shared/services/okr-activities.service';



// Classes
import { Timeframeclass } from '../../okr-shared/classes/time-frame-class';
import { Teamclass } from '../../okr-shared/classes/team-class';
import { Activityclass} from '../../okr-shared/classes/activitie-class';
import { Reviewclass } from '../../okr-shared/classes/review-class';
import { Goalclass } from '../../okr-shared/classes/goal-class';
import { Userclass } from '../../../../shared/classes/user-class';








// Component
import {GoalTeamsComponent} from './goal-teams/goal-teams.component';
import {GoalActivitiesComponent} from './goal-activities/goal-activities.component';
import {GoalOkrsComponent} from './goal-okrs/goal-okrs.component';

@Component({
  selector: 'app-okr-goals-detail',
  templateUrl: './okr-goals-detail.component.html',
  providers: [
    SettingGoalService, ReviewsService, SettingTimeFrameService, SettingTeamService, OkrActivitiesService
  ],
  styleUrls: ['./okr-goals-detail.component.css']
})
export class OkrGoalsDetailComponent implements OnInit {

  public selfInfo: Userclass;
  public selfInfoSubscription: Subscription;
  public isAdmin: boolean;
  public goalId: any;

  public goalSubscription: Subscription;
  public goalInfo: Goalclass;
  public goalInfoBackup: Goalclass;
  public overallGoalInfo: Goalclass;


  private tempData: any;
  public errorMessage: any;
  public displayStyle: any;
  public labelStyle: any;



  public totalObjectivesNumber: any;
  public overallProgressNumber: any;

  private overallProgressNumberSubscription: Subscription;
  private totalObjectivesNumberSubscription: Subscription;


  @ViewChild('goalModal')  goalModal: ModalDirective;

  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  @ViewChild(GoalTeamsComponent) goalTeams: GoalTeamsComponent;
  @ViewChild(GoalActivitiesComponent) goalActivities: GoalActivitiesComponent;
  @ViewChild(GoalOkrsComponent) goalOKRs: GoalOkrsComponent;




  // ------------------ Modal ------------------



  public modalTitle: string;
  public editModeIO: number;

  public timeFrames: Timeframeclass[];
  public submitActivityInfo: Activityclass;



  //goalModal parameter
  public goalsData: any;

  public isLoaded: boolean;
  public selectedGoal: Goalclass;

  //edit mode parameter
  editGoal: Goalclass;
  goalNameInputBoxValue: string;
  goalDescriptionInputBoxValue: string;


  //Dropdownlist;

  public timeFrameDropDownListOptions: any;
  public selectedTimeFrame: any;


  public tagDropDownListOptions: any;
  public selectedTag: any;


  // RATINGS METHODS + Review

  public max: number;
  public rate: number;
  public isReadonly: boolean;
  public overStar: number;
  public percent: number;
  public reviewDescription: string;



  public reviewInfo: Reviewclass;










  constructor(
    private _reviewsService: ReviewsService,
    private _alertService: AlertService,

    private _settingGoalService: SettingGoalService,
    private _settingTimeFrameService: SettingTimeFrameService,
    private _okrActivitiesService: OkrActivitiesService,


    private _userInfoContainerService: UserInfoContainerService,
    private _shareGoalDetailsService: ShareGoalDetailsService,

    private _activatedRoute: ActivatedRoute,
    private _router: Router
  ) {


    this.isLoaded = true;
    this.modalTitle = '';


    this.isAdmin = false;
    this.timeFrames = new Array<Timeframeclass>();
    this.editModeIO = 0;
    this.editGoal = new Goalclass();
    this.goalNameInputBoxValue = '';
    this.goalDescriptionInputBoxValue = '';
    this.timeFrameDropDownListOptions = [];
    this.selectedTimeFrame = [];

    this.submitActivityInfo = new Activityclass();
    this.selfInfo = new Userclass();





    this.tagDropDownListOptions=[{ id: "None", text: "None" },{ id: "Warning", text: "Warning" },{ id: "Risk", text: "Risk" },{ id: "Complete", text: "Complete" }];
    this.selectedTag=[{ id: "None", text: "None" }];



    // rating
    this.isReadonly = true;
    this.max = 10;
    this.rate = 0;
    this.overStar = 0;
    this.percent = 0;
    this.reviewDescription = '';





    this.goalId = 0;
    this.goalInfo = new Goalclass();
    this.goalInfoBackup = new Goalclass();

    this.overallGoalInfo = new Goalclass();

    this.tempData = '';
    this.errorMessage = '';
    // this.displayStyle = '';
    this.isAdmin = false;

  }

  ngOnInit() {
    this.loadGoalInfo();
  }



  ngOnDestroy() {
    this.goalSubscription.unsubscribe();
    this.overallProgressNumberSubscription.unsubscribe();
    this.totalObjectivesNumberSubscription.unsubscribe();
    this.selfInfoSubscription.unsubscribe();

  }

  loadGoalInfo(){
    this.subsTargetGoalId();
    this.goalInfoSubscribe();
    this.selfInfoSubscribe();

  }

  selfInfoSubscribe(){
    this.selfInfoSubscription = this._userInfoContainerService.userInfo$.subscribe(selfInfo => this.selfInfo = selfInfo);
    if (this.selfInfo.role.toLowerCase() == 'admin'){
      this.isAdmin = true;
    }else{
      this.isAdmin = false;
    }

  }

  subsTargetGoalId(){
    this.goalSubscription = this._activatedRoute.params.subscribe(params => {
      this.goalId = ''+params['goalId'] || 0; // (+) converts string 'id' to a number


      if (!this.goalId || this.goalId == 'undefined'){
        // console.log( 'empty' );
      }else{
        this.getTargetGoalInfo(this.goalId);

      }

    });

  }
  getTargetGoalInfo(goalId){
    this.goalInfo = new Goalclass();
    this.goalInfoBackup = new Goalclass();
    this._settingGoalService.getDetailedById( Number(goalId) ).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData.status == 'success'){

          if(this.tempData.data){

            // This data retured as an array type;
            this.goalInfo = <Goalclass> this.tempData.data[0];
            this.goalInfoBackup = <Goalclass> this.tempData.data[0];
            //this.objectivesCalculation( this.goalInfo );
            this.updateStyle(this.goalInfo.goal_status.toString());

          }

        }else {
          console.log('error');
        }

      }
    );

  }

  goalInfoSubscribe() {

    this.overallProgressNumberSubscription = this._shareGoalDetailsService._shareOverallProgressNumber$.subscribe(data => this.overallProgressNumber = data);

    if (!this.overallProgressNumber) {
      this.overallProgressNumber = 0;
    }
    this.totalObjectivesNumberSubscription = this._shareGoalDetailsService._shareObjectivesNumber$.subscribe(data => this.totalObjectivesNumber = data);
    if(!this.totalObjectivesNumber){
      this.totalObjectivesNumber = 0 ;
    }
  }


  updateStyle(currentStatus: string){
    // {'hred': true, 'hgreen': false, 'hyellow': false, 'hblue': false}
    let status = currentStatus.toLowerCase();
    switch (status){
      case 'warning': {
        this.displayStyle = {'hyellow': true};
        this.labelStyle = {'label-warning ':true};
        break;

      }
      case 'risk':{
        this.displayStyle = {'hred': true};
        this.labelStyle = {'label-danger': true};
        break;
      }
      case 'complete':{
        this.displayStyle = {'hgreen': true};
        this.labelStyle = {'label-success': true};
        break;
      }
      default:{
        this.displayStyle = {'hblue': true};
        this.labelStyle = {'label': false};
        break;

      }
    }
  }


  loadGoalOKRsInformation(){
    this.goalOKRs.loadOKRSInfo();
  }
  loadTeamsInformation(){
    this.goalTeams.loadTeamsInfo();
  }

  loadActivitiesInformation(){
    this.goalActivities.loadActivitiesInfo();
  }

  selectTab(tab_id: number) {
    this.staticTabs.tabs[tab_id].active = true;
  }

  // Modal

  // At this function I just need update
  modalSaveChangeButton(goalNameInput: string, goalDescription: string) {

    /*
     if (0 == this.editModeIO) {
     this.createNewGoal(goalNameInput, goalDescription);
     } else {
     */
    this.updateGoal(this.editGoal, goalNameInput, goalDescription);
    // }
  }


  editGoalsButton(Goal) {
    this.modalTitle = 'Update Goal';

    this.editModeIO = 1;
    this.editGoal = Goal;
    this.goalNameInputBoxValue = Goal.goal_name;
    this.goalDescriptionInputBoxValue = Goal.goal_description;



    const timeFrameName = Goal.time_frame_description
      + "    --- (" + Goal.time_frame_start +
      " To " + Goal.time_frame_end + ")";


    this.selectedTimeFrame = [{ id: Goal.time_frame_id, text: timeFrameName }];
    this.selectedTag = [{ id: Goal.goal_status, text: Goal.goal_status }];

    this.getAllTimeFrames();



    this.goalModal.show();

  }

  closeGoalModal(){
    this.goalModal.hide();
  }

  /*
   createNewGoal(goalNameInput: string, goalDescription: string) {

   if (!goalNameInput || !this.selectedTimeFrame[0]) {
   //alert("Do not leave any empty!");
   this._alertService.displayWarningMessage('Do not leave any empty!');

   return;
   } else {
   const timeFrameId = this.selectedTimeFrame[0].id;
   const goalStatusTag = this.selectedTag[0].id;
   this._settingGoalService.addNew(goalNameInput, goalDescription, timeFrameId, goalStatusTag ).subscribe(
   data => this.tempData = data,
   error => this.errorMessage = <any>error,
   () => {

   if (this.tempData.status == "success" && this.tempData.data) {

   const tempInfo = <Goalclass>this.tempData.data;
   const searchedTimeFrame = this.timeFrames.find(x => x.time_frame_id == tempInfo.time_frame_id);

   tempInfo.time_frame_description = searchedTimeFrame.time_frame_description;
   tempInfo.time_frame_start = searchedTimeFrame.time_frame_start;
   tempInfo.time_frame_end = searchedTimeFrame.time_frame_end;
   const tempArray=[];
   tempArray.push(tempInfo);
   let i = 0;
   for ( i = 0; i < this.goals.length; i ++ ){
   tempArray.push(this.goals[i]);
   }
   this.goals = tempArray;
   this.updateOverallNumbers();
   this.goalNameInputBoxValue = "";
   this.goalDescriptionInputBoxValue = "";

   var submitANewActivity= new Activityclass();
   submitANewActivity.user_id=this.selfInfo.user_id;
   submitANewActivity.activity_detail = " Created a new goal : " + tempInfo.goal_name;
   submitANewActivity.activity_type="Create";
   this.submitActivity(submitANewActivity);

   } else {
   this._alertService.displayErrorMessage(this.tempData.errorMessage);

   }

   }
   );
   }

   this.goalModal.hide();
   }


   */



  updateGoal(editGoal, goalNameInput: string, goalDescription: string) {

    if (!goalNameInput) {
      //alert("Do not leave any empty!");
      // swal("Warning", "you did not change any time!", "warning");\
      return;
    } else {

      const originalGoal = this.goalInfoBackup;


      let modifyLog = '';
      if (originalGoal.goal_description !=goalDescription){
        modifyLog = modifyLog + ' Change Goal description to ' + goalDescription + '; ';
      }
      if (originalGoal.goal_name != goalNameInput){
        modifyLog = modifyLog + "Change goal name to " + goalNameInput +"; ";
      }
      if (originalGoal.goal_status != this.selectedTag[0].id){
        modifyLog = modifyLog + "Change goal tag to " + this.selectedTag[0].id+"; ";
      }

      if (originalGoal.time_frame_id !=  this.selectedTimeFrame[0].id){
        modifyLog = modifyLog + "Change goal time_frame to " + this.selectedTimeFrame[0].text+"; ";
      }
      //console.log(modifyLog);

      const timeFrameId = this.selectedTimeFrame[0].id;
      const goalStatusTag = this.selectedTag[0].id;

      editGoal.goal_description = goalDescription;
      editGoal.goal_name = goalNameInput;

      editGoal.time_frame_id = timeFrameId;
      editGoal.time_frame_description = this.selectedTimeFrame[0].text;
      editGoal.goal_status = goalStatusTag;



      this.goalInfoBackup.goal_description = goalDescription;
      this.goalInfoBackup.goal_name = goalNameInput;
      this.goalInfoBackup.time_frame_id = timeFrameId;
      this.goalInfoBackup.time_frame_description = this.selectedTimeFrame[0].text;
      this.goalInfoBackup.goal_status = goalStatusTag;


      this._settingGoalService.update(editGoal)
        .subscribe(
          data => { this.tempData = data },
          error => this.errorMessage = <any>error,
          () => {
            // console.log("update Members this.tempData + " + JSON.stringify(this.tempData));
            // console.log(this.tempData.data);

            if(this.tempData.status == "success" && this.tempData.data)  {

              this._alertService.displaySuccessMessage('Your goal has been updated. ');

              this.goalNameInputBoxValue = '';
              this.goalDescriptionInputBoxValue = '';

              this.createActivity(
                'Update',
                modifyLog,
                '', // activity comment
                'g',
                editGoal.goal_id
              ); //action: string, activityDetails: string, groupType: string, groupId: string


              this.loadGoalInfo();



            }else {
              //swal("Warning", this.tempData.errorMessage, "warning");
              this._alertService.displayErrorMessage(this.tempData.errorMessage);

            }

          }
        );


    }

    this.goalModal.hide();

  }



  getAllTimeFrames() {
    this._settingTimeFrameService.getAllTimeFrames()
      .subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {


          if (this.tempData.status == "success" && this.tempData.data) {
            this.timeFrames =<Timeframeclass[]> this.tempData.data;
            this.setTimeFrameDropDownList(this.timeFrames);
          }
        }
      );

  }


  setTimeFrameDropDownList(timeframes: Timeframeclass[]) {
    let i = 0;
    const tempArray = [];


    for (i = timeframes.length - 1; i > 0; i--) {
      const timeFrameName = timeframes[i].time_frame_description
        + '   --- (' + timeframes[i].time_frame_start +
        ' To ' + timeframes[i].time_frame_end + ')';


      const tempInfo1 = { id: timeframes[i].time_frame_id, text: timeFrameName };
      tempArray.push(tempInfo1);

    }
    // This way is working...
    this.timeFrameDropDownListOptions = tempArray;

  }





















  /* ---------------------------------- Activities ----------------------------------------------------------
   * */



  createActivity(action: string, activityDetails: string, activityComment: string, groupType: string, groupId: number){

    const submitANewActivity = new Activityclass();
    submitANewActivity.user_id =  this.selfInfo.user_id;
    submitANewActivity.activity_comment =  activityComment;
    submitANewActivity.activity_detail = activityDetails; // "Created a new Objective : " + newObjective.objective_name;
    submitANewActivity.activity_type = action;
    submitANewActivity.activity_group = groupType;
    submitANewActivity.activity_group_id = groupId;
    this.submitActivity(submitANewActivity);

  }


  submitActivity(activity: any) {
    this._okrActivitiesService.addNewByClass(activity).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if(this.tempData.data && this.tempData && <Activityclass>this.tempData.data){
          // swal("Success!", "Your goal has been created.", 'success');

        }
      }
    );
  }














  // -------------------------------------------- Rating --------------------------------




  closeReviewGoalButton(){
    this.reviewInfo = new Reviewclass();
    //this.reviewGoalModal.hide();
  }































}
