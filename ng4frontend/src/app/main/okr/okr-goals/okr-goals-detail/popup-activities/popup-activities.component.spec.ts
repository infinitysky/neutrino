import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupActivitiesComponent } from './popup-activities.component';

describe('PopupActivitiesComponent', () => {
  let component: PopupActivitiesComponent;
  let fixture: ComponentFixture<PopupActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
