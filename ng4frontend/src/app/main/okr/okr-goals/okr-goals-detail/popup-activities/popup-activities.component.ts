import { Component, OnInit } from '@angular/core';

import {OkrActivitiesService} from '../../../okr-shared/services/okr-activities.service';
import {Activityclass} from '../../../okr-shared/classes/activitie-class';
@Component({
  selector: 'app-popup-activities',
  templateUrl: './popup-activities.component.html',
  providers: [ OkrActivitiesService ],
  styleUrls: ['./popup-activities.component.css']
})
export class PopupActivitiesComponent implements OnInit {

  public activitiesInfo: Activityclass[];
  public tempData: any;
  public errorMessage: any;

  constructor(
    private _okrActivitiesService: OkrActivitiesService
  ) {
    this.activitiesInfo = new Array<Activityclass>();

  }

  ngOnInit() {
  }


  getInfo(keyResult) {
    this.activitiesInfo = new Array<Activityclass>();
    this._okrActivitiesService.getByKeyResultId(keyResult).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData && this.tempData.status.toLowerCase() == 'success'){
          this.activitiesInfo = <Activityclass[]> this.tempData.data;
        }
    }
    );
  }

}
