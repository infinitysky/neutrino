import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//import 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
// Observable class extensions
import 'rxjs/add/observable/of';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { Subscription } from 'rxjs/Subscription';


// 3rd-party libraries
import { ModalDirective } from 'ngx-bootstrap/modal';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { NouisliderModule } from 'ng2-nouislider';


import {UserDetailsService} from '../../../../../shared/services/user-details.service';
import {ShareGoalDetailsService} from '../share-goal-details.service';
import {UserInfoContainerService} from '../../../../../shared/services/user-info-container.service';



// goal data service
import { SettingGoalService } from '../../../okr-shared/services/okr-goal.service';
// time frame data service
import { SettingTimeFrameService } from '../../../okr-shared/services/okr-time-frame.service';
// team data service
import { SettingTeamService } from '../../../okr-shared/services/okr-team.service';
// objective data service
import { SettingObjectiveService } from '../../../okr-shared/services/okr-objective.service';
// key result data service
import { SettingKeyResultService } from '../../../okr-shared/services/okr-key-result.service';
// activity data service. submit 'create' and 'update'
import {OkrActivitiesService} from '../../../okr-shared/services/okr-activities.service';
import {AlertService} from '../../../../../shared/services/alert.service';
import {OverallCalculatorService} from '../../../okr-shared/services/overall-calculator.service';
import { ReviewsService } from '../../../okr-shared/services/reviews.service';
// classes here
import { Reviewclass } from '../../../okr-shared/classes/review-class';
import { Timeframeclass } from '../../../okr-shared/classes/time-frame-class';
import { Teamclass } from '../../../okr-shared/classes/team-class';
import { Goalclass } from '../../../okr-shared/classes/goal-class';
import { Objectiveclass } from '../../../okr-shared/classes/objective-class';
import {Keyresultclass} from '../../../okr-shared/classes/key-restult-class';
import {Activityclass} from '../../../okr-shared/classes/activitie-class';
import {Userclass} from '../../../../../shared/classes/user-class';





@Component({
  selector: 'app-goal-okrs',
  templateUrl: './goal-okrs.component.html',
  providers: [
    SettingGoalService,
    SettingTimeFrameService,
    SettingTeamService,
    SettingObjectiveService,
    SettingKeyResultService,
    OkrActivitiesService,
    AlertService,
    UserDetailsService,
    OverallCalculatorService,
    ReviewsService

  ],
  styleUrls: ['./goal-okrs.component.css']
})
export class GoalOkrsComponent implements OnInit {

  public goalInfo: Goalclass;


  private goalId: string;
  private goalIdSubscription: Subscription;

  private userInfoSubscription: Subscription;
  private selfInfo: Userclass;
  public isAdmin: boolean;
  private tempData: any;
  public errorMessage: any;

  public isLoading: boolean;

  public activitiesInfo: any;
  public activitiesLoading: boolean;

  public reviewInfo: Reviewclass;


  /*
   * --------------Modals--------------------------------------------------------------------------------
   *
   * */



  @ViewChild('objectiveModal') public objectiveModal: ModalDirective;
  @ViewChild('keyResultModal') public keyResultModal: ModalDirective;
  @ViewChild('updateProgressModal') public updateProgressModal: ModalDirective;
  @ViewChild('activitiesModal') public activitiesModal: ModalDirective;
  @ViewChild('reviewObjectiveModal') public reviewObjectiveModal: ModalDirective;



  public modalTitle: string;


  public goals: Goalclass[];

  public timeFrames: Timeframeclass[];
  public teams: Teamclass[];

  public objectives: Objectiveclass[];


  public keyResultsArray: Keyresultclass[];
  //public keyResultsBackup: Keyresultclass[];
  // public backUpTeamKeyResult: Keyresultclass[];


  public newSubmitActivity: Activityclass;







  //--------------goalModal parameter------------------

  //goalModal action control
  animation: boolean = true;
  keyboard: boolean = true;
  backdrop: string | boolean = "static";
  public modalType:string=''; //objective | keyresult


  //current model
  public editModeIO: number;


  //edit mode parameter
  public editObjective: any;
  public parentObjective: any;
  public editKeyResult: any;

  public editReview: any;


  public objectiveNameInputBoxValue: string;
  public objectiveDescriptionInputBoxValue: string;

  public keyResultNameInputBoxValue: string;
  public keyResultDescriptionInputBoxValue: string;
  public keyResultProgressValue: number;
  public progressUpdateDescription: string;

  //Dropdownlist;
  public timeFrameDropDownListOptions: any;
  public selectedTimeFrame: any;

  public goalsDropDownListOptions: any;
  public selectedGoal: Array<any>;



// TODO: DO we have to set the tag on key-result?
  public tagDropDownListOptions: any;
  public selectedTag: any;

  public teamDropDownListOptions: any;
  public selectedTeam: any;

  public currentTimeFrameId: any;


// Multi Selection for Goals
  public goalsSelectorSettings: IMultiSelectSettings = {
    pullRight: false,
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default',
    selectionLimit: 0,
    closeOnSelect: false,
    showCheckAll: true,
    showUncheckAll: true,
    dynamicTitleMaxItems: 0,
    maxHeight: '300px',
  };

  public goalsSelectorTexts: IMultiSelectTexts = {
    checkAll: 'Check all',
    uncheckAll: 'Uncheck all',
    checked: 'checked',
    checkedPlural: 'checked',
    searchPlaceholder: 'Search...',
    defaultTitle: 'Select Goals',
  };




  // RATINGS METHODS

  public max: number;
  public rate: number;
  public isReadonly: boolean;
  public overStar: number;
  public percent: number;
  public reviewDescription: string;





  constructor(
    private _reviewsService: ReviewsService,
    private _overallCalculatorService: OverallCalculatorService,
    private _alertService: AlertService,
    private _userInfoContainerService: UserInfoContainerService,
    private _shareGoalDetailsService: ShareGoalDetailsService,
    private _activatedRoute: ActivatedRoute,
    private _settingGoalService: SettingGoalService,
    private _userDetailsService: UserDetailsService,

    /*
     * --------------Modal----------------------------------------
     *
     * */
    private _settingTimeFrameService: SettingTimeFrameService,
    private _settingTeamService: SettingTeamService,
    private _settingObjectiveService: SettingObjectiveService,
    private _settingKeyResultService: SettingKeyResultService,
    private _okrActivitiesService: OkrActivitiesService,


  ) {
    this.selfInfo = new Userclass();
    this.goalInfo =  new Goalclass();

    this.isAdmin = false;
    this.isLoading = true;

    this._shareGoalDetailsService.setObjectivesSubjectNumber(0);
    this._shareGoalDetailsService.setGoalsSubject('');
    this._shareGoalDetailsService.setOverAllProgressSubject(0);

    this.activitiesInfo =  new Array<Activityclass>();

    this.reviewInfo = new Reviewclass();
    /*
     * pop up
     * */
    this.activitiesLoading = true;




    /*
     * --------------------------Modal-----------------------------------------
     *
     * */
    this.goalId = '';
    this.modalTitle = '';

    this.goals = [];
    this.timeFrames = [];

    this.teams = [];
    this.objectives = [];
    this.keyResultsArray = [];

    this.editModeIO = 0;
    this.editObjective = new Objectiveclass();
    this.parentObjective = new Objectiveclass();

    this.objectiveNameInputBoxValue = '';
    this.objectiveDescriptionInputBoxValue = '';

    this.editKeyResult = new Keyresultclass();
    this.keyResultNameInputBoxValue = '';
    this.keyResultDescriptionInputBoxValue = '';
    this.progressUpdateDescription = '';


    //Drop Down List
    this.timeFrameDropDownListOptions = [];
    this.selectedTimeFrame = [];


    this.goalsDropDownListOptions = [];
    this.selectedGoal = []; // multi selector is using different plugin

    this.newSubmitActivity = new Activityclass();

    this.keyResultProgressValue = 0;


    this.tagDropDownListOptions = [{ id: 'None', text: 'None' }, { id: 'Warning', text: 'Warning' }, { id: 'Risk', text: 'Risk' }, { id: 'Complete', text: 'Complete' }];
    this.selectedTag = [{ id: 'None', text: 'None' }];

    this.teamDropDownListOptions = [];
    this.selectedTeam = [];


    // once load
    this.getAllGoals();
    this.getTeamsInfo();
    this.setGoalsDropDownList(this.goals);
    this.setTeamDropDownOptions(this.teams);




// rating
    this.isReadonly = true;
    this.max = 10;
    this.rate = 0;
    this.overStar = 0;
    this.percent = 0;
    this.reviewDescription = '';
  }

  ngOnInit() {
    this.loadOKRSInfo();
    // this.selfInfo = new Userclass();
    // this.goalInfo =  new Goalclass();
    // this.getSelfInfo();
    // this.subsTargetGoalId();
  }

  ngOnDestroy() {
    this.goalIdSubscription.unsubscribe();
    this.userInfoSubscription.unsubscribe();
  }



  loadOKRSInfo() {


    this.goalInfo =  new Goalclass();
    this.isAdmin = false;
    this.subsTargetGoalId();
    this.getSelfInfo();
    this.getAllKeyResult();

  }





  subsTargetGoalId() {
    this.goalIdSubscription = this._activatedRoute.params.subscribe(params => {
      this.goalId = '' + params['goalId']; // (+) converts string 'id' to a number
      this.getGoalsInfo( this.goalId);
    });
  }

  getGoalsInfo(goalId: any) {
    this.goalInfo = new Goalclass();

    this.isLoading = true;
    this._settingGoalService.getDetailedById(goalId).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if(this.tempData){
          if(this.tempData.status.toLowerCase() == 'success' && this.tempData.data){
            this.goalInfo = <Goalclass> this.tempData.data[0]; //  the backend API will return the data as array

            this.updateOverallNumbers(this.goalInfo);
            this.isLoading = false;
            this.setCurrentGoal(this.goalInfo);
          }
        }
      }
    );

  }


  updateOverallNumbers(goalDetails) {
    let overAllObjectivesNumber  = 0;
    goalDetails = this._overallCalculatorService.goalProgressRecalculate(goalDetails);
    const overAllProgressNumber = goalDetails.goal_progress_status;

    if(goalDetails.objective_array){
      overAllObjectivesNumber = goalDetails.objective_array.length;
    }else {
      overAllObjectivesNumber = 0;
    }


    this._shareGoalDetailsService.setObjectivesSubjectNumber(overAllObjectivesNumber);
    this._shareGoalDetailsService.setOverAllProgressSubject(overAllProgressNumber);
    this._shareGoalDetailsService.setGoalsSubject(goalDetails);

  }



  getSelfInfo() {
    this.selfInfo = new Userclass();
    this.userInfoSubscription = this._userInfoContainerService.userInfo$.subscribe( userData => this.selfInfo = userData );
    if( this.selfInfo){
      if( this.selfInfo.role == 'admin'){
        this.isAdmin = true;
        this.isReadonly = false;
      }
    }else{
      this.selfInfo = new Userclass();
    }

  }



  keyResultEditable(CurrentObjective: Objectiveclass): boolean{
    let editableIO = false;

    if (this.isAdmin && this.isAdmin == true ){
      editableIO = true;
    }else{
      if (CurrentObjective){
        let i = 0;

        while (i < CurrentObjective.team_members.length){
          if (CurrentObjective.team_members[i].user_id == this.selfInfo.user_id ){
            editableIO = true;
            break;
          }else {
            // console.log(CurrentObjective.team_members[i]);
            i++;
          }
        }
      }
    }

    return editableIO;
  }



  /*
   * pop up
   * */
  openPOPup(keyResult) {
    this.activitiesLoading = true;
    this.activitiesInfo = new Array<Activityclass>();
    this._okrActivitiesService.getByKeyResultId(keyResult).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData && this.tempData.status.toLowerCase() == 'success'){
          this.activitiesInfo = <Activityclass[]> this.tempData.data;
        }
        this.activitiesLoading = false;
      }
    );

  }








  /*
   * -------------------------------Modals Controller-------------------------------------------------------------------------------
   *
   * */


  /*
   * -------------------------------------Modals Button---------------------------------------
   * */


  addObjectiveButton() {
    this.getAllGoals();
    // this.setCurrentGoal(this.goalInfo);

    this.getTeamsInfo();

    this.modalType = 'objective';
    this.modalTitle = 'Create A Objective';
    this.editModeIO = 0;

    this.selectedTag = [{ id: 'None', text: 'None'}];
    this.selectedTeam = [];
    this.objectiveNameInputBoxValue = '';
    this.objectiveDescriptionInputBoxValue = '';

    this.objectiveModal.show();

  }

  closeObjectiveButton() {

    this.modalType = '';
    this.modalTitle = '';
    this.editModeIO = 0;
    this.selectedTag = [{ id: 'None', text: 'None'}];
    this.selectedTeam = [];
    this.objectiveNameInputBoxValue = '';
    this.objectiveDescriptionInputBoxValue = '';
    this.objectiveModal.hide();

  }




  addKeyResultButton(parentObjective) {
    this.modalType = 'keyresult';
    this.modalTitle = 'Create A Key Result';
    this.editModeIO = 0;
    this.parentObjective = new Objectiveclass();
    this.parentObjective = parentObjective;
    // this.editObjective = parentObjective;

    this.keyResultNameInputBoxValue = '';
    this.objectiveDescriptionInputBoxValue = '';
    this.keyResultProgressValue = 0;
    this.keyResultModal.show();

  }


  closeKeyResultButton() {
    this.modalType = '';
    this.modalTitle = '';
    this.editModeIO = 0;
    this.keyResultNameInputBoxValue = '';
    this.keyResultDescriptionInputBoxValue = '';
    this.keyResultProgressValue = 0;
    this.keyResultModal.hide();
  }


  closeKeyResultProgressChangeButton() {


    this.updateProgressModal.hide();


    // keyResult.result_progress_status

    this.progressUpdateDescription = '';
    this.modalType = '';
    this.modalTitle = '';
    this.editModeIO = 0;

    //this.loadOKRSInfo();
    this.ngOnInit();
  }





  deleteObjectiveButton(deletedObjective) {
    this._settingObjectiveService
      .delete(deletedObjective)
      .subscribe(
        data => { this.tempData = data},
        error => { this.errorMessage = <any>error},
        () => {

          if (this.tempData.status=='success' && this.tempData.data.affectRows) {
            if( this.tempData.data.affectRows>0){
              this.createActivity(
                'Delete',
                'Deleted Objective : ' + deletedObjective.objective_name + ' ( Objective ID: '+deletedObjective.objective_id + ')',
                '', // activityComment
                'g',
                this.goalInfo.goal_id
              ); //action: string, activityDetails: string, groupType: string, groupId: string

              this.goalInfo.objective_array = this.goalInfo.objective_array.filter(currentObjectives => currentObjectives !== deletedObjective);


              //this.updateOverAllNumbers();
              this.updateOverallNumbers(this.goalInfo);


              this._alertService.displaySuccessMessage( 'Your Objective has been deleted.');
            }else {
              this._alertService.displayErrorMessage('Your Objective did not deleted successfully.');
            }

          } else {
            this._alertService.displayErrorMessage('Your Objective did not deleted successfully.');
          }
        }
      );
  }



  deleteKeyResultButton(goalObjective, deletedKeyResult) {

    let i = 0;
    this._settingKeyResultService
      .delete(deletedKeyResult)
      .subscribe(
        data => { this.tempData = data},
        error => { this.errorMessage = <any>error},
        () => {
          if (this.tempData.data.affectRows > 0) {

            for ( i = 0; i < this.goalInfo.objective_array.length ; i ++  ){
              if (this.goalInfo.objective_array[i].objective_id == goalObjective.objective_id){
                this.goalInfo.objective_array[i].keyResult_array = this.goalInfo.objective_array[i].keyResult_array.filter(currentKeyResult => currentKeyResult !== deletedKeyResult);


              }
            }

            this.createActivity(
              'Delete',
              'Deleted Objective : ' + deletedKeyResult.result_name + ' ( Key Result ID: '+ deletedKeyResult.result_id + ')',
              '', //activityComment
              'o',
              goalObjective.objective_id
            ); //action: string, activityDetails: string, groupType: string, groupId: string



            this._alertService.displaySuccessMessage('Your Key Result has been deleted.');

          } else {
            this._alertService.displayErrorMessage('Your Key Result did not deleted successfully.');
          }
        }
      );

    this.updateOverallNumbers(this.goalInfo);

  }


  modalSaveObjectiveChangeButton() {
    // read the 2 way binding;
    let objectiveNameInput = this.objectiveNameInputBoxValue;
    let objectiveDescription = this.objectiveDescriptionInputBoxValue;

    if (!objectiveNameInput || !objectiveDescription || !this.selectedGoal) {
      //alert("Do not leave any empty!");

      this._alertService.displayWarningMessage("Objective Name, Objective Description or Goal selector empty!");

    } else if  ( this.selectedTeam.length == 0  ){
      this._alertService.displayWarningMessage("Team selector empty!");
    }
    else{
      if (0 == this.editModeIO) {
        this.createNewObjective(objectiveNameInput, objectiveDescription, this.selectedTeam);
      } else {
        this.updateObjective(this.editObjective, objectiveNameInput, objectiveDescription, this.selectedTeam);
      }
    }


  }


  modalSaveKeyResultChangeButton() {
    // read the 2 way binding;
    const keyResultNameInput = this.keyResultNameInputBoxValue;
    const keyResultDescription = this.keyResultDescriptionInputBoxValue;
    const keyResultProgressValue = this.keyResultProgressValue;

    if (keyResultProgressValue > 100){
      this._alertService.displayErrorMessage(' Key Result Progress Value Must Lower Than 100% !');
    }else if (keyResultProgressValue < 0){
      this._alertService.displayErrorMessage(' Key Result Progress Value Must Great Than 0% !');
    }else{

      if (0 == this.editModeIO) {
        this.createNewKeyResult(keyResultNameInput, keyResultDescription, this.parentObjective, keyResultProgressValue);
      } else {


        this.updateKeyResult(this.editKeyResult, keyResultNameInput, keyResultDescription, keyResultProgressValue);
      }

    }


  }




  modalSaveKeyResultProgressChangeButton() {
    // read the 2 way binding;
    if(!this.progressUpdateDescription){
      this._alertService.displayWarningMessage('Please leave your update log');
    }else{
      const progressUpdateDescription = this.progressUpdateDescription;



      this.keyResultProgressUpdate( this.editKeyResult, progressUpdateDescription );

      // this.updateKeyResultArray(this.editKeyResult);

      this.modalType = '';
      this.modalTitle = '';
      this.progressUpdateDescription = '';
      this.editModeIO = 0;
      this.editKeyResult = new Keyresultclass();


      this.updateProgressModal.hide();
    }



  }


  editObjectiveButton(objective: Objectiveclass) {
    this.getAllGoals();
    this.modalType = 'objective';
    this.modalTitle = 'Update A Objective';
    this.editModeIO = 1;
    this.editObjective = new Objectiveclass();
    this.editObjective = objective;
    this.objectiveNameInputBoxValue = objective.objective_name;
    this.objectiveDescriptionInputBoxValue = objective.objective_description;

    this.selectedTag = [{ id: objective.objective_status, text: objective.objective_status }];

    this.setCurrentTeamObjectivesRelationship(objective);
    this.objectiveModal.show();

  }

  editKeyResultButton(Objective: Objectiveclass, keyResult: Keyresultclass) {
    this.modalType = 'keyresult';
    this.modalTitle = 'Update A Key Result';
    this.editModeIO = 1;

    this.parentObjective = new Objectiveclass();
    this.parentObjective = <Objectiveclass> Objective;

    this.editKeyResult = new Keyresultclass();
    this.editKeyResult = <Keyresultclass> keyResult;



    // console.log('parentObjective' + JSON.stringify(this.parentObjective));
    // console.log('editKeyResult' + JSON.stringify(this.editKeyResult));

    this.keyResultNameInputBoxValue = keyResult.result_name;
    this.keyResultDescriptionInputBoxValue = keyResult.result_description;
    this.keyResultProgressValue = Number(keyResult.result_progress_status);
    this.keyResultModal.show();

  }


  keyResultProgressUpdate(keyResult: Keyresultclass , progressUpdateDescription){


    const originalKeyResult = this.keyResultsArray.find(result => keyResult.result_id == result.result_id );
    const myLog =  'Update Key Result Progress From ' + originalKeyResult.result_progress_status +  '% to ' + keyResult.result_progress_status +'%';

    this._settingKeyResultService.update(keyResult).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any> error,
      () => {
        if(this.tempData.status == 'success' && this.tempData.data){
          if(this.tempData.data.affectRows && this.tempData.data.affectRows > 0){

            if (!keyResult.result_progress_status){
              keyResult.result_progress_status = 0;
            }
            if(!originalKeyResult.result_progress_status){
              originalKeyResult.result_progress_status = 0;
            }

            this.updateKeyResultArray(keyResult);



            this.createActivity(
              'Update',
              myLog,
              progressUpdateDescription, // activity comment
              'k',
              keyResult.result_id
            ); // action: string, activityDetails: string, groupType: string, groupId: string

            this._alertService.displaySuccessMessage('Success Update Progress');


            this.updateOverallNumbers(this.goalInfo);
          }
        }
      }
    );


  }





  // Review Modals
  reviewObjectiveButton( reviewObjective: Objectiveclass){
    // console.log(reviewObjective);
    this.editObjective = new Objectiveclass();

    this.editObjective = reviewObjective;
    if(reviewObjective && reviewObjective.reviewed == 1 ){
      this.loadReviewInfo(reviewObjective);
    }else{
      this.reviewInfo = new Reviewclass();
      this.rate = this.reviewInfo.review_rating;
      this.reviewDescription =  this.reviewInfo.review_details;
    }

    this.reviewObjectiveModal.show();
  }

  loadReviewInfo(reviewObjective: Objectiveclass){
    if (reviewObjective.reviewed == 1) {
      this._reviewsService.getByObjectiveId(reviewObjective.objective_id).subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any> error,
        () => {
          if (this.tempData.status == 'success' && this.tempData.data) {
            this.reviewInfo = <Reviewclass>this.tempData.data;
            this.rate = this.reviewInfo.review_rating;
            this.reviewDescription =  this.reviewInfo.review_details;
          }
        }
      );
    }else{
      this.reviewInfo = new Reviewclass();
      this.rate = this.reviewInfo.review_rating;
      this.reviewDescription =  this.reviewInfo.review_details;
    }
  }

  closeReviewObjectiveButton(){
    this.reviewInfo = new Reviewclass();
    this.reviewObjectiveModal.hide();
  }

  saveReviewObjectiveButton( reviewObjective: Objectiveclass){



    if(reviewObjective ){
      if (reviewObjective.reviewed == 0) {
        if( !this.rate || this.rate == 0){
          this._alertService.displayWarningMessage('Pleaes rate this Objective');
        }else{

          this.reviewInfo = new Reviewclass();
          this.reviewInfo.review_details = this.reviewDescription;
          this.reviewInfo.review_rating = this.rate;
          this.reviewInfo.review_user_id = this.selfInfo.user_id;
          this.reviewInfo.review_object_type = 'o';
          this.reviewInfo.review_object_id = reviewObjective.objective_id;
          this.createNewReview(this.reviewInfo, reviewObjective);

        }



      }else{
        this.reviewInfo.review_rating = this.rate;
        this.reviewInfo.review_details = this.reviewDescription ;
        this.updateReviewInfo(this.reviewInfo, reviewObjective);
      }

    }

    this.reviewObjectiveModal.show();
  }


  public hoveringOver(value:number):void {
    this.overStar = value;
    this.percent = 100 * (value / this.max);
  };

  public resetStar():void {
    this.overStar = void 0;
  }


  createNewReview(newReviewInfo: Reviewclass, reviewObjective: Objectiveclass){
    this._reviewsService.addNewByClass(newReviewInfo).subscribe(
      data => this.tempData = data,
      error => this.errorMessage =<any>error,
      () => {
        if(this.tempData.status == 'success' && this.tempData.data) {
          this.updateObjectiveReviewStatus(reviewObjective);

        }else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }
      }
    );

  }
  updateReviewInfo(ReviewInfo: Reviewclass, reviewObjective: Objectiveclass){
    this._reviewsService.update(ReviewInfo).subscribe(
      data => this.tempData = data,
      error => this.errorMessage =<any>error,
      () => {
        if(this.tempData.status == 'success' && this.tempData.data) {
          this._alertService.displaySuccessMessage('Your Review Infomation has been updated. <br> affectRows: ' + this.tempData.data.affectRows);

          this.createActivity(
            'Updated',
            'Updated reviewed Info for Objective ' + reviewObjective.objective_name,
            '', // activity comment
            'o',
            reviewObjective.objective_id
          ); //action: string, activityDetails: string, groupType: string, groupId: string

        }else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }
        this.reviewObjectiveModal.hide();
      }
    );
  }

  updateObjectiveReviewStatus( objective: Objectiveclass ){
    objective.reviewed = 1;
    objective.objective_status = 'Complete';
    this._settingObjectiveService.update(objective).subscribe(
      data => this.tempData = data,
      error => this.errorMessage =<any>error,
      () => {
        if(this.tempData.status == 'success' && this.tempData.data) {
          this.createActivity(
            'Review',
            'Reviewed Objective ' + objective.objective_name,
            '', // activity comment
            'o',
            objective.objective_id
          ); //action: string, activityDetails: string, groupType: string, groupId: string
          this._alertService.displaySuccessMessage('Success!');

        }else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }
        this.reviewObjectiveModal.hide();
      }
    );

  }








// slider

  keyResultProgressOnChange(keyResult: Keyresultclass, event: any){
    //console.log(event);


    this.editKeyResult = new Keyresultclass();
    this.modalType = 'keyResultProgressUpdate';
    this.modalTitle = 'Progress update confirmation';
    this.editModeIO = 1;

    this.editKeyResult = keyResult;

//    console.log('change progress : ' + JSON.stringify(this.editKeyResult.result_id));


    this.updateProgressModal.show();

  }








  /*
   *  -------------------------------------- CRUD Operation ----------------------------------------------
   *
   *
   */


  createNewObjective(objectiveNameInput: string, objectiveDescription: string, selectedTeam:any) { // now I start use 2-way binding to process this

    // if(!this.viewTeamID){
    //   this.errorMessage("Internal error! Please refresh your page!");
    // }


    if (!objectiveNameInput || !objectiveDescription || !this.selectedGoal) {
      //alert("Do not leave any empty!");

      this._alertService.displayWarningMessage("Objective Name, Objective Description or Goal selector empty!");
      return;
    }
    else {
      const goalIds = this.selectedGoal;
      const objectiveStatusTag = this.selectedTag[0].text;
      const newObjective = new Objectiveclass();
      newObjective.objective_name = objectiveNameInput;
      newObjective.objective_description = objectiveDescription;
      newObjective.objective_progress_status = 0;
      newObjective.objective_status = objectiveStatusTag;

      let teamid = this.teams[0].team_id;
      let teamName = this.teams[0].team_name;

      if(selectedTeam){
        teamid = selectedTeam[0].id;
        teamName = selectedTeam[0].text;
      }



      this._settingObjectiveService.addNewByObjective(newObjective ).subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {
          if (this.tempData.status == 'success' && this.tempData.data) {
            let newObjective = <Objectiveclass>this.tempData.data;

            let checkStatus = 0;
            this.createGoalObjectiveRelationship(newObjective, goalIds);

            this.createTeamObjectiveRelationship(newObjective, teamid);

            newObjective.team_info.team_id = teamid;
            newObjective.team_info.team_name = teamName;


            newObjective.keyResult_array=[];
            const tempArray = [];

            tempArray.push(newObjective);
            let i = 0;
            for (i = 0; i < this.goalInfo.objective_array.length; i ++) {
              tempArray.push(this.goalInfo.objective_array[i]);
            }

            this.goalInfo.objective_array = tempArray;

            this._alertService.displaySuccessMessage( 'Your Team Objective create successfully' );
            // " Created a new Objective : " + newObjective.objective_name;
            this.createActivity(
              'Create',
              'Created a new Objective ' + newObjective.objective_name,
              '', // activity comment
              'o',
              newObjective.objective_id
            ); //action: string, activityDetails: string, groupType: string, groupId: string

            this.objectiveModal.hide();
            this.objectiveNameInputBoxValue = '';
            this.objectiveDescriptionInputBoxValue = '';
            // this.getTeamObjectiveByTimeFrameId(this.viewTeamID,this.currentTimeFrameId);
          }
          this.updateOverallNumbers(this.goalInfo);

        }
      );
    }


  }

  updateObjective(editObjective, objectiveNameInput: string, objectiveDescription: string, selectedTeam: any) {
    let modifyLog = '';

    if (!objectiveNameInput || !objectiveDescription||!this.selectedGoal) {
      this._alertService.displayWarningMessage("Objective Name or Objective Description empty!");
      return;
    } else {


      let goalIds = this.selectedGoal;
      let objectiveStatusTag = this.selectedTag[0].text;


      if (editObjective.objective_name != objectiveNameInput){

        modifyLog =  modifyLog +  "Change Objective name to: "+ objectiveNameInput+"; ";
      }
      if (editObjective.objective_description != objectiveDescription){


        modifyLog = modifyLog + " Change Objective description to: "+ objectiveDescription + "; ";
      }

      if (editObjective.objective_status != objectiveStatusTag){

        modifyLog =  modifyLog +  "Change Objective tag to: "+ objectiveStatusTag+"; ";
      }
      if (editObjective.team_info.team_name != this.selectedTeam[0].text){

        modifyLog =  modifyLog +  "Change Objective Team to: "+  this.selectedTeam[0].text +"; ";
      }

      let updateObjective = new Objectiveclass();
      updateObjective = editObjective;
      updateObjective.objective_description = objectiveDescription;
      updateObjective.objective_name = objectiveNameInput;

      // editObjective.object = timeFrameId;
      updateObjective.objective_status = objectiveStatusTag;


      this._settingObjectiveService.update(updateObjective)
        .subscribe(
          data => { this.tempData = data },
          error => this.errorMessage = <any>error,
          () => {



            if(this.tempData.status == 'success' && this.tempData.data)  {

              updateObjective.team_info.team_name = this.selectedTeam[0].text;

              //this.updateObjectiveTeams();
              this.updateTeamObjectiveRelationship(updateObjective, this.selectedTeam);



              this.createActivity(
                'Update',
                modifyLog,
                '', // activity comment
                'o',
                updateObjective.objective_id
              ); //action: string, activityDetails: string, groupType: string, groupId: string


              this._alertService.displaySuccessMessage('Your Objective has been updated.');




              this.objectiveNameInputBoxValue = '';
              this.objectiveDescriptionInputBoxValue = '';

              this.objectiveModal.hide();


            }else {

              this._alertService.displayErrorMessage(this.tempData.errorMessage);

            }
            this.updateOverallNumbers(this.goalInfo);
            this.editObjective = new Objectiveclass();


          }
        );

    }
  }


  createTeamObjectiveRelationship(objective: Objectiveclass, teamId: any ): any {
    this._settingObjectiveService.setTeamObjectives(objective, teamId).subscribe(
      data => { this.tempData = data },
      error => this.errorMessage = <any>error,
      () => {

        if (this.tempData.status == 'success' && this.tempData.data) {

          objective.team_info.team_id = teamId;
        } else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }

      }
    );
  }


  updateTeamObjectiveRelationship(objective: Objectiveclass, selectedTeams: any): any {
    let i = 0;
    const teamIdArray = [];

    if (selectedTeams){
      for ( i = 0; i < selectedTeams.length; i++){
        teamIdArray.push(selectedTeams[i].id);
      }
    }


    this._settingObjectiveService.updateTeamsObjectivesRelationship(objective, teamIdArray).subscribe(
      data => { this.tempData = data },
      error => this.errorMessage = <any>error,
      () => {

        if (this.tempData.status == 'success' && this.tempData.data) {

          // objective.team_info.team_id = teamId;
        } else {
          this._alertService.displayWarningMessage(this.tempData.errorMessage);
        }

      }
    );
  }

  createGoalObjectiveRelationship(objective: Objectiveclass, golasArray:any ): any{
    this._settingObjectiveService.setGoalsObjectives(objective, golasArray).subscribe(
      data => { this.tempData = data },
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData.status == 'success' && this.tempData.data) {

        } else {

        }
      }
    );
  }








  createNewKeyResult(keyResultNameInput: string, keyResultDescription: string, parentObjective: Objectiveclass, keyResultProgressStatus: number) {
    // now I start use 2-way binding to process this

    if (!keyResultNameInput || !keyResultDescription || !parentObjective || !this.parentObjective) {
      //alert("Do not leave any empty!");

      this._alertService.displayWarningMessage('Objective Name or Objective Description empty!');
      return;
    }
    else {
      if (!keyResultProgressStatus){
        keyResultProgressStatus = 0;
      }

      const newKeyResult = new Keyresultclass();
      newKeyResult.result_name = keyResultNameInput;
      newKeyResult.result_description = keyResultDescription;
      newKeyResult.result_progress_status = keyResultProgressStatus;
      newKeyResult.objective_id = parentObjective.objective_id;
      this._settingKeyResultService.addNewbyResult(newKeyResult).subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {

          if (this.tempData.status == 'success' && this.tempData.data) {
            const tempKeyResultInfo = <Keyresultclass>this.tempData.data;
            let i = 0;
            for ( i = 0; i < this.goalInfo.objective_array.length ; i ++  ){
              if (this.goalInfo.objective_array[i].objective_id == this.parentObjective.objective_id){


                newKeyResult.result_id = tempKeyResultInfo.result_id;
                //tempKeyResultInfo.result_progress_status = 0;
                this.keyResultsArray.push(newKeyResult); // add new key result to key result array in RAM .




                const tempArray = [];
                tempArray.push(tempKeyResultInfo);


                let j = 0;
                for (j = 0; j < this.goalInfo.objective_array[i].keyResult_array.length; j ++) {
                  tempArray.push(this.goalInfo.objective_array[i].keyResult_array[j]);
                }
                //this.goalInfo.objective_array[i].keyResult_array.push(tempKeyResultInfo);

                this.goalInfo.objective_array[i].keyResult_array = tempArray;
                break;

              }
            }


            this.createActivity(
              'Create',
              'Created a new Key Result ' + tempKeyResultInfo.result_name,
              '', // activity comment
              'k',  // 'k' stands for key result in database
              tempKeyResultInfo.result_id
            ); //action: string, activityDetails: string, groupType: string, groupId: string


            this.keyResultDescriptionInputBoxValue = '';
            this.keyResultNameInputBoxValue = '';
            this.editModeIO = 0;
            this.modalTitle = '';
            this.modalType = '' ;

            // this.calculateObjectivesProgress();
            this.updateOverallNumbers(this.goalInfo);
            this.keyResultModal.hide();
            this.parentObjective = new Objectiveclass();
            this.editKeyResult = new Keyresultclass();
            // this.ngOnInit();

          } else {
            this._alertService.displayErrorMessage(this.tempData.errorMessage);
          }
        }
      );


    }


  }



  updateKeyResult(editKeyResult: Keyresultclass, keyResultNameInput: string, keyResultDescription: string, keyResultProgressValue: number) {


    let originalKeyResult =  new Keyresultclass();
    originalKeyResult = this.keyResultsArray.find( result => editKeyResult.result_id == result.result_id);

    let modifyLog = '';

    if(!keyResultProgressValue){
      keyResultProgressValue = 0;
    }

    if (!keyResultNameInput || !keyResultDescription ||!this.parentObjective ||!this.editKeyResult) {

      this._alertService.displayWarningMessage("Objective Name or Objective Description empty!");

      return;
    } else {

      if (originalKeyResult.result_name != keyResultNameInput){
        modifyLog = modifyLog + 'Change Key Result name From '+ originalKeyResult.result_name +' to: ' + keyResultNameInput  + ';  ' ;
      }
      if (originalKeyResult.result_description != keyResultDescription){
        modifyLog = modifyLog + ' Change Key Result description to: ' + keyResultDescription + ';  ' ;
      }


      editKeyResult.result_description = keyResultDescription;
      editKeyResult.result_name = keyResultNameInput;
      editKeyResult.result_progress_status = keyResultProgressValue;

      this._settingKeyResultService.update(editKeyResult)
        .subscribe(
          data => { this.tempData = data },
          error => this.errorMessage = <any>error,
          () => {
            if(this.tempData.status == 'success' && this.tempData.data)  {
              this.updateKeyResultArray(editKeyResult);

              this.createActivity(
                'Update',
                modifyLog,
                '', // activity comment
                'k',
                editKeyResult.result_id
              ); //action: string, activityDetails: string, groupType: string, groupId: string

              this._alertService.displaySuccessMessage( 'Your key Result has been updated. <br /> affectRows: ' + this.tempData.data.affectRows);

              this.objectiveNameInputBoxValue = '';
              this.objectiveDescriptionInputBoxValue = '';


            }else {

              this._alertService.displayErrorMessage( this.tempData.errorMessage);
              //  this.displayErrorMessage(this.tempData.errorMessage);
            }
            this.updateOverallNumbers(this.goalInfo);
          }
        );



    }

    this.keyResultModal.hide();

  }


  updateKeyResultArray(editKeyResult: Keyresultclass){
    let i = 0;
    if (this.keyResultsArray){

      while ( i < this.keyResultsArray.length ){
        if ( this.keyResultsArray[i].result_id == editKeyResult.result_id ){
          this.keyResultsArray[i].result_progress_status = Number(editKeyResult.result_progress_status);
          this.keyResultsArray[i].result_description =  editKeyResult.result_description;
          this.keyResultsArray[i].result_target =  editKeyResult.result_target;
          this.keyResultsArray[i].result_unit =  editKeyResult.result_unit;
          this.keyResultsArray[i].result_name =  editKeyResult.result_name;


          // console.log( this.keyResultsArray[i]);
          break;
        }else {
          i ++;
        }
      }

    }





  }

  setCurrentTeamObjectivesRelationship(objective: Objectiveclass){


    let currentObjective = new Objectiveclass();
    currentObjective = this.goalInfo.objective_array.find( current => objective == current);

    if (!currentObjective.team_info.team_id || currentObjective.team_info.team_id == undefined ){
      this.selectedTeam = [{ id: this.teams[0].team_id.toString(), text: this.teams[0].team_name }];

      //this.selectedTag = [{ id: 'None', text: 'None'}];

    }else{
      this.selectedTeam = [];
      this.selectedTeam = [{ id: currentObjective.team_info.team_id.toString(), text: currentObjective.team_info.team_name  }];
    }
  }









  /*
   * ----------------------- Modal Info -------------------------------------
   *
   * */


  getAllGoals() {
    this._settingGoalService.getAll()
      .subscribe(
        data => this.tempData = data,
        error => this.errorMessage = <any>error,
        () => {
          if (this.tempData.status == 'success' && this.tempData.data) {
            this.goals = <Goalclass[]> this.tempData.data;

            this.setGoalsDropDownList(this.goals);
            //this.goals.sort();
          }
        }
      );
  }

  getAllKeyResult(){
    this.keyResultsArray =  new Array<Keyresultclass>();
    this._settingKeyResultService.getAll().subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any> error,
      () => {
        if (this.tempData.status == 'success' && this.tempData.data) {
          this.keyResultsArray = <Keyresultclass[]> this.tempData.data;

        }

      }
    );

  }

  setCurrentGoal(goalInfo) {
    if(goalInfo) {
      this.selectedGoal = [];
      this.selectedGoal.push(goalInfo.goal_id.toString());

    }
  }



  setGoalsDropDownList(goals: Goalclass[]) {
    let tempArray = [];
    let i = 0;
    for (i = 0; i < goals.length;i++){

      if(goals[i].goal_status != 'Complete' ){
        const info = {id: goals[i].goal_id.toString(), name: goals[i].goal_name};
        tempArray.push(info);
      }
    }
    this.goalsDropDownListOptions = tempArray;
  }

  getTeamsInfo () {
    this.teams = new Array <Teamclass>();
    this._settingTeamService.getAll().subscribe(
      data => this.tempData = data,
      error =>  this.errorMessage =  <any>error,
      () =>{
        if (this.tempData.status.toLowerCase() == 'success' && this.tempData.data) {
          this.teams = <Teamclass[]> this.tempData.data;
        }
        this.setTeamDropDownOptions(this.teams);
      }
    );
  }

  setTeamDropDownOptions (teams: Teamclass[]) {
    const tempArray = [];
    let i = 0;
    if (teams) {
      for (i = 0; i < teams.length; i ++){
        const info = {id: teams[i].team_id.toString(), text: teams[i].team_name};
        //const info = {value: teams[i].team_id.toString(), label: teams[i].team_name};
        tempArray.push(info);
      }
      this.teamDropDownListOptions = tempArray;

    }

  }













  /*
   * --------------------------- activities submission ---------------------------------------------------
   *
   * */


  createActivity(action: string, activityDetails: string, activityComment: string, groupType: string, groupId: number){

    const submitANewActivity = new Activityclass();
    submitANewActivity.user_id =  this.selfInfo.user_id;
    submitANewActivity.activity_comment =  activityComment;
    submitANewActivity.activity_detail = activityDetails; // "Created a new Objective : " + newObjective.objective_name;
    submitANewActivity.activity_type = action;
    submitANewActivity.activity_group = groupType;
    submitANewActivity.activity_group_id = groupId;
    this.submitActivity(submitANewActivity);

  }


  submitActivity(activity: any) {
    this._okrActivitiesService.addNewByClass(activity).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if(this.tempData.data && this.tempData && <Activityclass>this.tempData.data){
          // swal("Success!", "Your goal has been created.", 'success');

        }
      }
    );
  }





  public showActivitiesModal(keyResult): void {
    this.activitiesLoading = true;
    this.activitiesInfo = new Array<Activityclass>();
    this._okrActivitiesService.getByKeyResultId(keyResult).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData && this.tempData.status.toLowerCase() == 'success'){
          this.activitiesInfo = <Activityclass[]> this.tempData.data;
        }
        this.activitiesLoading = false;
      }
    );
    this.activitiesModal.show();
  }

  public hideActivitiesModal(): void {
    this.activitiesModal.hide();
  }


}
