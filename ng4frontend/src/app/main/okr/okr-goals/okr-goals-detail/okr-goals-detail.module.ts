import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// 3rd-party library

import {SharedModulesRegisterModule} from '../../../../shared/shared-modules-register.module';


import { OkrGoalsDetailRoutingModule } from './okr-goals-detail-routing.module';
import { OkrGoalsDetailComponent } from './okr-goals-detail.component';
import { GoalOkrsComponent } from './goal-okrs/goal-okrs.component';
import { GoalActivitiesComponent } from './goal-activities/goal-activities.component';
import { GoalTeamsComponent } from './goal-teams/goal-teams.component';

import {ShareGoalDetailsService} from './share-goal-details.service';

import {PopupActivitiesComponent} from './popup-activities/popup-activities.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,

    // 3rd-party

    SharedModulesRegisterModule,


    OkrGoalsDetailRoutingModule
  ],
  providers: [ShareGoalDetailsService],
  declarations: [
    OkrGoalsDetailComponent,
    GoalOkrsComponent,
    GoalActivitiesComponent,
    GoalTeamsComponent,

    PopupActivitiesComponent,
  ]
})
export class OkrGoalsDetailModule { }
