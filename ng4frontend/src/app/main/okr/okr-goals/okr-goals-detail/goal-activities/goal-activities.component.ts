import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
// Observable class extensions
import 'rxjs/add/observable/of';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { Subscription } from 'rxjs/Subscription';


// 3rd-party libraries


import {UserInfoContainerService} from '../../../../../shared/services/user-info-container.service';

// activity data service. submit 'create' and 'update'
import {OkrActivitiesService} from '../../../okr-shared/services/okr-activities.service';
import {AlertService} from '../../../../../shared/services/alert.service';


// classes here
import {Activityclass} from '../../../okr-shared/classes/activitie-class';
import {Userclass} from '../../../../../shared/classes/user-class';


@Component({
  selector: 'app-goal-activities',
  templateUrl: './goal-activities.component.html',
  providers: [ OkrActivitiesService ],
  styleUrls: ['./goal-activities.component.css']
})
export class GoalActivitiesComponent implements OnInit {
  public isLoading: boolean;
  public goalIdSubscription: Subscription;
  public goalId: any;
  public activitiesInfo: Activityclass[];


  public tempData: any;
  public errorMessage: any;




  constructor(
    private _alertService: AlertService,
    private _userInfoContainerService: UserInfoContainerService,
    private _okrActivitiesService: OkrActivitiesService,
    private _activatedRoute: ActivatedRoute,

  ){

    this.activitiesInfo = new Array<Activityclass>();
    this.isLoading = true;


  }

  ngOnInit() {
    this.loadActivitiesInfo();
  }
  ngOnDestroy() {
    this.goalIdSubscription.unsubscribe();
  }



  loadActivitiesInfo(){
      this.subsTargetGoalId();

  }

  subsTargetGoalId(){
    this.goalIdSubscription = this._activatedRoute.params.subscribe(params => {
      this.goalId = '' + params['goalId']; // (+) converts string 'id' to a number
      this.getActivities( this.goalId);
    });
  }

  getActivities(goalId){
    this.activitiesInfo = new Array<Activityclass>();
    this.isLoading = true;

    this._okrActivitiesService.getByGoalId(goalId).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData && this.tempData.status.toLowerCase() == 'success'){
          this.activitiesInfo = <Activityclass[]> this.tempData.data;
        }
        this.isLoading = false;
      }
    );
  }


}
