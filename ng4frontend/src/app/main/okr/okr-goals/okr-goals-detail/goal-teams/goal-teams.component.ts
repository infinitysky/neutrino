import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//import 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
// Observable class extensions
import 'rxjs/add/observable/of';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { Subscription } from 'rxjs/Subscription';


// 3rd-party libraries
import { ModalDirective } from 'ngx-bootstrap/modal';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { NouisliderModule } from 'ng2-nouislider';


import {UserDetailsService} from '../../../../../shared/services/user-details.service';
import {ShareGoalDetailsService} from '../share-goal-details.service';
import {UserInfoContainerService} from '../../../../../shared/services/user-info-container.service';

import {PopupActivitiesComponent} from '../popup-activities/popup-activities.component';



// goal data service
import { SettingGoalService } from '../../../okr-shared/services/okr-goal.service';
// time frame data service
import { SettingTimeFrameService } from '../../../okr-shared/services/okr-time-frame.service';
// team data service
import { SettingTeamService } from '../../../okr-shared/services/okr-team.service';
// objective data service
import { SettingObjectiveService } from '../../../okr-shared/services/okr-objective.service';
// key result data service
import { SettingKeyResultService } from '../../../okr-shared/services/okr-key-result.service';
// activity data service. submit 'create' and 'update'
import {OkrActivitiesService} from '../../../okr-shared/services/okr-activities.service';
import {AlertService} from '../../../../../shared/services/alert.service';
import {OverallCalculatorService} from '../../../okr-shared/services/overall-calculator.service';

// classes here

import { Teamclass } from '../../../okr-shared/classes/team-class';
import { Goalclass } from '../../../okr-shared/classes/goal-class';
import { Objectiveclass } from '../../../okr-shared/classes/objective-class';
import {Keyresultclass} from '../../../okr-shared/classes/key-restult-class';
import {Activityclass} from '../../../okr-shared/classes/activitie-class';
import {Userclass} from '../../../../../shared/classes/user-class';



@Component({
  selector: 'app-goal-teams',
  templateUrl: './goal-teams.component.html',
  providers: [
    SettingGoalService,
    SettingTimeFrameService,
    SettingTeamService,
    SettingObjectiveService,
    SettingKeyResultService,
    OkrActivitiesService,
    AlertService,
    UserDetailsService,
    OverallCalculatorService,


  ],
  styleUrls: ['./goal-teams.component.css']
})
export class GoalTeamsComponent implements OnInit {
  public goalId: string;
  public goalIdSubscription: Subscription;
  public isLoading: boolean;
  public teamsInfo: Teamclass[];
  public errorMessage: any;
  public tempData: any;

  public timeFrameIdSubscription: Subscription;
  public currentTimeFrameId: number;
  /*---------------------------------  Start ------------------------------------------------
   * */
  constructor(private _settingTeamService: SettingTeamService,
              private _alertService: AlertService,
              private _userInfoContainerService: UserInfoContainerService,
              private _shareGoalDetailsService: ShareGoalDetailsService,
              private _activatedRoute: ActivatedRoute,
              private _settingGoalService: SettingGoalService,
              private _userDetailsService: UserDetailsService,) {

    this.teamsInfo = new Array<Teamclass>();
    this.isLoading = true;
    this.goalId = '';
    this.errorMessage = '';
    this.tempData = '';
    this.currentTimeFrameId = 0;
    this.timeFrameIdSubscription = new Subscription();

  }

  ngOnInit() {

    this.loadTeamsInfo();
  }

  ngOnDestroy() {
    this.timeFrameIdSubscription.unsubscribe();
    this.goalIdSubscription.unsubscribe();

  }

  loadTeamsInfo() {
    this.subsTargetGoalId();
    this.timeFrameIdParameterSubscribe();
  }

  subsTargetGoalId() {
    this.goalIdSubscription = this._activatedRoute.params.subscribe(params => {
      this.goalId = '' + params['goalId']; // (+) converts string 'id' to a number
      this.getTeamsInfoByGoalId(this.goalId);
    });
  }



  timeFrameIdParameterSubscribe(){

    this.timeFrameIdSubscription = this._activatedRoute.queryParams.subscribe(params => {
      this.currentTimeFrameId =  +params['timeFrameId'] || 0;
      if( 0 == this.currentTimeFrameId ){
        this.timeFrameIdSubscription.unsubscribe();
        this.timeFrameIdSubscription = this._userInfoContainerService.timeFrame$.subscribe(
          timeFrame => this.currentTimeFrameId = timeFrame.time_frame_id
        );
      }
    });

  }



  getTeamsInfoByGoalId(goalId: any) {
    this.teamsInfo = new Array<Teamclass>();
    this.isLoading = true;
    this._settingTeamService.getByGoalId(goalId).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData) {
          this.isLoading = false;
          if (this.tempData.status.toLowerCase() == 'success' && this.tempData.data) {
            this.teamsInfo = <Teamclass[]> this.tempData.data; //  the backend API will return the data as array
            this.teamsInfo = this.ranking(this.teamsInfo);
            //console.log(this.teamsInfo);
          }
        }
      }
    );
  }

  ranking(teamsInfo: Teamclass[]): Teamclass[] {
    let i = 0;
    let switched = 1;
    if (teamsInfo) {

      while (switched != 0) {
        switched = 0;
        i = 0;

        //loop through all numbers in the array.
        while (i < teamsInfo.length - 1) {

          if(!teamsInfo[i].team_progress_status){
            teamsInfo[i].team_progress_status = 0;
          }
          //compare the two values.
          if (Number(teamsInfo[i].team_progress_status) < Number(teamsInfo[i + 1].team_progress_status)) {

            const swap = teamsInfo[i];
            teamsInfo[i] = teamsInfo[i + 1];
            teamsInfo[i + 1] = swap;
            switched = 1;
          }
          //increment
          i++;
        }

      }

      return teamsInfo;
    }

  }








}
