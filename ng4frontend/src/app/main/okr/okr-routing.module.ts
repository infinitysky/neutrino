import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo: 'okr-company', pathMatch: 'full'},
  { path: 'okr-setting', loadChildren: './okr-setting/okr-setting.module#OkrSettingModule' },
  { path: 'okr-users', loadChildren: './okr-users/okr-users.module#OkrUsersModule' },
  { path: 'okr-teams', loadChildren: './okr-teams/okr-teams.module#OkrTeamsModule' },
  { path: 'okr-goals', loadChildren: './okr-goals/okr-goals.module#OkrGoalsModule' },
  { path: 'okr-company', loadChildren: './okr-company/okr-company.module#OkrCompanyModule' },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class OkrRoutingModule { }
