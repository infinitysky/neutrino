import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { SharedModulesRegisterModule } from '../../shared/shared-modules-register.module';



import { OkrSettingModule } from './okr-setting/okr-setting.module'
import { OkrRoutingModule } from './okr-routing.module';
import { OkrComponent } from './okr.component';



@NgModule({
  imports: [
    FormsModule,
    CommonModule,

    SharedModulesRegisterModule,
    OkrRoutingModule,
    OkrSettingModule
  ],
  declarations: [OkrComponent]
})
export class OkrModule { }
