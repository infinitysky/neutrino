import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


import { Subscription } from 'rxjs/Subscription';

import {SettingTeamService} from '../okr-shared/services/okr-team.service';
import {UserInfoContainerService} from '../../../shared/services/user-info-container.service';
import {UserDetailsService} from '../../../shared/services/user-details.service';

import {Teamclass} from '../okr-shared/classes/team-class';
import {Userclass} from '../../../shared/classes/user-class';


@Component({
  selector: 'app-okr-teams',
  templateUrl: './okr-teams.component.html',
  providers:[SettingTeamService,UserDetailsService],
  styleUrls: ['./okr-teams.component.css']
})
export class OkrTeamsComponent implements OnInit {


  public teams: Teamclass[];
  public users: Userclass[];

  private tempData: any;
  private errorMessage: any;

  public teamLength: number;
  private currentTimeFrameId: any;
  private timeFrameIdSubscription: any;

  private userInfoSubscription: Subscription;
  private selfInfo: Userclass;
  public isAdmin: boolean;



  constructor(
    private _userInfoContainerService: UserInfoContainerService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _settingTeamService: SettingTeamService,
  ){
    this.teams = new Array<Teamclass>();
    this.users = [];
    this.teamLength = 0;

  }
  ngOnInit(){
    this.timeFrameIdParameterSubscribe();
    this.getSelfInfo();


  }
  ngOnDestroy(){
    this.timeFrameIdSubscription.unsubscribe();
    this.userInfoSubscription.unsubscribe();
  }



  getSelfInfo() {
    this.selfInfo = new Userclass();
    this.userInfoSubscription = this._userInfoContainerService.userInfo$.subscribe( userData => this.selfInfo = userData );
    if( this.selfInfo){
      if( this.selfInfo.role == 'admin'){
        this.isAdmin = true;

      }
    }else{
      this.selfInfo = new Userclass();
    }

  }


  timeFrameIdParameterSubscribe(){

    this.timeFrameIdSubscription = this._activatedRoute.queryParams.subscribe(params => {
      this.currentTimeFrameId =  +params['timeFrameId'] || 0;
      // console.log('Query param currentTimeFrame: ', this.currentTimeFrameId);
      this.getTeamsByTimeFrame(this.currentTimeFrameId );
    });

  }



  getTeamsByTimeFrame(timeFrameId){

    this.teams = new Array<Teamclass>();
    this._settingTeamService.getCurrentTeamProgressAndMember(timeFrameId)
      .subscribe(
        data => this.tempData = data,
        error =>  this.errorMessage = <any>error,
        () => {
          // console.log( "this.TeamsData + "+JSON.stringify(this.TeamsData.data));
          if(this.tempData && this.tempData.data ){

            this.teams = <Teamclass[]>this.tempData.data;
            this.teamLength = this.teams.length;
          }

        }
      );

  }


  getTeams() {
    this.teams = new Array<Teamclass>();
    console.log("get All teams");
    this._settingTeamService.getAllTeamProgressAndMember()
      .subscribe(
        data => this.tempData = data,
        error =>  this.errorMessage = <any>error,
        () => {
          // console.log( "this.TeamsData + "+JSON.stringify(this.TeamsData.data));
          if(this.tempData && this.tempData.data ){

            this.teams = <Teamclass[]>this.tempData.data;
            this.teamLength = this.teams.length;
          }

        }
      );

  }


}
