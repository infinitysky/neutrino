import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { OkrTeamsDetailRoutingModule } from './okr-teams-detail-routing.module';
import { OkrTeamsDetailComponent } from './okr-teams-detail.component';



//3rd party library module

import {SharedModulesRegisterModule} from '../../../../shared/shared-modules-register.module';


import {OkrTeamsMembersComponent} from '../okr-teams-members/okr-teams-members.component';
import {OkrTeamsActivityComponent} from './okr-teams-activity/okr-teams-activity.component';
import {OkrTeamsOkrComponent} from './okr-teams-okr/okr-teams-okr.component';
import { ShareTeamsOkrinfoService } from '../share-teams-okrinfo.service';

@NgModule({
  imports: [
    SharedModulesRegisterModule,
    FormsModule,
    HttpModule,
    CommonModule,
    OkrTeamsDetailRoutingModule
  ],
  providers:[ShareTeamsOkrinfoService],
  declarations: [OkrTeamsDetailComponent, OkrTeamsMembersComponent, OkrTeamsActivityComponent, OkrTeamsOkrComponent ]
})
export class OkrTeamsDetailModule { }
