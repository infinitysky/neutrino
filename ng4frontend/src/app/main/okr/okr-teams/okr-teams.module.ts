import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { OkrTeamsRoutingModule } from './okr-teams-routing.module';
import { OkrTeamsComponent } from './okr-teams.component';
import {OkrTeamsDetailModule} from './okr-teams-detail/okr-teams-detail.module';

// 3rd party library
import {SharedModulesRegisterModule} from '../../../shared/shared-modules-register.module';



@NgModule({
  imports: [


    CommonModule,

    SharedModulesRegisterModule,
    OkrTeamsDetailModule,
    OkrTeamsRoutingModule
  ],
  declarations: [OkrTeamsComponent ]
})
export class OkrTeamsModule { }
