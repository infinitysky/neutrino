import { Component, OnInit,ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import {Subscription} from 'rxjs/Subscription';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';

import {SettingTeamService} from '../../okr-shared/services/okr-team.service';
import {UserDetailsService} from '../../../../shared/services/user-details.service';
import {AlertService} from '../../../../shared/services/alert.service';
import {UserInfoContainerService} from '../../../../shared/services/user-info-container.service';


import {Teamclass} from '../../okr-shared/classes/team-class';
import {Userclass} from '../../../../shared/classes/user-class';

@Component({
  selector: 'app-okr-teams-members',
  templateUrl: './okr-teams-members.component.html',
  providers:[SettingTeamService, UserDetailsService, AlertService],
  styleUrls: ['./okr-teams-members.component.css']
})
export class OkrTeamsMembersComponent implements OnInit {

  public isLoaded = false;

  public teams: Teamclass[];
  public users: Userclass[];
  public teamMembers: Userclass[];
  public  viewTeamId: any;
  public tempData: any;
  public errorMessage: any;

  public subscribeTeamInfo: any;



  public membersDropdownListOptions: IMultiSelectOption[];
  public teamLeadersDropdownOptions: Array<any>;
  public teamDropdownListOptions: Array<any>;


  public memberSelectedOptions: any[]; // Default selection
  public teamLeaderSelectedOptions: any[];
  public parentTeamSelectedOptions: any[];


  public originalMembers: any[];


  public timeFrameIdSubscription: Subscription;
  public currentTimeFrameId: number;


  public teamMemberSelectorSettings: IMultiSelectSettings = {
    pullRight: false,
    enableSearch: true,
    checkedStyle: 'checkboxes',
    buttonClasses: 'btn btn-default',
    selectionLimit: 0,
    closeOnSelect: false,
    showCheckAll: true,
    showUncheckAll: true,
    dynamicTitleMaxItems: 0,
    maxHeight: '350px',
  };

  public myTexts: IMultiSelectTexts = {
    checkAll: 'Check all',
    uncheckAll: 'Uncheck all',
    checked: 'checked',
    checkedPlural: 'checked',
    searchPlaceholder: 'Search...',
    defaultTitle: 'Select',
  };

  @ViewChild('teamMembersModal') public teamMembersModal: ModalDirective;


  constructor(
    private _alertService: AlertService,
    private _userInfoContainerService: UserInfoContainerService,
    private _userDetailsService: UserDetailsService,
    private _settingTeamService: SettingTeamService,
    private _activatedRoute: ActivatedRoute
  ) {
    this.teams=[];
    this.teamMembers=[];
    this.users = [];
    this.viewTeamId = null;
  }

  ngOnInit() {
   this.getTeamMemberInfo();

  }


  ngOnDestroy() {
    this.subscribeTeamInfo.unsubscribe();
    this.timeFrameIdSubscription.unsubscribe();
  }



  getTeamMemberInfo(){

    this.timeFrameIdParameterSubscribe();
    this.getAllUsersInfo();

    this.viewTeamId = this._activatedRoute.snapshot.params['teamid'];
    this.subscribeTeamInfo = this._activatedRoute.params.subscribe(
      params => {
        this.viewTeamId = '' + params['teamid']; // (+) converts string 'id' to a number
        this.getTeamMembers( this.viewTeamId);
      });
  }

  timeFrameIdParameterSubscribe(){

    this.timeFrameIdSubscription = this._activatedRoute.queryParams.subscribe(params => {
      this.currentTimeFrameId =  +params['timeFrameId'] || 0;
      if( 0 == this.currentTimeFrameId ){
        this.timeFrameIdSubscription.unsubscribe();
        this.timeFrameIdSubscription = this._userInfoContainerService.timeFrame$.subscribe(
          timeFrame => this.currentTimeFrameId = timeFrame.time_frame_id
        );
      }
    });

  }



  getTeamMembers(teamId: any){
    this._settingTeamService.getTeamMembersByTeamId(teamId).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if(<Userclass[]>this.tempData.data){
          this.teamMembers = new Array<Userclass>();
          this.teamMembers = <Userclass[]>this.tempData.data;

          let membersDetails = this.tempData.data;
          let i = 0;
          let tempselectedMemberArray = [];
          for (i = 0; i < membersDetails.length; i++) {

            let fullName = membersDetails[i].first_name + "" + membersDetails[i].last_name;
            //for multi select dropdown list
            let tempInfo = membersDetails[i].user_id;

            tempselectedMemberArray.push(tempInfo);

          }
          this.memberSelectedOptions = tempselectedMemberArray;


          //for check delete and insert users
          this.originalMembers = tempselectedMemberArray;

        }
      }
    );
  }

  closeTeamMembersModalButton(){
    this.teamMembersModal.hide();
  }




  modalSaveChangeButton() {
    const editTeam = new Teamclass();
    editTeam.team_id = this.viewTeamId;
    this.updateTeamMembers(editTeam, this.memberSelectedOptions);

  }

  manageTeamMemberButton(){
    this.getAllUsersInfo();
    this.teamMembersModal.show();

  }
  updateTeamMembers(team: Teamclass, selectedMember) {
    this._settingTeamService.updateTeamMember(team, selectedMember).subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData.status = "success" && this.tempData.data) {
          this._alertService.displaySuccessMessage('Your team has been updated.');
          this.getTeamMemberInfo();
          this.teamMembersModal.hide();
        } else {
          this._alertService.displayErrorMessage(this.tempData.errorMessage);
        }
      }
    );
  }


  getAllUsersInfo() {
    //console.log("get All users");

    this._userDetailsService.getAll().subscribe(
      data => this.tempData = data,
      error => this.errorMessage = <any>error,
      () => {
        if (this.tempData.data && this.tempData.status == "success") {
          this.users =new Array<Userclass>();

          this.users = <Userclass[]>this.tempData.data;
          this.setUserAndMembersDropdownList(this.users);
        }
        // console.log(this.users);
      },
    );

  }


  setUserAndMembersDropdownList(usersInfoList: Userclass[]) {
    let i = 0;
    let memberArray = [];
    for (i = 0; i < usersInfoList.length; i++) {
      const fullName = usersInfoList[i].first_name + " - " + usersInfoList[i].last_name;
      //for multi select dropdown list
      const tempInfo = { id: usersInfoList[i].user_id, name: fullName };
      //for single select list
      //this.membersDropdownListOptions.push(tempInfo);
      memberArray.push(tempInfo);
    }
    // This way is working...
    this.membersDropdownListOptions = memberArray;
  }



}
