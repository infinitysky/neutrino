export class Reviewclass {
  review_id: number;
  review_rating; number;
  review_details: string;
  review_user_id: number;
  review_object_type: string;
  review_object_id: number;

  review_user_first_name: string;
  review_user_last_name: string;
}
