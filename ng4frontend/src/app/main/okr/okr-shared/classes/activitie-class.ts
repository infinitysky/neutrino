export class Activityclass {

  activity_id: number;
  activity_detail: string;
  activity_comment: string;
  activity_type: string;
  user_id: number;
  activity_timestamp: Date;

  activity_group: string;
  activity_group_id: number;
  receiver_user_id: number;

  first_name: string;
  last_name: string;
  receiver_first_name: string;
  receiver_last_name: string;


}
