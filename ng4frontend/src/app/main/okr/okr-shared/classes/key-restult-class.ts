export class Keyresultclass {
  result_id: number;
  result_name: string;
  result_description: string;
  result_unit: string;
  result_status: string;
  result_progress_status: number;
  result_target: string;
  objective_id: number;
}
