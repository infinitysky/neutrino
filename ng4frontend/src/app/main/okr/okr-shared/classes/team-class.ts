export class Teamclass {

    team_id: number;
    team_description: string;
    team_name: string;
    parent_team_id: number;
    team_leader_user_id: number;
    first_name: string;
    last_name: string;

    team_progress_status: number;
    teamMember_array: any;
    objective_array: any;




}
