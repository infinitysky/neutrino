export class Objectiveclass {

  objective_id: number;
  objective_name: string;
  objective_description: string;
  objective_unit: string;
  objective_status: string;
  objective_progress_status: number;
  team_leader_user_id: any;

  reviewed: number;
  objective_target: string;

  keyResult_array: any;
  goals_id_array: any;

  team_info: any;
  team_members: any;

  // changed structure after I discussed with Nik.
  // The objectives should 1 to 1 with Teams
  // team_id_array: any;

}
