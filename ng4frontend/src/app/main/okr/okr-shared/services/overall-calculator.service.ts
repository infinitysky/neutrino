import { Injectable } from '@angular/core';

import {Keyresultclass} from '../classes/key-restult-class';
import {Objectiveclass} from '../classes/objective-class';
import {Goalclass} from '../classes/goal-class';

@Injectable()
export class OverallCalculatorService {

  constructor() { }

  objectiveProgressRecalculate(objective: Objectiveclass): Objectiveclass{
    let i = 0;
    let tempKeyResultProgressValue = 0;
    if (objective.keyResult_array){
      if (objective.keyResult_array.length == 0){
        objective.objective_progress_status = 0;
      }else{
        for (i = 0; i < objective.keyResult_array.length; i++){
          tempKeyResultProgressValue = Number(tempKeyResultProgressValue) + Number(objective.keyResult_array[i].result_progress_status);
        }
        objective.objective_progress_status = Number(tempKeyResultProgressValue) / objective.keyResult_array.length;
      }
    }else{
      objective.objective_progress_status = 0;
    }
    return objective;
  }

  objectivesArrayProgressRecalculate(objectivesArray: Objectiveclass[]): Objectiveclass[]{
    let i = 0;
    if (objectivesArray){
      for ( i = 0; i < objectivesArray.length; i++){
        objectivesArray[i] =  this.objectiveProgressRecalculate(objectivesArray[i]);
      }
    }
    return objectivesArray;
  }

  goalProgressRecalculate(goal: Goalclass): Goalclass{
    let i = 0;
    let tempObjectivesProgressValue = 0;

    if (goal){

      if(goal.objective_array){
        goal.objective_array = this.objectivesArrayProgressRecalculate(goal.objective_array);
        for (i =0; i < goal.objective_array.length; i++ ){
          tempObjectivesProgressValue = Number(tempObjectivesProgressValue) + Number(goal.objective_array[i].objective_progress_status);
        }
        goal.goal_progress_status = Number(tempObjectivesProgressValue) / goal.objective_array.length;

      }else{
        goal.goal_progress_status = 0;
      }

    }
    return goal;
  }

  goalsArrayProgressRecalculate(goalsArray: Goalclass[]): Goalclass[]{
    let i = 0;
    if (goalsArray){
      for ( i = 0; i < goalsArray.length; i++){
        goalsArray[i] =  this.goalProgressRecalculate(goalsArray[i]);
      }
    }
    return goalsArray;
  }




}
