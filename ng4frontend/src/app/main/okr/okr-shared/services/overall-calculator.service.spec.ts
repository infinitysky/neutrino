import { TestBed, inject } from '@angular/core/testing';

import { OverallCalculatorService } from './overall-calculator.service';

describe('OverallCalculatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OverallCalculatorService]
    });
  });

  it('should be created', inject([OverallCalculatorService], (service: OverallCalculatorService) => {
    expect(service).toBeTruthy();
  }));
});
