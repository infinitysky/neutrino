import {Injectable} from '@angular/core';


import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';


import {MY_CONFIG} from '../../../../app-config';

import { Reviewclass } from '../classes/review-class';

@Injectable()
export class ReviewsService {


  private basicAPI = MY_CONFIG.apiEndpoint + MY_CONFIG.apiPath +  MY_CONFIG.reviewsUrl;
  private createAPI = this.basicAPI + '/create';
  private operateAPI = this.basicAPI + '/items';

  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private http: Http) { }

  private handleErrorObservable (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }



  getAll(): Observable<Reviewclass[]> {
    return this.http.get(this.basicAPI)
      .map(res => res.json())
      .catch(this.handleErrorObservable);
  }

  get(reviewObject: Reviewclass): Observable<Reviewclass> {
    const url = `${this.operateAPI}/${reviewObject.review_id}`;
    return this.http.get(url, this.headers)
      .map(res => res.json())
      .catch(this.handleErrorObservable);
  }

  getByGoalId(goalId: any): Observable<Reviewclass> {

    const url = `${this.basicAPI}/get_by_goal_id/${goalId}`;

    return this.http.get(url, this.headers)
      .map(res => res.json())
      .catch(this.handleErrorObservable);
  }



  getByObjectiveId(objectiveId: any): Observable<Reviewclass> {

    const url = `${this.basicAPI}/get_by_objective_id/${objectiveId}`;

    return this.http.get(url, this.headers)
      .map(res => res.json())
      .catch(this.handleErrorObservable);
  }


  delete(reviewObject: Reviewclass): Observable<Reviewclass> {


    const url = `${this.operateAPI}/${reviewObject.review_id}`;

    return this.http.delete(url, this.headers)
      .map(res => res.json())
      .catch(this.handleErrorObservable);
  }

  update(reviewObject: Reviewclass): Observable<Reviewclass> {

    let httpBody = JSON.stringify(reviewObject);

    const url = `${this.operateAPI}/${reviewObject.review_id}`;
    return this.http
      .put(url,httpBody , this.headers)
      .map(res => res.json())
      .catch(this.handleErrorObservable);
  }




  addNewByClass( newInfo:Reviewclass) : Observable<Reviewclass>  {

    const jsonBody = JSON.stringify(newInfo);

    return this.http.post(this.createAPI, jsonBody, this.headers)
      .map(res => res.json())
      .catch(this.handleErrorObservable);
  }




}
