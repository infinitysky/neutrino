import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';


// 3rd-party library
import { SharedModulesRegisterModule } from '../../shared/shared-modules-register.module';

@NgModule({
  imports: [

    SharedModulesRegisterModule,
    CommonModule,
    FormsModule,
    HttpModule,

    HomeRoutingModule,



  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
