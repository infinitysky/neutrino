import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

import {ValidationService} from '../shared/services/validation.service';

import { SharedModulesRegisterModule } from '../shared/shared-modules-register.module';

@NgModule({
  imports: [

    SharedModulesRegisterModule,


    CommonModule,
    LoginRoutingModule,


  ],


  declarations: [LoginComponent],
  providers: [ ValidationService ]
})
export class LoginModule { }
