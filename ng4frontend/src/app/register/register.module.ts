import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';

import {SharedModulesRegisterModule} from '../shared/shared-modules-register.module';

@NgModule({
  imports: [
    SharedModulesRegisterModule,
    CommonModule,
    RegisterRoutingModule
  ],
  declarations: [RegisterComponent]
})
export class RegisterModule { }
