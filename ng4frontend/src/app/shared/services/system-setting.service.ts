import { Injectable } from '@angular/core';

import { Http,Response, Headers,RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';


import { MY_CONFIG, ApplicationConfig } from '../../app-config';

import { Systemsettingclass } from '../classes/system-setting-class';

@Injectable()
export class SystemSettingService {

  private basicAPI = MY_CONFIG.apiEndpoint + MY_CONFIG.apiPath +  MY_CONFIG.systemSettingUrl;
  private operateAPI = this.basicAPI + '/items';
  private createAPI = this.basicAPI + '/create';


  private headers = new Headers({ 'Content-Type': 'application/json' });


  constructor(private http: Http) { }



  private handleErrorObservable (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }



  getAll(): Observable<Systemsettingclass[]> {
    return this.http.get(this.basicAPI)
    // .map(res => <DatabasesClass[]> res.json().data)
      .map(res => res.json())
      // .do(data => console.log(data)) // eyeball results in the console
      .catch(this.handleErrorObservable);
  }

  getCompanyInfo():  Observable<Systemsettingclass[]> {

    const url = `${this.basicAPI}/get_company_info`;

    return this.http.get(this.basicAPI)
    // .map(res => <DatabasesClass[]> res.json().data)
      .map(res => res.json())
      // .do(data => console.log(data)) // eyeball results in the console
      .catch(this.handleErrorObservable);
  }

  updateCompanyInfo(updateCompanyInfo) {

    const url = `${this.basicAPI}/update_company_info`;
    const httpBody = JSON.stringify(updateCompanyInfo);
    return this.http.post(url, httpBody, this.headers)
      .map(res => res.json())
      .catch(this.handleErrorObservable);
  }


  getById(settingId: number): Observable<Systemsettingclass> {
    const url = `${this.operateAPI}/${settingId}`;
    return this.http.get(url)
      .map(res => res.json())
      .catch(this.handleErrorObservable);

  }



  delete(systemSetting: Systemsettingclass) {

    const url = `${this.operateAPI}/${systemSetting.setting_id}`;

    return this.http.delete(url, this.headers)
      .map(res => res.json())
      .catch(this.handleErrorObservable);
  }

  update(systemSetting: Systemsettingclass) {

    let httpBody = JSON.stringify(systemSetting);

    const url = `${this.operateAPI}/${systemSetting.setting_id}`;
    return this.http
      .put(url, httpBody, this.headers)
      .map(res => res.json())
      .catch(this.handleErrorObservable);
  }


  addNew(systemSetting: Systemsettingclass): Observable<Systemsettingclass> {
    let httpBody = JSON.stringify(systemSetting );
    return this.http.post(this.createAPI, httpBody, this.headers)
      .map(res => res.json())
      .catch(this.handleErrorObservable);

  }




}
