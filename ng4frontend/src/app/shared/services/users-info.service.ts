import { Injectable } from '@angular/core';

import { Http,Response, Headers,RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';


import { MY_CONFIG, ApplicationConfig } from '../../app-config';

import { Userclass } from '../classes/user-class';


@Injectable()
export class UsersInfoService {

  private timeOutSeconds = MY_CONFIG.timeOut;
  private getAllAPI = MY_CONFIG.apiEndpoint + MY_CONFIG.apiPath +  MY_CONFIG.userGetAllUrl;
  private basicAPI = this.getAllAPI;
  private createAPI = MY_CONFIG.apiEndpoint + MY_CONFIG.apiPath +  MY_CONFIG.userCreateUrl;
  private operateAPI = MY_CONFIG.apiEndpoint + MY_CONFIG.apiPath +  MY_CONFIG.userOperateUrl;


  private headers = new Headers({ 'Content-Type': 'application/json' });

  private options = new RequestOptions({ headers: this.headers });
  public tempData: any;

  constructor(private http: Http) { }



  private extractDataObservable(res: Response) {
    let body = null;

    if(res.status < 200 || res.status >= 300) {
      body=res;
      throw new Error('This request has failed ' + res.status);
    }
    // If everything went fine, return the response
    else {
      body = res.json()  ;
    }
    return body;
  }

  private handleErrorObservable (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }



  getAll(): Observable<Userclass[]> {
    return this.http.get(this.getAllAPI)
    // .map(res => <DatabasesClass[]> res.json().data)
      .map(res => res.json())
      // .do(data => console.log(data)) // eyeball results in the console
      .timeout(this.timeOutSeconds)
      .catch(this.handleErrorObservable);
  }
  getTotalNumber(){
    const url = `${this.getAllAPI}/count_users`;

    return this.http.get(url)
    // .map(res => <DatabasesClass[]> res.json().data)
      .map(res => res.json())
      .timeout(this.timeOutSeconds)
      // .do(data => console.log(data)) // eyeball results in the console
      .catch(this.handleErrorObservable);

  }

  get(user: Userclass): Observable<Userclass> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    const url = `${this.operateAPI}/${user.user_id}`;

    return this.http.get(url, options)
      .map(res => res.json())
      .timeout(this.timeOutSeconds)
      .catch(this.handleErrorObservable);
  }

  getById(userId: number): Observable<Userclass> {


    const url = `${this.operateAPI}/${userId}`;

    return this.http.get(url, this.options)
      .map(res => res.json())
      .timeout(this.timeOutSeconds)
      .catch(this.handleErrorObservable);
  }


  delete(user: Userclass): Observable<Userclass> {



    const url = `${this.operateAPI}/${user.user_id}`;

    return this.http.delete(url, this.options)
      .map(res => res.json())
      .timeout(this.timeOutSeconds)
      .catch(this.handleErrorObservable);
  }

  update(user: Userclass): Observable<Userclass> {


    const httpBody = JSON.stringify(user);

    const url = `${this.operateAPI}/${user.user_id}`;
    return this.http.put(url, httpBody , this.options)
      .map(res => res.json())
      .timeout(this.timeOutSeconds)
      .catch(this.handleErrorObservable);
  }





  addNewUserByClass(newUserInfo: Userclass ) : Observable<Userclass>  {

    let httpBody = JSON.stringify({ first_name : newUserInfo.first_name, last_name: newUserInfo.last_name,
      email: newUserInfo.email, password: newUserInfo.password});
    // let body2 = "{time_frame_description:Team_description,time_frame_start:Team_start,time_frame_end :Team_end}";

    //console.log('Post message body: '+httpBody);
    return this.http.post(this.createAPI, httpBody, this.options)
    //.map(this.extractDataObservable)
      .map(res => res.json())
      .timeout(this.timeOutSeconds)
      .catch(this.handleErrorObservable);

  }

  updatePassword(userId: any, currentPassword: string, NewPassword: string,){
    const httpBody = JSON.stringify({ password: currentPassword, new_password: NewPassword});
    const url = `${this.basicAPI}/updatePassword/${userId}`;
    return this.http.post(url, httpBody, this.options)
      .map(res => res.json())
      .timeout(this.timeOutSeconds)
      .catch(this.handleErrorObservable);

  }

  adminUpdatePassword(userId: any, currentUserId: any, NewPassword: string,){
    const httpBody = JSON.stringify({ current_user_id: currentUserId, new_password: NewPassword});
    const url = `${this.basicAPI}/updatePassword/${userId}`;
    return this.http.post(url, httpBody, this.options)
      .map(res => res.json())
      .timeout(this.timeOutSeconds)
      .catch(this.handleErrorObservable);

  }



}
