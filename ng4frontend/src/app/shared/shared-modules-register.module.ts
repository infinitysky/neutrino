import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

/*
* 3rd party libraries
* */

// ngx-bootstrap
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { RatingModule } from 'ngx-bootstrap/rating';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { DatepickerModule } from 'ngx-bootstrap/datepicker';
import { PopoverModule } from 'ngx-bootstrap/popover';

// Others

import { DoorgetsTruncateModule } from 'doorgets-ng-truncate';
import { NouisliderModule } from 'ng2-nouislider';
import { MyDatePickerModule } from 'mydatepicker';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import { SelectModule } from 'ng2-select';
import { SliderModule } from 'primeng/primeng';
import { ChartsModule } from 'ng2-charts';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';


// TODO: Going to remove this module (ngx-bootstrap/modal) is better than Ng2Bs3ModalModule
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';



// shared Directives && Components
import { EasypiechartDirective } from './directives/easypiechart/easypiechart.directive';
import { ControlMessagesComponent } from './directives/control-messages/control-messages.component';


// Services
import { CookieService } from 'ng2-cookies';


// https://angular.io/styleguide#!#04-10
// This method is from Angle Theme

@NgModule({
  imports: [
    // Official Modules
    CommonModule,

    FormsModule,
    ReactiveFormsModule,


    // ngx-bootstrap
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    DatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    ProgressbarModule.forRoot(),
    RatingModule.forRoot(),
    TabsModule.forRoot(),
    TimepickerModule.forRoot(),
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),
    PopoverModule.forRoot(),








    // Other 3rd-party libraries

    NouisliderModule,
    DoorgetsTruncateModule,
    SelectModule,
    MultiselectDropdownModule,
    MyDateRangePickerModule,
    MyDatePickerModule,
    NgxMyDatePickerModule,
    SliderModule,

    Ng2Bs3ModalModule,

    // Charts
    ChartsModule,


  ],
  declarations: [
    EasypiechartDirective,

    ControlMessagesComponent,

  ],
  exports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule, // For solving the Forms error -> Can't bind to 'formGroup' since it isn't a known property of 'form'. ("



    //
    AccordionModule,
    AlertModule,
    ButtonsModule,
    CarouselModule,
    CollapseModule,
    DatepickerModule,
    BsDropdownModule,
    ModalModule,
    PaginationModule,
    ProgressbarModule,
    RatingModule,
    TabsModule,
    TimepickerModule,
    TooltipModule,
    TypeaheadModule,



    // Others

    NouisliderModule,
    DoorgetsTruncateModule,
    SelectModule,
    MultiselectDropdownModule,
    MyDateRangePickerModule,
    MyDatePickerModule,
    NgxMyDatePickerModule,
    SliderModule,

    Ng2Bs3ModalModule,


    // Charts
    ChartsModule,




    // Directives

    EasypiechartDirective,
    ControlMessagesComponent,


    // Services


  ],
  providers:[CookieService]

})
export class SharedModulesRegisterModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModulesRegisterModule
    };
  }
}
