
export class userClass{
  constructor(
    public user_id:number,
    public username:string,
    public email:string,
    public account_status:number,
    public first_name:string,
    public last_name:string,
    public mobile_number:string,
    public data_of_birth:string

  ){}
}
