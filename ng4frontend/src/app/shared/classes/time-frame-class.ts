export class Timeframeclass {

    time_frame_id : number;
    time_frame_description : string;
    time_frame_start : Date;
    time_frame_end : Date;
    time_frame_start_epoch:Date;
    time_frame_end_epoch:Date;


}
