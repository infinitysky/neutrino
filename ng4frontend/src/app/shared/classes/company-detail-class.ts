export class CompanyDetailClass {
  company_info_id: number;
  company_name: string;
  company_mission: string;
  company_vision: string;
  company_address: string;
  company_phone: string;
  company_email: string;
}
