import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundRoutingModule } from './not-found-routing.module';
import { NotFoundComponent } from './not-found.component';


import {SharedModulesRegisterModule} from '../shared/shared-modules-register.module';



@NgModule({
  imports: [
    SharedModulesRegisterModule,
    CommonModule,
    NotFoundRoutingModule
  ],
  declarations: [NotFoundComponent]
})
export class NotFoundModule { }
