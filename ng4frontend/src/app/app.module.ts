import { BrowserModule } from '@angular/platform-browser';
import { NgModule, enableProdMode } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

// 3rd-party library
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { DoorgetsTruncateModule } from 'doorgets-ng-truncate';

import { CookieService } from 'ng2-cookies';

// import { Ng2LetterAvatar } from "node_modules/ng2letteravatar/ng2letteravatar.js";

// developed module import here

import { MainModule } from './main/main.module';
import { LoginModule } from './login/login.module';



// developed component import here
import { AppComponent } from './app.component';

// developed services here
import {UserInfoContainerService} from './shared/services/user-info-container.service'; // global var services
import { MyCookieService } from './shared/services/my-cookie.service';
import {AlertService} from './shared/services/alert.service';

import { AuthGuard } from './shared/guards/auth.guard';
import {AuthenticationService} from './shared/services/authentication.service';
// enableProdMode();


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [






    ChartsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,

    DoorgetsTruncateModule,

    // Developed Module regist here
    LoginModule,
    MainModule,

  ],
  // sign the UserInfoContainerService as a global data store service.
  providers: [UserInfoContainerService, MyCookieService, AuthGuard, AuthenticationService, CookieService, AlertService],

  bootstrap: [AppComponent]
})

export class AppModule { }
