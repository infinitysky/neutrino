**Note: This Project created by using angular 4 (Frontend) & PHP(Backend).**

## Environment:
* Nodejs 6.x LTS
* PHP 7.x
* Mysql 5.6.x

## Environment preparation

#### Step 1 Server/Development Environment  Preparation
1. Install `node.js` 
2. Install `angular/cli` at the command prompt. ( `npm i -g @angular/cli` )
3. Install `xAMP/xNMP` stack ( Windows/Linux server, Apache/Nginx web server, Mysql, PHP )


#### Step 2 Application Preparation
1. Put `backEndAPI` folder into a web server folder (The web server should be Apache or Nginx with PHP 7 support. Do not consider any Node-PHP server this will not work for now)
2. Change the PHP configuration for MySQL server at `application/config/database`
3. Build a database called `okrs` (yes, it is lower case) at MySQL server
4. Insert the SQL file into the `okrs` database. The SQL file is `databse/okrs_startup_databse.sql`
5. Change the backend connections at `ng4Frontend/src/app/app-config.ts` 
6. Build the frontend at `ng4Frontend` folder (`ng build`). The final build will be located in `ng4frontend/dist` (If about )
7. Create a `.htaccess` file if necessary (Check **Not Found** at `Q & A` ↓ )




## Front End
#### Basic angular cli   
* `ng serve`
* `ng build `
* `ng build --aot --prod` (recommend) 

Go to Angular/cli  [https://github.com/angular/angular-cli](https://github.com/angular/angular-cli) for more information 

#### Start front end    
Go to folder `ng4 frontend`, then use command-line in the `ng4frontend` to type `ng serve` or `ng s`.  

#### Build front end for distribution   
Go to folder `ng4frontend`, then use command-line in the `ng4frontend` folder to type `ng build --aot --prod`.  

The `@angular/cli` will generate a `dist` folder for the production application.

> E.g Build the Front End to `/neutrino/ng4frontend/dist/?`
 the command is `ng build --aot --prod --base-href /neutrino/ng4frontend/dist/`  
 
See here : [https://github.com/angular/angular-cli/wiki/build](https://github.com/angular/angular-cli/wiki/build)


* After product build, rename the `dist` folder to any one you want and place into the webserver. 


#### Change call backend API:

Open `ng4frontend\src\app\app-config.ts`:  


* At Line `86` `apiEndpoint` for host IP/Domain and port number.
* At `apiPath` for the PHP index path. 

E.g : If the path of the PHP backend URL is `http://127.0.0.1:8080/neutrino/backEndAPI/index.php`, then the `apiEndpoint` must be `http://127.0.0.1:8080` and the `apiPath` should be `/neutrino/backEndAPI/index.php`.  

**Check the `/` (do not put / at the end ) !**

#### services  

There are two places to find the services:

* For login, user information and any ganeral sevices `ng4frontend\src\app\shared` . 

* Anything about OKRs that will be located at `ng4frontend\src\app\main\okr\okr-shared\services`


* Note: These are some services called ` xxx-container.service`. These services are using `Observable` method to store the necessary information globally or in the module. To share these information, that stored in the container service, you need declare the service provider at `xxx.module` 


#### Classes

There are two places to find the Classes:

* For login, user information and any ganeral Classes  
 `ng4frontend\src\app\shared\classes`

* Any thing about OKRs system that will be located at `ng4frontend\src\app\main\okr\okr-shared\classes`


#### 3rd party Libraries
In order to import these 3rd party libraries (include CSS), you have to register them in `ng4frontend\.angular-cli.json`.   
**Note: this is a formal JSON file, so you can not put any comments inside**

Add new library:
1. find the 3rd-party library.
2. add the library into `package.json`
3. use `npm` command to install the new 3rd-party library
 
This way will keep the `package.json` more easy to read!


* In order to save time, I created a module for 3rd-party libraries in project.(`/ng4frontend/src/app/shared/shared-modules-register.module`)




Currently, the Angular 4 default libraries are:   

```
{
  "name": "ng4frontend",
  "version": "0.0.0",
  "license": "MIT",
  "scripts": {
    "ng": "ng",
    "start": "ng serve",
    "build": "ng build",
    "test": "ng test",
    "lint": "ng lint",
    "e2e": "ng e2e"
  },
  "private": true,
  "dependencies": {
    "@angular/common": "^4.0.0",
    "@angular/compiler": "^4.0.0",
    "@angular/core": "^4.0.0",
    "@angular/forms": "^4.0.0",
    "@angular/http": "^4.0.0",
    "@angular/platform-browser": "^4.0.0",
    "@angular/platform-browser-dynamic": "^4.0.0",
    "@angular/router": "^4.0.0",
    "core-js": "^2.4.1",
    "rxjs": "^5.1.0",
    "zone.js": "^0.8.4"
      },
  "devDependencies": {
    "@angular/cli": "1.0.6",
    "@angular/compiler-cli": "^4.0.0",
    "@types/jasmine": "2.5.38",
    "@types/node": "~6.0.60",
    "codelyzer": "~2.0.0",
    "jasmine-core": "~2.5.2",
    "jasmine-spec-reporter": "~3.2.0",
    "karma": "~1.4.1",
    "karma-chrome-launcher": "~2.1.1",
    "karma-cli": "~1.0.1",
    "karma-jasmine": "~1.1.0",
    "karma-jasmine-html-reporter": "^0.2.2",
    "karma-coverage-istanbul-reporter": "^0.2.0",
    "protractor": "~5.1.0",
    "ts-node": "~2.0.0",
    "tslint": "~4.5.0",
    "typescript": "~2.2.0"
  }
}


```

Please keep in mind that the angular/cli could be updated by the angular/CLI team! Keep the `package.json` file order will help you to target 3rd-part libraries. Do not use `ng upgrade` to update the project! It will overwrite current source code. The `ng upgrade` will overwrite `app-touting.modules.ts`, `app.component.x` files and `app.module.ts`





## Database
* database name : `okrs`
* the `neutrino\database\okrs.sql` is the structure of the database.
* the `neutrino\database\okrs_new_fk_with_dummy.sql` is the the database dummy data & database structure.


## PHP

Change `neutrino\backEndAPI\application\config\database.php` for mysql database IP/domain, username and password.






## Q & A
### 1. How to start up?
#### Step 1 Server preparation
1. Install `node.js` 
2. Install `angular/cli` at the command prompt. ( `npm i -g @angular/cli` )
3. Install `xAMP/xNMP` stack ( Windows/Linux server, Apache/Nginx web server, Mysql, PHP )
#### Step 2 Application preparation
1. Put `backEndAPI` folder into a web server folder (The web server should be Apache or Nginx with PHP 7 support. Do not consider any Node-PHP server this will not work for now)
2. Change the PHP configuration for MySQL server at `application/config/database`
3. Build a database called `okrs` (yes, it is lower case) at MySQL server
4. Insert the SQL file into the `okrs` database. The SQL file is `databse/okrs_startup_databse.sql`
5. Change the backend connections at `ng4Frontend/src/app/app-config.ts` 
6. Build the frontend at `ng4Frontend` folder (`ng build`). The final build will be located in `ng4frontend/dist` (If about )
7. Create a `.htaccess` file if necessary (Check **Not Found** ↓)




### 2. Not Found

The application is working but not for direct URL or after F5 refresh web page.

E.g: `http://okr.mytestdomain.me/okr/okr-goals?timeFrameId=64`
I got an error message:  

```
Not Found

The requested URL /okr/okr-goals was not found on this server.

Apache/2.4.7 (Ubuntu) Server at okr.mytestdomain.me Port 80
```

But the  `http://okr.mytestdomain.me/` is working.

`OR` After I pressed F5 in the web application, I get the `Not Found` Error message


### **SO, IT IS A `PushState` PROBLEM FOR WEB SERVER!**


Check this:
 
[https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions#how-to-configure-your-server-to-work-with-html5mode](https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions#how-to-configure-your-server-to-work-with-html5mode)  

and   
[https://gist.github.com/jbutko/6b500210b4c6811bca54](https://gist.github.com/jbutko/6b500210b4c6811bca54)




### To solve this problem, you have two ways to solve this issue.
1.  Create a .htaccess file 

* this ensures support for pretty links even after page refresh (F5)

```
<IfModule mod_rewrite.c>
    Options +FollowSymlinks
    RewriteEngine On
    # Don't rewrite files or directories
    RewriteCond %{REQUEST_FILENAME} -f [OR]
    RewriteCond %{REQUEST_FILENAME} -d
    RewriteRule ^ - [L]

    # Rewrite everything else to index.html to allow html5 state links
    RewriteRule ^ index.html [L]
</IfModule>
```

2. Change the web server `Configuration` 

* Apache Rewrites  

```
<VirtualHost *:80>
    ServerName my-app

    DocumentRoot /path/to/app

    <Directory /path/to/app>
        RewriteEngine on

        # Don't rewrite files or directories
        RewriteCond %{REQUEST_FILENAME} -f [OR]
        RewriteCond %{REQUEST_FILENAME} -d
        RewriteRule ^ - [L]

        # Rewrite everything else to index.html to allow html5 state links
        RewriteRule ^ index.html [L]
    </Directory>
</VirtualHost>
```

* Nginx Rewrites

```
server {
    server_name my-app;
    
    index index.html;

    root /path/to/app;

    location / {
        try_files $uri $uri/ /index.html;
    }
}

```

* Azure IIS Rewrites

```
<system.webServer>
  <rewrite>
    <rules> 
      <rule name="Main Rule" stopProcessing="true">
        <match url=".*" />
        <conditions logicalGrouping="MatchAll">
          <add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />                                 
          <add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />
        </conditions>
        <action type="Rewrite" url="/" />
      </rule>
    </rules>
  </rewrite>
</system.webServer>

```


* Express Rewrites

``` 
var express = require('express');
var app = express();

app.use('/js', express.static(__dirname + '/js'));
app.use('/dist', express.static(__dirname + '/../dist'));
app.use('/css', express.static(__dirname + '/css'));
app.use('/partials', express.static(__dirname + '/partials'));

app.all('/*', function(req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    res.sendFile('index.html', { root: __dirname });
});

app.listen(3006); //the port you want to use

``` 

Also, read this great tutorial about Angular + Express

* ASP.Net C# Rewrites

In `Global.asax`

```
private const string ROOT_DOCUMENT = "/default.aspx";

protected void Application_BeginRequest( Object sender, EventArgs e )
{
	string url = Request.Url.LocalPath;
	if ( !System.IO.File.Exists( Context.Server.MapPath( url ) ) )
		Context.RewritePath( ROOT_DOCUMENT );
} 

```

* Java EE

In `web.xml`

```  
<?xml version="1.0" encoding="UTF-8"?>
<web-app version="3.0" xmlns="http://java.sun.com/xml/ns/javaee" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  
xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd">
	<error-page>
		<error-code>404</error-code>
		<location>/</location>
	</error-page>
</web-app>

```

Play Framework In routes file:

```
GET        /                                 controllers.Application.index(someRandomParameter = "real index")
# All other routes should be here, in between
GET        /*someRandomParameter             controllers.Application.index(someRandomParameter)   
```


### 3. Shared Modules

* I created a module file `src\app\shared\shared-modules-register.module.ts` for new module registration. It will applay to the application. (for time saving propose)